/label ~"possible bug"

## Reproduction steps
<!-- Add list of reproduction steps. More details are better. You can also attach screenshot if applicable. -->

1.

## Expected behaviour
<!-- Describe the expected application behaviour -->

## Actual behaviour
<!-- Describe the actual application behaviour -->

## Context
<!-- Add any related context (similar features in other apps, related issues, ...). -->
<!-- If you don't have anything to add, feel free to delete this section. -->

## Impact
<!-- Leave this empty when creating the issue. -->
<!-- Assignee MUST list all parts of the application that could be affected by the change. -->
