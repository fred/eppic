"""This is a script that generates the structure of the help file, based on the currently registered commands.

Rewrites the help file located at `eppic/help.yaml`.

Usage:
    Run from the main folder: `python scripts/help_structure_gen.py`
    Dry run: `python scripts/help_structure_gen.py --dry-run`
    (Dry run output can be used to create more help files, for example using a pipe).
"""
from pathlib import Path
from sys import argv
from typing import Dict, cast

from yaml import dump, safe_load

from eppic.app import Application
from eppic.commands.command import Command
from eppic.models import ArgumentLabelSuggestion
from eppic.settings import Settings

HELP_PATH = Path("eppic/help.yaml")


def get_current_help_contents(help_path: Path) -> Dict:
    """Get contents of the existing help file (if it exists)."""
    if not help_path.exists():
        return {}
    with help_path.open("r") as f:
        ret = safe_load(f)
    return cast(Dict, ret)


def add_command(help: Dict, name: str, has_args: bool) -> None:
    """Add `name` to `help`, with default help structure."""
    help[name] = {"description": "TODO"}
    if has_args:
        help[name]["arguments"] = {}


def update_argument(help_object: Dict, suggestion: ArgumentLabelSuggestion) -> None:
    """Add an argument to the help, or update an existing argument with current subargument structure."""
    name = ".".join(suggestion.labels)

    argument_help = help_object[name] if name in help_object.keys() else "TODO"
    if isinstance(argument_help, str):
        description = argument_help
        values: Dict[str, str] = {}
    else:
        description = argument_help["description"]
        values = argument_help["values"] if "values" in argument_help.keys() else {}

    if suggestion.values:  #
        for value in suggestion.values:
            if value not in values.keys():
                values[value] = "TODO"
        for old_value in values.keys():
            if old_value not in suggestion.values:
                values.pop(old_value)
    else:
        values = {}

    help_object[name] = description if values == {} else {"description": description, "values": values}


def update_help_via_commands(current_help: Dict, commands: Dict[str, Command]) -> None:
    """Update the existing help with current commands and their arguments."""
    for name, command in commands.items():
        if name == 'help':
            continue
        if name not in current_help.keys():
            add_command(current_help, name, bool(command.arg_parser._arguments_list))
        for suggestion in command.arg_parser.get_available_labels([]):
            if "arguments" not in current_help[name].keys():
                current_help[name]["arguments"] = {}
            update_argument(current_help[name]["arguments"], suggestion)


def check(new_content: str) -> None:
    """Check if help is up to date."""
    with open(HELP_PATH) as f:
        original_content = f.read()

    if "TODO" in new_content:
        print("TODO in new content!")
        for line in new_content.split("\n"):
            if "TODO" in line:
                print(line)
        exit(1)

    if original_content != new_content:
        print("Contents do not match!")
        for line_new, line_old in zip(new_content.split("\n"), original_content.split("\n")):
            if line_new != line_old:
                print("First conflict:")
                print("New:", line_new)
                print("Old:", line_old)
                break
        exit(2)


def main() -> None:
    """Generate help."""
    app = Application(Settings(), None)
    registry = app.command_registry
    current_help = get_current_help_contents(HELP_PATH)
    update_help_via_commands(current_help, registry._commands)

    if "--check" in argv:
        check(dump(current_help))
        return

    if "--dry-run" in argv:
        print(dump(current_help))
        return

    with open(HELP_PATH, "w") as f:
        dump(current_help, f)


if __name__ == '__main__':
    main()
