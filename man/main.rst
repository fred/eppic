fred-eppic(1)
#############

========
SYNOPSIS
========

``eppic [options] [--] [command ...]``

===========
DESCRIPTION
===========

*fred-eppic* is a command-line interface to the FRED EPP server.

If ``command`` is specified, it starts up, runs the command, prints its output
and exits. Otherwise, it is started in an interactive mode, where it gathers
commands from the standard input and executes them in the usual REPL fashion.

*fred-eppic* can be configured from a config file (as described in
:doc:`config`), some configuration options can be further altered by the
``[options]`` (as described in the *OPTIONS* section), and finally, some
options can be changed in-session by the ``change-settings`` command (for more
information, run ``eppic help change-settings``).

Any internal errors are printed to the standard error. When using the
interactive input, all the errors that the command parser is able to capture
(such as incorrect argument names, incorrect value type etc.) are printed to
the user in the so-called bottom toolbar (last line of the screen), and the
command is not allowed to be submitted before the errors are resolved.

Default Input Format
--------------------

The default command structure is as follows::

   <command-name> --<argument-name>(=)<argument-value> ...

The ``=`` is optional and can be omitted. The ``argument-name`` is optional in
some cases, where it's obvious what the argument name should be, but it is
highly encouraged that it is supplied anyways to avoid ambiguous commands and
unexpected behavior.

In case the ``<argument-value>`` is more complicated (for example, it uses
spaces, quotes, equal signs), it can be surrounded by quotes. For example::

  sample-command --arg="very complicated value with -- and ="

would result in ``arg``'s value being *very complicated value with -- and =*.
If quotes also need to be a part of the value, then either the value can be
surrounded by the other kind of quotes, or the quotes can be escaped using
``\``. For example::

  sample-command --arg="very complicated value for \"name\" with \"--\" and \"=\" and \"\\\""

would result in ``arg``'s value being *very complicated value for "name" with
"--" and "=" and "\\"*. The placement of the quotes doesn't matter, so
``command --arg="val ue"`` is the same as ``command "--arg=val ue"``

Some arguments require a list of values. Multiple values can be supplied to
such arguments by appending a unique identifier to the argument name. For
example, the ``check-domain`` command takes a ``names`` argument, which is a
list of strings. To check the names ``nic.cz``, ``foo.cz`` and ``bar.cz``, the
following command can be used::

   check-domain --names-1=nic.cz --names-2=foo.cz --names-3=bar.cz

The argument names have to be unique, but they don't need to be numbers. For
example::

   check-domain --names-nic=nic.cz --names-foo=foo.cz --names-bar=bar.cz

This comes in handy when the argument isn't a list of strings, but a list of a
more complicated type (such as in the ``create-contact`` command). For a more
advanced example, suppose there was a command ``get-ages``, with one argument
``arg``, which would take a list of dataclasses, with fields ``name`` and
``age``. Then the command::

   get-ages --arg-1.name=Tony --arg-1.age=42 --arg-2.name=Eve --arg-2.age=40

would resolve in ``arg`` containing two dataclasses, one with ``name`` equal to
Tony, with ``age`` 42, and the other with ``name`` Eve and ``age`` 40.

Help
----

All the commands of *eppic* are described in its built-in *help* module. You
can access these descriptions using the ``help`` command in several ways, for
example::

   help
   help partial-command-name
   help command-name [args]


This results in the following behavior:

* If ``help`` is run on its own, it prints out all the help for all the
  commands.
* If ``partial-command-name`` is supplied, it only shows help for the matching
  commands.
* If ``args`` are also specified, it still shows the description of the matching command.

The intended usage of this is, that when you're mid-way through writing a
command and you forget a name or the exact functionality of an argument, you
can pre-pend what you've already written with ``help``, and the system gives
you the exact information you require. In fact, when using the interactive
input, there's a shortcut ``<C-Space>``, which does exactly that.

Sessions
--------

*eppic* uses so-called *sessions* to manage connections to the EPP server. A
session is just a collection of information needed to successfully connect to a
server, such as hostname, username, password etc. All fields of a session are
described in the configuration manual (:doc:`config`).

Each session has an ``ID``. By default, a session's ``ID`` is its username, but
this can be changed by specifying its ``alias`` field. Whenever choosing a
session, you can either use its ``ID``, or its position in the list of sessions
(starting from zero). Note that sessions defined using the ``new-session``
command are always put at the end of the list.

There can always be at most one active session.
When starting *eppic*, a session is chosen as follows\:

#. If any of the options to create a new session is specified, they are used to
   create a new session and it is automatically selected.
#. If the ``--session-id`` option is specified, that session is selected.
#. If any sessions are defined in the configuration file, then the first
   session is selected.
#. Otherwise, *eppic* is started without a session.

Whenever a session is selected, *eppic* looks if its ``username`` and
``password`` fields are filled in. If they are, it initializes a connection
with the server and logs in the specified user.

Sessions with unspecified ``username`` and ``password`` seem useless, but they
have a purpose. They can be used as "templates" -- specifying details about the
server connection, and then, user can specify the ``username`` and ``password``
later using the ``new-session`` command, with the template session's id as the
``--base-session-id``. For more information, use ``eppic help new-session``.

*WARNING* if you decide to specify ``password``, other processes might be able
to read it and it will end up in the command history file!

You can exit a session using the ``logout`` command. It properly terminates
your connection to the server. Proper logout is also initialized when you're
logged in and you decide to switch your session (by invoking the ``login``
command) or when *eppic* sees a problem with the current server connection.

=======
OPTIONS
=======

The options can be given in any order, but before the ``command``.

:--help: Show help.
:--input-file: Specify the file to get the input from. If not specified, stdin
               is used. If ``command`` is specified, it takes precedence over
               ``--input-file`` (or the stdin).
:--config: Settings file location. Defaults to :file:`~/.eppic.conf`, or
           :file:`/etc/fred/eppic.conf`, if the previous doesn't exist.
:--session-id: Specify session by alias. The default is the first in the list,
               or to an empty session, if no sessions are defined. If any of
               the following arguments are specified, ``--session-id`` is
               ignored, and a new session is created and used.
:--username: Create a new session and set its ``username``.
:--password: Create a new session and set its ``password``.
:--hostname: Create a new session and set its ``hostname``.
:--port: Create a new session and set its ``port``.
:--cert-file: Create a new session and set its ``cert_file``.
:--key-file: Create a new session and set its ``key_file``.
:--obj-uris: Create a new session and set its ``obj_uris``.
:--verify: Create a new session and set its ``verify``.
:--alias: Create a new session and set its ``alias``. Defaults to ``username``.
:--reconnects: Create a new session and set its ``reconnects``. Defaults to 1.

===========
EXIT STATUS
===========

The exit status of ``eppic`` depends on the result of the last submitted user command. It can be one of the following

:0: The utility ran as expected, no errors found.
:1: Invalid config file.
:2: Invalid usage (i.e. unknown option, illegal option value, etc).
:3: Misc error caught by ``eppic``, (i.e. trying to communicate with the server with no active session).
:4: Command parsing failed (i.e. tried to supply an unknown argument).
:5: Communication with the server failed (either the credentials were invalid or some other error occured).

=====
FILES
=====

:~/.eppic.conf: The primary configuration file. This file is used to get the
                configuration.
:/etc/eppic/eppic.conf: The fallback configuration file. This file is used,
                        when :file:`~/.eppic.conf` doesn't exist. If this
                        configuration file doesn't exist either, *eppic* is
                        started with the default configuration.

========
SEE ALSO
========

| :doc:`config`

Further information about FRED can be found on the project's homepage <https://fred.nic.cz/>.
