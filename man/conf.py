"""Configuration file for the Sphinx documentation builder."""
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import os

project = 'fred-eppic'
# copyright =
author = 'Filip Úradník'
version = '3.0.1'
# The full version, including alpha/beta/rc tags
release = os.getenv('CI_COMMIT_TAG')
if not release:
    release = os.popen('git describe --tags').read().strip().split("-")[0]  # noqa: S605, S607

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

exclude_patterns = ['_build']


man_pages = [
    # (startdocname, name, description, authors, section),
    ("main", "fred-eppic", "EPP Interactive Client", "", "1"),
    ("config", "fred-eppic", "eppic configuration files", "", "5"),
]
