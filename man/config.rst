fred-eppic(5)
#############

===========
DESCRIPTION
===========

*fred-eppic* gets its configuration from configuration files in the *YAML* format.

It always uses at most one configuration file, and it looks for it in the following order:

#. the file from the ``--config`` option, if specified
#. :file:`~/.eppic.conf`
#. :file:`/etc/eppic/.eppic.conf`

Only if it doesn't find any of these files, it is started with it's default configuration.

=====================
CONFIGURATION OPTIONS
=====================

:sessions: A list of *sessions*. Each session has the following subkeys

   :hostname: Hostname of the EPP server.
   :port: The port of the EPP server.
   :cert_file: The path to the file with the certificate.
   :key_file: The path to the key file.
   :obj_uris: A list of object uris. Defaults to the standard CZ.NIC EPP object uris.
   :username: The username to log in with.
   :password: The password to log in with.
   :verify: Whether or not to verify the peer certificate. Default\: ``true``.
   :alias: The alias of the session. This can be used when choosing from
      multiple session. If unspecified, the session is identified by the
      ``username``.
   :reconnects: The number of times to reconnect if sending a request
      to the EPP server fails. Default\: ``1``.

:input_file: Path to the file from which to take the input. If unspecified, the
             standard input is used.
:input_format: The expected format of the input. Default\: ``DEFAULT``. Can be one of the following

   :DEFAULT: The input is expected in the format described in :doc:`main`. Each command is on one line.
   :JSON: Each line should contain a *valid JSON document* containing exactly
      *one key* - the command name. The arguments should be in its subkeys.
      Arguments with subarguments contain their subarguments as subkeys.
      (Sub-)Arguments which expect a list of values contain a *list*, where each
      element of the list is a valid subargument, as expected by the command.
   :YAML: The entire input file (or standard input) is a valid *YAML
      multi-document*, with exactly one valid document for each command. Each
      document contains exactly one key, the command name. The arguments are in
      its subkeys. Arguments with subarguments contain their subarguments as
      subkeys. (Sub-)Arguments which expect a list of values contain a *list*,
      where each element of the list is a valid subargument, as expected by the
      command.

:output_format: The format used when printing responses to the standard output.
   Default\: ``DEFAULT``. Can be one of the following

   :DEFAULT: Human-readable output, without specified format. This is the only
      output format that uses syntax highlighting (if enabled).
   :JSON: Each line is a valid *JSON* document, containing several keys with
      the information received from the server.
   :YAML: The entire standard output is a valid *YAML* multi-document, each
      document represents one response from the server.

:logging: Configuration of logging, as specified in the Python documentation.
   Defaults to the default Python logging configuration.
:interactive: Settings defining the behavior of interactive input/output.
   Can have any of the following subkeys

   :autocomplete: Whether or not to use autocompletion in interactive input.
      Default\: only when the terminal supports it. This option can be
      overwritten to ``true`` or ``false``. Note that ``true`` forces the
      completion on even in situations that do not support it, such as when
      piping input and output to/from a file, which may insert illegal
      characters to the output file.
   :history_file_path: The path to which to write the history of inputted
      commands. Those are later used for auto-suggestions. Default\:
      :file:`~/.cache/eppic/autosuggest_history`.
   :vi_mode: Whether to use VI-like keybindings (``true``), or Emacs-like
      keybindings (``false``, default).
   :online_validation: Whether to validate the commands as they are typed in.
      Default\: ``true``.
   :enable_prompt: Whether or not to show the prompt. Default\: only when the
      terminal supports it. This option can be overwritten to ``true`` or
      ``false``. Note that ``true`` forces the prompt on even in situations
      that do not support it, such as when piping input and output to/from a
      file, which may insert illegal characters to the output file.
   :command_confirmation: If true, *eppic* will print the EPP to be sent to the
                          server and prompt the user for confirmation before
                          sending it. Default\: ``false``.
   :syntax_highlighting: Whether or not to use syntax highlighting on input and
                         output. Default\: only when the terminal supports it.
                         This option can be overwritten to ``true`` or ``false``.
                         Note that ``true`` forces syntax highlighting on even in
                         situations that do not support it, such as when piping
                         input and output to/from a file, which may insert illegal
                         characters to the output file.

:print_raw_xml: Whether to print the raw XML sent to, and received from the server.
   Default\: ``DISABLED``. Can be one of the following

   :DISABLED: Do not print the raw XML.
   :RAW: Print the XML exactly as it was sent/received.
   :FORMATTED: Parse the XML and format it so that it is easier to read.

   Note: if ``syntax_highlighting`` is enabled, the XML is also highlighted.

:print_list_results_automatically: In FRED, ``list`` commands only prepare the
                                   list results, but do not retreive them. With
                                   this option set to ``true``, *eppic* follows
                                   up any ``list`` command with multiple calls
                                   of the ``get-results`` command, until all
                                   the results are gathered, and then prints
                                   them all to the user.

:exit_on_error: If a command fails, log out and stop eppic. By default, it is
                ``false`` when running from a tty, ``true`` otherwise.

=====
FILES
=====

:~/.eppic.conf: The primary configuration file. This file is used to get the
                configuration.
:/etc/eppic/eppic.conf: The fallback configuration file. This file is used,
                        when ``~/.eppic.conf`` doesn't exist. If this
                        configuration file doesn't exist either, *eppic* is
                        started with the default configuration.

========
SEE ALSO
========

| :doc:`main`
