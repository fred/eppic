===============
Command parsing
===============

Schema of command parsing::
                                                                                                                                                           
                                                User input                                                                                               
                                                                                                                                                           
     +----------+         +------------------+          +----------------------+        +----------------+                                                 
     |          |         |                  |          |                      |        |                |                                                 
     |   File   |         |  Standard input  |          |  Interactive input   |        |  CLI argument  |                                                 
     |          |         |                  |          |                      |        |                |                                                 
     +============================================+    +========================+  +========================+                                              
     |        |        |                          |    |                        |  |                        |                                              
     |  JSON  |  YAML  |  Text (standard format)  |    | Text (standard format) |  | Text (standard format) |                                              
     |        |        |                          |    |                        |  |                        |                                              
     +============================================+    +========================+  +========================+                                              
         |          |                 |                     |                           |                                                                  
         |          |                 |                     |                           |                                                                  
         |          |                 |                     v                           |             Input Handler Layer:                                 
         v          v                 v          Prompt toolkit IH                      v                                                                  
     Json IH      Yaml IH          File IH       |                                  Text IH               * provides an interface to                       
         |          |                 |          | * further provides:                  |                   files/stdin/...                                
         |          |                 |          |     * autocompletion                 |
         |          |                 |          |     * autosuggestion                 |                 * splits the user input to                       
         +----------+                 +----------+     * validation                     |                   commands (str or dict)                         
                |                         |                                             |                                                                  
   -------------|-------------------------|---------------------------------------------|--------------------------------                      
                |                         |                                             |                                                      
 Dict IP  ------+                         +- Text IP -----------------------------------+     Input Processor Layer:                               
                                                                                                                                                 
 * expects each command to be a dict         * expects each command to be a string               * unifies different command formats             
 	with one key: command name,                	(in the standard format)                           		(dictionary, text)                            
     and its args as subkeys                                                                                                                     
     (subarguments as sub-sub keys)          * this is where argument names are                  * recognises the command name                   
              |                              	split to *labels*                                                                                  
              |                                             |                                    * gathers and puts together the argument labels,
              |                                             |                                    	and argument values                            
              +--------->+============================+<----+                                                                                    
                         |   Parsed Command           |                                                                                        
   ----------------------|    * raw_args filled       |--------------------------------------------------------------------                    
                         |    * args currently None   |                                                                                        
                         +============================+                                                                                   
                                        |                                     Command Resolution:                                                  
                                        v                                                                                                          
                      +-----------------------------------+                       * resolves the command name                                      
                      |                                   |                                                                                        
                      |    CommandRegistry.get_command    |                       * returns a Command object, with the knowledge of the            
                      |                                   |                       	final class to be sent to the Epp handler,                      
                      +-----------------------------------+                           and the `ArgumentParser`, which is able to build the class   
                                        |                                                                                                          
                                        |                                                                                                          
                                        v                                                                                                          
                        +---------------------------------+                     Argument Parsing:                                                  
                        |                                 |                                                                                        
                        | ArgumentParser.parse_arguments  |                         * works as follows:                                            
                        |                                 |                             * resolve the labels of positional arguments               
                        +---------------------------------+                             * group the parsed arguments by the main label             
                                        |                                               * parse values of each argument using Argument instances   
                                        |                                                                                                          
                                        |                                                                                                          
                                        |                                                                                                          
                                        |                                        EPP Command Building:                                             
                      +-----------------v----------------+                                                                                         
                      |                                  |                          * handles taking off command extensions                        
                      |     Command.prepare_command      |                                                                                         
                      |                                  |                          * uses the parsed arguments to build the epplib.command class  
                      +----------------------------------+                                                                                         
                                        |                                           * later re-adds the extensions to the built class.             
                                        |                                                                                                          
                                        |                                                                                                          
                                        v                                                                                                          
                        +-----------------------------+                          Command Execution:                                                
                        |                             |                                                                                            
                        |   Application.run_command   |                             * has the following steps:                                     
                        |                             |                                 * run pre hooks                                            
                        |   EppHandler.execute        |                                 * send EPP command to the server                           
                        |                             |                                 * print the server response                                
                        +-----------------------------+                                 * run the post hooks                                       
