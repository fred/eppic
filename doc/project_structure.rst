===============
EPPIC structure
===============

::

   * Application------------------------------------------------------------------------

      Holds the app session information, runs everything.

      * Command-------------------------------------------------------------------------
      |
      |   * Argument parser
      |
      |     - Argument / SequenceArgument / ListArgument
      |
      |        * parse the arguments.
      |        * if the arguments are too complicated, they split them into easier
      |          arguments and create their Argument subclasses.
      |
      |
      |  * Command class
      |
      |     Instance of this class is the final product of the Command.
      |     It can be:
      |
      |        * an EPP command:
      |
      |           when this is returned to the application, it calls its EPP
      |           handler, which calls the EPPlib and communicates with the
      |           server.
      |
      |        * a Service command:
      |
      |           used for non-epp commands, such as switching sessions, logging
      |           in/out, printing help, etc.
      |
      |           def handle(self, application: Application)
      |
      |              This is the function that's called by Application when the
      |              command is being executed.
      |
      |              It takes the app instance as an argument, because it is
      |              needed for commands like login, which need to edit the
      |              app's epp handler instance.

      * EPP handler---------------------------------------------------------------------
      |
      |  Communicates with the EPP server through EPPlib.

      * Output handler------------------------------------------------------------------
      |
      |  Handles printing, either as fancy colored text, or as plain text in
      |  whatever format.

      * Input handler-------------------------------------------------------------------
      |
      |  Gets the input from the user. It only knows how to split the input into
      |  commands, doesn't know about anything else.
      |
      |  It is an iterator that yields raw commands.

      * Input processor-----------------------------------------------------------------
      |
      |  Processes the raw input from the input handler.
      |
      |  It is callable, with the raw input representing one command, and it
      |  returns a tuple:
      |
      |     * command name
      |     * iterable for arguments, where each argument is just a tuple containing:
      |
      |        * a list of labels: since arguments can have more levels, such as
      |          --argument.subarg.subsub=..., it splits it by the labels so it's
      |          syntax-independent: ["argument", "subarg", "subsub"]
      |
      |        * a value
      |
      |        both of these can be None. The missing ones are handled by the arg
      |        parser. The labels can be None when the user inserted an implicit
      |        argument, ie `info-domain nic.cz`, and the value can be None when
      |        parsing for help or completions.
