ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

3.0.1 (2024-04-05)
------------------

* Add support for ``fred-epplib`` 3.1.0 (#165).
* Move printing of raw xml to the epp handler. (#157).
* Migrate from `shlex.shlex` to `shlex.split` (#160).
* Update issue templates.

3.0.0 (2024-02-01)
------------------

* Add support for python 3.12.
* Update ``fred-epplib`` to 3.X.X (#158).
* Add VI mode (#124, #132).
* Add help content (#47, #152).
* Get results hook print all results (#125).
* Add ``,``, ``%`` and ``:`` to the shlex wordchars (#121, #147).
* Fix Command-line args POSIX (#135).
* Output ``MsgQ`` if it isn't empty (#128).
* Add ``values`` and ``values_ext`` tags to output (#129).
* Add an option to print raw XML output in eppic (#123).
* Fully disable prompt toolkit when not tty (#131).
* Allow users to specify custom cltrid (#137).
* Print extensions on output (#145).
* Clean up interactive settings (#144).
* Exit on error (#150, #151).
* Put in proper description of output formats (#115).
* Rename ``switch-session`` to ``login`` in readme (#118).
* Add eppic manpages (#57).
* Support Pydantic 2.* (#139).
* Return only the value from ``parse_value`` (#117).
* Force all-optional dataclasses return ``None`` (#122).
* Consider ``Optional[T]`` fields as not required (#154).
* Add custom ``Argument`` parsing exceptions (#120).
* Add an output format definition (#136).
* Simplify ``Application`` API (#140).
* Move ``get_input_handler`` to ``Settings`` (#130).
* Rename ``ExecutableCommand`` to ``Request`` (#143).
* Add ``Command`` subclasses which handle execution-specific code (#141).
* Add debug logs (#149).
* Improve internal documentation (#108, #134).
* Fix tests (#113).
* Update package setup.

2.0.0 (2023-08-10)
------------------

* Enable configless run (#42).
* Restrict positional arguments. (#96).
* Fix and improve output (#68, #70, #82, #92).
* Use dashes instead of underscores in argument names (#79, #111).
* Automatically apply the disclose policy (#91).
* Add `hello` command (#88).
* Add ``poll`` commands (#93, #114).
* Add ``raw-request`` command (#40, #102).
* Add ``change-settings`` command (#41).
* Add ``new-session`` command (#84).
* Remove ``set-session`` command ().
* Change ``login`` command ().
* Settings validation no longer throws traceback (#55).
* Add an option for command confirmation (#28).
* Add an option to print results automatically on list commands.
* Fix the option to disable auto completion (#101).
* Disable interactive mode when running without tty (#103).
* Add command hooks (#90, #97).
* Move session to epp handler. (#49).
* Fix and refactor argument processing (#71, #72, #73, #74, #77, #78, #80, #83, #85, #86, #89, #98, #104, #105, #110).
* Close connection on `send_command` error. (#69).
* Unite the models into `models`. (#99).
* Fix and update tests (#68, #106, #107, #109).
* Update project setup (#112, #116).

1.0.1 (2022-12-21)
------------------

* Fix nested dataclass attributes (#66).
* Output in unicode encoding (#67).

1.0.0 (2022-12-15)
------------------

* Add support for Python 3.11.
* Suggest required args first (#59).
* Tweak prompt validation (#62).
* Update session defaults (#54).
* Fix get-results output (#60).
* Fix representing enums in output (#56).
* Fix subarguments' required flag (#58).

* Add ``ArgumentField`` object (#63, #58).
* Public API specification (#64).
* Update project setup.
* Fix typing and tests (#52).

0.1.2 (2022-10-05)
------------------

* Restrict supported pydantic versions.

0.1.1 (2022-09-29)
------------------

A number of changes and fixes for the initial version.

0.1.0 (2022-09-19)
------------------

Initial version.
