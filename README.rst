==========
Fred-eppic
==========

Fred-eppic is EPP Interactive Client.

Learn more about the project and our community on the `FRED's home page`__.

.. _FRED: https://fred.nic.cz
.. _Namespaces and Schemas: https://fred.nic.cz/documentation/html/EPPReference/SchemasNamespaces/index.html
.. _Python Logging Dict Schema Wiki: https://docs.python.org/3/library/logging.config.html#logging-config-dictschema

__ FRED_

Usage
===========

For info about usage, read the `manpage <man/main.rst>`_.

Configuration
=============

For further info about configuration, read the `configuration manpage <man/config.rst>`_.

Public API specification
========================

Public API of this application marked as compatible for the purpose of semantic versioning is a command-line interface.
Specifically:

* Command names, arguments and behaviour, regardless of the input format.
* Structure of command outputs, with respect to the format used, in all formats except for the text output.
* Structure of the configuration file.

Changes excluded from public API compatibility:

* Any features marked as experimental.
* Command behaviour changes caused by changes on the EPP server, especially changes in EPP schemas.
* Interactive prompt features, such as history, autocompletion and command validation.
