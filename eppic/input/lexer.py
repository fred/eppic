"""Lex the commands."""
from pygments.lexer import RegexLexer, bygroups
from pygments.token import Error, Keyword, Name, Operator, String, Whitespace


class Lexer(RegexLexer):
    """Provide lexer for input."""

    name = "Command and argument lexer"

    tokens = {
        "root": [
            (r'^[^ ]+', Keyword),
            (r'\n', Whitespace),
            (r'( +)(--)([^ =]*)([ =])(\')',
                bygroups(Whitespace, Operator, Name.Attribute, Operator, String.Single), "string_single"),
            (r'( +)(--)([^ =]*)([ =])(")',
                bygroups(Whitespace, Operator, Name.Attribute, Operator, String.Double), "string_double"),
            (r'( +)(--)([^ =]*)([ =]{0,1})', bygroups(Whitespace, Operator, Name.Attribute, Operator), "string"),
            (r'( +)(")', bygroups(Whitespace, String.Double), "string_double"),
            (r"( +)(')", bygroups(Whitespace, String.Single), "string_single"),
            (r'( +)', Whitespace, "string"),
        ],
        "string_double": [
            (r'\\\S{0,1}', String.Escape),
            (r'"', String.Double, "#pop"),
            (r'([^"\\\n]*)(\n)', bygroups(String, Error)),
            (r'[^"\\]+', String),
        ],
        "string_single": [
            (r'\\\S{0,1}', String.Escape),
            (r"'", String.Single, "#pop"),
            (r'([^\'\\\n]*)(\n)', bygroups(String, Error)),
            (r"[^\\']+", String),
        ],
        "string": [
            (r'([^ \\\n]*)(\\\S{0,1})', bygroups(String, String.Escape)),
            (r'[^ \\\n]+', String, "#pop"),
        ],
    }
