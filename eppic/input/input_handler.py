"""InputHandler class."""
import json
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Callable, Dict, Iterator, Optional, TextIO, Union, cast

import yaml
from prompt_toolkit import PromptSession
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
from prompt_toolkit.completion import Completer
from prompt_toolkit.history import FileHistory
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.key_binding.key_processor import KeyPressEvent
from prompt_toolkit.lexers import PygmentsLexer
from prompt_toolkit.validation import Validator

from ..settings import InteractiveSettings
from .lexer import Lexer


class InputHandler(ABC):
    """Gets raw input."""

    @abstractmethod
    def __iter__(self) -> Iterator:
        """Return iterator."""

    def set_settings(self, settings: InteractiveSettings) -> None:  # noqa: B027
        """Update the handler to the new interactive settings."""


class CliArgsInputHandler(InputHandler):
    """Gets raw input from args, which it got from constructor."""

    def __init__(self, args: Optional[str]) -> None:
        """Save arguments."""
        self._args: Optional[str] = args

    def __iter__(self) -> Iterator[str]:
        """Return the arguments, then stop."""
        if self._args is not None:
            yield self._args


class FileInputHandler(InputHandler):
    """Gets raw input from a file."""

    def __init__(self, io: TextIO) -> None:
        """Check and save the path to the file."""
        self._lines = [x.strip() for x in io.readlines() if x.strip() != ""]

    def __iter__(self) -> Iterator[str]:
        """Return next line in file."""
        yield from self._lines


class JsonInputHandler(InputHandler):
    """Gets json input from a file."""

    def __init__(self, io: TextIO) -> None:
        """Check and save the path to the file."""
        self._lines = [x.strip() for x in io.readlines() if x.strip() != ""]

    def __iter__(self) -> Iterator[Dict]:
        """Iterate over JSON."""
        for x in self._lines:
            yield cast(dict, json.loads(x))


class YamlInputHandler(InputHandler):
    """Get yaml input from a file."""

    def __init__(self, io: TextIO) -> None:
        self._parser = yaml.safe_load_all(io.read())

    def __iter__(self) -> Iterator[Dict]:
        """Iterate over yaml documents."""
        yield from self._parser


class PromptToolkitInputHandler(InputHandler):
    """Get input from prompt_toolkit."""

    def __init__(self, completer: Optional[Completer], validator: Validator, history_path: Optional[Path] = None,
                 prompt: Union[str, Callable[[], str]] = "> ") -> None:
        """Create prompt session."""
        self.history = FileHistory(str(history_path.expanduser())) if history_path is not None else None

        if history_path is not None:
            history_path.expanduser().parent.mkdir(parents=True, exist_ok=True)
            history_path.expanduser().touch(exist_ok=True)

        self.bindings = KeyBindings()
        self.bindings.add("c-@")(self.help_keybind)

        self.lexer = PygmentsLexer(Lexer)
        self.completer = completer
        self.raw_prompt = prompt
        self.validator = validator

        self.prompt: PromptSession = PromptSession(
            self.raw_prompt,
            key_bindings=self.bindings,
            history=self.history,
            auto_suggest=AutoSuggestFromHistory(),
        )

    def set_settings(self, settings: InteractiveSettings) -> None:  # noqa: B027
        """Update the handler to the new interactive settings."""
        self.prompt = PromptSession(
            self.raw_prompt if settings.enable_prompt else None,
            key_bindings=self.bindings,
            history=self.history,
            auto_suggest=AutoSuggestFromHistory(),
            validator=self.validator if settings.online_validation else None,
            lexer=self.lexer if settings.syntax_highlighting else None,
            completer=self.completer if settings.autocomplete else None,
            vi_mode=settings.vi_mode,
        )

    def __iter__(self) -> Iterator[str]:
        """Fetch input from the user."""
        while True:
            try:
                prompted = cast(str, self.prompt.prompt())
                if prompted:
                    yield prompted.strip()
            except KeyboardInterrupt:  # press ctrl-c
                pass
            except EOFError:  # press ctrl-d
                return

    def help_keybind(self, event: KeyPressEvent) -> None:
        """Display help for command.

        This method saves the currently written text to the history, then
        pre-pends it with `help ` and forces it to execute.
        """
        event.current_buffer.append_to_history()  # append the original command to history for easier usage
        event.current_buffer.transform_current_line(lambda x: "help " + x)
        event.current_buffer.validate_and_handle()
