"""The prompt_toolkit completer."""
import operator
from typing import TYPE_CHECKING, Any, Generator, Iterable, List, Optional, Tuple, cast

from prompt_toolkit.completion import Completer as BaseCompleter, Completion
from prompt_toolkit.document import Document
from prompt_toolkit.validation import ValidationError, Validator as BaseValidator

from eppic.input.input_processor import InputProcessor
from eppic.models import ArgumentLabels, ArgumentLabelSuggestion

if TYPE_CHECKING:
    from eppic.commands.command_registry import CommandRegistry


class CommandInfoMixin:
    """A mixin for PromptToolkit classes that handles communication between commands and the document."""

    def __init__(self, *, command_registry: "CommandRegistry", input_processor: InputProcessor, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self._command_registry = command_registry
        self._input_processor = input_processor

    def last_space(self, document: Document) -> Optional[int]:
        """Get last space (or None if first word)."""
        return document.find_backwards(" ")

    def last_word(self, document: Document) -> str:
        """Get last word from space/equal sign to the cursor."""
        last_space = self.last_space(document)
        current_line = document.current_line_before_cursor
        return current_line[last_space:].strip() if last_space is not None else current_line.strip()

    def list_commands(self, document: Document, match: Optional[str] = None) -> Generator[str, None, None]:
        """Get list of possible commands."""
        for command in self._command_registry.registered_command_names:
            if match is None or match in command:
                yield command

    def last_argument_has_no_value(self, document: Document) -> bool:
        """Check whether the last argument has value or not."""
        cmd = self._input_processor(document.current_line_before_cursor[:self.last_space(document)])
        return bool(cmd.raw_args) and bool(cmd.raw_args[-1]) and cmd.raw_args[-1][-1] is None

    def list_arguments(self, document: Document) -> Generator[ArgumentLabelSuggestion, None, None]:
        """Get list of still possible arguments."""
        input = document.current_line_before_cursor[:self.last_space(document)]
        cmd = self._input_processor(input)
        arg_parser = self._command_registry.get_command(cmd.name).arg_parser
        parsed_command = arg_parser.parse_arguments(cmd)
        yield from sorted(arg_parser.get_available_labels(list(parsed_command.raw_args)),
                          key=operator.attrgetter('required'), reverse=True)


class Completer(CommandInfoMixin, BaseCompleter):
    """Generate completions."""

    def get_completions(self, document: Document, completion_event: Any) -> Generator:
        """Get completions for document."""
        try:
            generator = self._command_completions if self.last_space(document) is None else self._argument_completions
            yield from generator(document)
        except Exception:  # noqa: S110 # The Validator will handle this, we just want completions.
            pass

    def _command_completions(self, document: Document) -> Generator:
        """Generate command completions."""
        last_word = self.last_word(document)
        for command in self.list_commands(document, match=last_word):
            yield Completion(command, -len(command))

    def _argument_completions(self, document: Document) -> Generator:
        """Generate argument completions."""
        if self.last_argument_has_no_value(document):
            return
        last_word = self.last_word(document)
        for name, values, required in self.list_arguments(document):
            for completion_name, completion_style in self._single_argument_completions(name, values, required):
                if last_word == "" or last_word in completion_name:
                    yield Completion(completion_name,
                                     display=completion_style, start_position=cast(int, self.last_space(document)) + 1)

    def _required_filter(self, val: str, required: bool) -> str:
        return val if required else f"({val})"

    def _single_argument_completions(self, labels: ArgumentLabels,
                                     values: Optional[List[str]], required: bool) -> Iterable[Tuple[str, str]]:
        values = values or []
        name = f"--{'.'.join(labels)}="
        yield name, self._required_filter(name, required)

        for value in values:
            valued_name = name + value
            yield valued_name, self._required_filter(valued_name, required)


class Validator(BaseValidator, CommandInfoMixin):
    """Validate input."""

    def validate(self, document: Document) -> None:
        """Validate for document."""
        if document.is_cursor_at_the_end and self.last_space(document) is None:
            # Do nothing if there's nothing to validate yet.
            return

        self._validate_command(document)

        try:
            self._validate_arguments(document.text)
        except ValidationError:
            if document.is_cursor_at_the_end:
                # There was an error, but it may be caused by unfinished command. Try to cut it.
                line = document.current_line_before_cursor[:self.last_space(document)]
                self._validate_arguments(line)
            else:
                raise

    def _validate_command(self, document: Document) -> None:
        """Validate an input of partial command."""
        parts = document.text.partition(' ')
        if not list(self.list_commands(document, match=parts[0])):
            raise ValidationError(message="Command not found")

    def _validate_arguments(self, text: str) -> None:
        """Validate an input of partial command."""
        try:
            cmd = self._input_processor(text)
            arg_parser = self._command_registry.get_command(cmd.name).arg_parser
            cmd = arg_parser.parse_arguments(cmd)
            if cmd.error:
                raise ValueError(cmd.error)
        except ValueError as e:
            raise ValidationError(message=str(e)) from e
