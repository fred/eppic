"""Input Processor class."""
from abc import ABC, abstractmethod
from shlex import split
from typing import Any, Dict, Generic, Iterable, List, Optional, Tuple

from eppic.models import ArgumentLabels, ParsedArgument, ParsedCommand, RawCommandType


class InputProcessor(ABC, Generic[RawCommandType]):
    """Parses raw input into the command name and args."""

    def __call__(self, raw_in: RawCommandType) -> ParsedCommand[RawCommandType]:
        """Get the input."""
        cmd, args = self._get_command_name_and_raw_args(raw_in)
        return ParsedCommand(raw_in, name=cmd, raw_args=tuple(self._get_args_generator(args)))

    @abstractmethod
    def _get_command_name_and_raw_args(self, raw_in: RawCommandType) -> Tuple[str, Any]:
        """Get command name."""

    @abstractmethod
    def _get_args_generator(self, args: Any) -> Iterable[ParsedArgument]:
        """Create a generator for arguments."""


class StringInputProcessor(InputProcessor[str]):
    """Process string input."""

    def _get_command_name_and_raw_args(self, raw_in: str) -> Tuple[str, str]:
        """Unpack command into command name and arguments."""
        if " " in raw_in:
            name, args = raw_in.split(" ", 1)
            return name, args
        return raw_in, ""

    def _split_labels(self, arg_name: str) -> List[str]:
        """Split `arg_name` to labels, remove `--` from prefix."""
        return arg_name[2:].split(".") if arg_name != "--" else []

    def _get_args_generator(self, args: str) -> Iterable[ParsedArgument]:
        """Split arguments string into `ParsedArgument`s."""
        next_labels: Optional[ArgumentLabels] = None
        for arg in split(args):
            # the entire `arg` is a value
            if next_labels is not None or not arg.startswith("--"):
                yield ParsedArgument(labels=next_labels, value=arg)
                next_labels = None
                continue
            # = in arg means it contains both labels and value
            elif '=' in arg:
                split_arg = arg.split("=", 1)
                yield ParsedArgument(labels=self._split_labels(split_arg[0]), value=split_arg[1])
            # there are only labels, value will be next token
            else:
                next_labels = self._split_labels(arg)

        if next_labels is not None:
            yield ParsedArgument(labels=next_labels, value=None)


class DictInputProcessor(InputProcessor[Dict[str, Any]]):
    """Process dict input."""

    def _get_command_name_and_raw_args(self, raw_in: Dict[str, Any]) -> Tuple[str, Any]:
        """Unpack command into command name and arguments."""
        if len(raw_in.keys()) != 1:
            raise ValueError("Only accepted one command per dictionary.")

        key = list(raw_in.keys())[0]
        return key, raw_in[key]

    def _get_args_generator(self, args: Any) -> Iterable[ParsedArgument]:
        """Get `ParsedArgument`s from the input."""
        for key, value in args.items():
            if isinstance(value, dict):

                # this basically flattens the inner dictionaries...
                for subkey, subvalue in self._get_args_generator(value):
                    yield ParsedArgument(labels=[key] + subkey if subkey is not None else [key], value=subvalue)

            elif isinstance(value, list):

                for i, list_value in enumerate(value):
                    yield from self._get_args_generator({f"{key}_{i}": list_value})

            else:
                yield ParsedArgument(labels=[key], value=value)
