"""Command hooks."""
from typing import TYPE_CHECKING, List

from epplib.commands import CreateContact, UpdateContact
from epplib.commands.list import GetResults
from epplib.models.list import GetResultsResultData
from epplib.responses import Result

from eppic.models import Request

if TYPE_CHECKING:
    from eppic.app import Application


def get_results_hook(app: 'Application', request: Request) -> None:
    """Print results immediately after list commands.

    This hook doesn't run if `app.settings.print_list_results_automatically` is `False`.
    """
    if not app.settings.print_list_results_automatically:
        return

    epp_handler = app.epp_handler
    res_data: List[GetResultsResultData] = []
    while True:
        response = epp_handler.send_request(GetResults())
        assert isinstance(response, Result)  # noqa: S101
        if response.code != 1000 or not response.res_data:
            break
        res_data += response.res_data
    response.res_data = res_data
    app.output_handler.output(response)


def apply_disclose_policy(app: 'Application', request: Request) -> None:
    """Apply the disclose policy according to the `epp_handler`'s.

    Requires `epp_command` to have a `disclose` field.
    """
    assert isinstance(request, (CreateContact, UpdateContact))  # noqa: S101

    policy = app.epp_handler.policy
    if policy is not None and request.disclose is not None:
        request.disclose = policy.apply_disclose_policy(request.disclose)
