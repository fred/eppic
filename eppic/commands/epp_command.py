"""A `Command` class for executing Epplib commands."""
import logging
from typing import TYPE_CHECKING, Any, Dict, Iterable, List, Optional, Type, TypeVar

from click import confirm
from epplib.commands import Command as EpplibCommand, Logout, Request as EpplibRequest

from eppic.models import ArgumentField

from .argument_registry import ArgumentRegistry
from .command import Command

if TYPE_CHECKING:
    from eppic.app import Application

LOGGER = logging.getLogger(__name__)

EppRequestT = TypeVar("EppRequestT", bound=EpplibRequest)


class EppCommand(Command[EppRequestT]):
    """A command, which sends EPP commands to the server."""

    def __init__(self,
                 name: str,
                 request_class: Type[EppRequestT],
                 registry: ArgumentRegistry,
                 *,
                 extensions: Optional[Iterable[ArgumentField]] = None,
                 **kwargs: Any
                 ) -> None:
        extensions = list(extensions) if extensions else []
        self._extensions_list = tuple(f.name for f in extensions)
        self._next_cltrid: Optional[str] = None
        super().__init__(name, request_class, registry,
                         extra_arguments=extensions + [ArgumentField("cltrid", str, False)],
                         **kwargs)

    def _pop_extensions(self, arguments: Dict[str, Any]) -> List[Any]:
        """Pop the extensions from arguments and return them.

        Arguments:
            arguments: Arguments and extensions.

        Note: The extensions will be removed from `arguments`.

        Returns: the extracted extensions.
        """
        ret = []
        for key in list(arguments.keys()):
            if key in self._extensions_list:
                extension = arguments.pop(key)
                if extension is not None:
                    ret.append(extension)
        return ret

    def _prepare_request(self, arguments: Dict[str, Any]) -> EppRequestT:
        """Build the Request Class and return it.

        Arguments:
            arguments: Already parsed arguments of the command.
        """
        cltrid = arguments.pop("cltrid") if "cltrid" in arguments.keys() else None
        extensions = self._pop_extensions(arguments)
        command = super()._prepare_request(arguments)

        for extension in extensions:
            # only `epplib.command.Command`s can have extensions
            assert isinstance(command, EpplibCommand)  # noqa: S101
            LOGGER.debug("Extension: %s", extension)
            command.add_extension(extension)

        # cannot be returned, as it would violate the supertype's type signature
        self._next_cltrid = cltrid
        return command

    def _run(self, app: "Application", request: EppRequestT) -> None:
        """Run the command."""
        if app.settings.interactive.command_confirmation and not self._confirm_request(request):
            return

        # get the cltrid from `_prepare_request` and immidiately reset it again
        cltrid = self._next_cltrid
        self._next_cltrid = None

        response = app.epp_handler.send_request(request, cltrid)
        LOGGER.debug(response)
        app.output_handler.output(response)

    def _confirm_request(self, request: EppRequestT) -> bool:
        """Fetch command confirmation of an Epp request."""
        print(request)  # give the user information about the request.
        return confirm("Send command?")


class LogoutCommand(EppCommand[Logout]):
    """Logout command wrapper."""

    def _run(self, app: "Application", request: Logout) -> None:
        """Run the command."""
        if app.settings.interactive.command_confirmation and not self._confirm_request(request):
            return

        # get the cltrid from `_prepare_request` and immediately reset it again
        cltrid = self._next_cltrid
        self._next_cltrid = None

        app.epp_handler.logout(request, cltrid=cltrid)
