"""A module parsing arguments and putting them together."""
from collections import defaultdict
from itertools import chain
from logging import getLogger
from typing import Dict, Iterable, List, Optional, Type, Union, cast, get_args, get_origin

from eppic.models import ArgumentField, ArgumentLabelSuggestion, ParsedArgument, ParsedCommand

from .argument_registry import ArgumentRegistry
from .arguments.argument import Argument

LOGGER = getLogger(__name__)


def _unpack_required_type(argument_type: Type) -> Type:
    """Turn a potentially optional type into a type.

    Arguments:
        argument_type: The type to unpack.

    Returns: the unpacked type.
    """
    args = list(get_args(argument_type))
    origin = get_origin(argument_type)

    if origin == Union and type(None) in args:
        args.remove(type(None))
        argument_type = args.pop(0)

        for more in args:
            argument_type = cast(Type, Union[argument_type, more])

        return argument_type

    return argument_type


class ArgumentParser:
    """A class responsible for parsing arguments."""

    def __init__(self, fields: Iterable[ArgumentField], registry: ArgumentRegistry,
                 number_of_positional_args: int) -> None:
        """Save arguments and initialize arguments list.

        Arguments:
            fields: An iterable of argument fields.
            registry: An ArgumentRegistry instance containing all the registered arguments.
            number_of_positional_args: Says that `n` first arguments are allowed to be positional.
        """
        self._fields = tuple(fields)
        self._registry = registry
        self._init_arguments_list()
        self._number_of_positional_args = number_of_positional_args

    def _create_argument(self, field: ArgumentField) -> Argument:
        """Get an argument from registry and initialize it.

        Arguments:
            field: Argument field.

        Returns: an Argument instance.
        """
        argument_type = _unpack_required_type(field.type)
        argument_class = self._registry.get_argument(argument_type)

        return argument_class(ArgumentField(field.name, argument_type, bool(field.required)), self._create_argument)

    def _init_arguments_list(self) -> None:
        """Initialize the necessary properties before every parsing."""
        self._arguments_list = [self._create_argument(f) for f in self._fields]

    def get_next_labels_suggestion(self, labeled_parsed_args: List[ParsedArgument],
                                   positional_args: List[Argument]) -> Optional[ArgumentLabelSuggestion]:
        """Get the next available labels.

        Returns `None` if no label exists.
        """
        available_labels = chain.from_iterable(
            x.get_available_labels(labeled_parsed_args) for x in positional_args)
        return next(available_labels, None)

    def _dereference_arguments(self, args: List[ParsedArgument]) -> Iterable[ParsedArgument]:
        """Dereference positional arguments."""
        # get the available labels to be filled in.
        labeled_parsed_args = [x for x in args if x.labels is not None]
        positional_args = self._arguments_list[:self._number_of_positional_args]

        # Dereference positional arguments
        for arg in args:
            # `arg` is not a positional argument.
            if arg.labels is not None:
                pass

            # `arg` is a positional argument, but there are no labels left to give it.
            elif (next_suggestion := self.get_next_labels_suggestion(labeled_parsed_args, positional_args)) is None:
                raise ValueError("Too many positional arguments received.")

            else:
                # Choose the first available label.
                arg = ParsedArgument(next_suggestion.labels, arg.value)

            if arg.value is None:
                raise ValueError(f"Argument {arg.labels} didn't receive a value.")

            labeled_parsed_args.append(arg)
            yield arg

    def _group_arguments(self, cmd: ParsedCommand,
                         arguments: Iterable[ParsedArgument]) -> Dict[str, List[ParsedArgument]]:
        """Group arguments by the `Argument` instances that recognise them."""
        groups = defaultdict(list)

        for argument in arguments:
            for arg_instance in self._arguments_list:
                if arg_instance.recognise_label(cast(List[str], argument.labels)):
                    groups[arg_instance.label].append(argument)
                    break
            else:
                cmd.add_error(f"Argument {argument.labels} not found.")

        return groups

    def parse_arguments(self, cmd: ParsedCommand) -> ParsedCommand:
        """Parse raw arguments from the command to the command arguments.

        Note: Updates the parsed command instance on input and returns it on output.

        Arguments:
            cmd: Parsed command with raw data only.

        Returns: Parsed command, possibly with the parsing error.
        """
        self._init_arguments_list()

        # Dereference positional arguments.
        try:
            dereferenced_args = list(self._dereference_arguments(list(cmd.raw_args)))
        except ValueError as e:
            cmd.add_error(str(e))
            return cmd
        LOGGER.debug("Dereferenced arguments: %s", dereferenced_args)

        # Group arguments -- assign each `ParsedArgument` to its `Argument` instance.
        arg_groups = self._group_arguments(cmd, dereferenced_args)
        LOGGER.debug("Argument groups: %s.", list(arg_groups.keys()))

        # Parse arguments.
        cmd.args = {}
        for argument in self._arguments_list:
            # Find parsed arguments for the argument instance.
            corresponding_arguments = arg_groups[argument.label]

            LOGGER.debug("Parsing argument %s of type %s.", argument.label, type(argument))
            try:
                value = argument.parse_value(corresponding_arguments)
            except Exception as error:
                cmd.add_error(str(error))
            else:
                cmd.args[argument.original_label] = value

        return cmd

    def get_available_labels(self, parsed_arguments: List[ParsedArgument]) -> List[ArgumentLabelSuggestion]:
        """Get available argument labels, potentially with values, and whether they're required."""
        dereferenced_args = list(self._dereference_arguments(parsed_arguments))

        ret = []
        for argument in self._arguments_list:
            ret += argument.get_available_labels(dereferenced_args)

        return ret
