"""Service requests.

These requests aren't sent to the server, but are used for other tasks,
such as manipulation of the session, changing settings etc.
"""
import logging
import sys
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import TYPE_CHECKING, List, Optional, cast

from eppic.models import SessionId
from eppic.output import OutputFormat as OutputFormatT
from eppic.settings import OutputFormat, PrintRawXml, Session, Settings

if TYPE_CHECKING:
    from eppic.app import Application

LOGGER = logging.getLogger(__name__)


class ServiceRequest(ABC):
    """Base class. Provides information to the app."""

    @abstractmethod
    def handle(self, app: "Application") -> None:
        """Handle the service request."""


class ServiceExit(ServiceRequest):
    """Allows the program to exit."""

    def handle(self, app: "Application") -> None:
        """Handle the service request."""
        app.epp_handler.logout()
        sys.exit(0)


@dataclass
class ServiceLogin(ServiceRequest):
    """Log into an existing session."""

    session: Optional[SessionId] = None
    session_id: Optional[SessionId] = None

    def handle(self, app: "Application") -> None:
        """Handle the service request."""
        if self.session_id is not None:
            app.print_warning("Argument --session-id is deprecated. Use --session instead.")
        if self.session is None:
            session_id = self.session_id
        else:
            session_id = self.session
        if isinstance(session_id, int):
            app.print_warning("Numeric values for --session argument are deprecated. Use aliases instead.")
        session = app.settings.get_session(session_id)

        if session is None:
            # `get_session` only returns `None` if `settigs.sessions` is an empty list.
            raise ValueError("No sessions exist.")
        else:
            app.epp_handler.login(session)


@dataclass
class ServiceNewSession(ServiceRequest):
    """Create new Session instance."""

    base_session: Optional[SessionId] = None
    base_session_id: Optional[SessionId] = None

    username: Optional[str] = None
    password: Optional[str] = None
    hostname: Optional[str] = None
    port: Optional[int] = None
    cert_file: Optional[str] = None
    key_file: Optional[str] = None
    obj_uris: Optional[List[str]] = None
    verify: Optional[bool] = None
    alias: Optional[str] = None
    reconnects: Optional[int] = None
    # Whether to perform login to the new session.
    login: Optional[bool] = None

    def modify_session(self, session: Session) -> Session:
        """Modify the session from the object attributes.

        If an attribute is not set, take it from the base `session`.
        (`alias` is the exception of this -- if it isn't set, don't set it at all)
        (`obj_uris` is another exception -- it isn't compared to `None`, but to `bool`)
        """
        username = self.username if self.username is not None else session.username
        password = self.password if self.password is not None else session.password
        hostname = self.hostname if self.hostname is not None else session.hostname
        port = self.port if self.port is not None else session.port
        cert_file = self.cert_file if self.cert_file is not None else session.cert_file
        key_file = self.key_file if self.key_file is not None else session.key_file
        # obj_uris are set based on whether the're an empty list (which would be an invalid state)
        obj_uris = self.obj_uris if self.obj_uris else session.obj_uris
        verify = self.verify if self.verify is not None else session.verify
        alias = self.alias
        reconnects = self.reconnects if self.reconnects is not None else session.reconnects

        return Session(
            username=username, password=password, hostname=hostname,
            port=port, cert_file=cert_file, key_file=key_file, obj_uris=obj_uris,
            verify=verify, alias=alias, reconnects=reconnects
        )

    def handle(self, app: "Application") -> None:
        """Handle the service request."""
        if self.base_session_id is not None:
            app.print_warning("Argument --base-session-id is deprecated. Use --base-session instead.")
        if self.base_session is None:
            base_session_id = self.base_session_id
        else:
            base_session_id = self.base_session
        if isinstance(base_session_id, int):
            app.print_warning("Numeric values for --base-session argument are deprecated. Use aliases instead.")
        if base_session_id is None:
            session = Session()
        else:
            session = cast(Session, app.settings.get_session(base_session_id))

        session = self.modify_session(session)
        session_number = app.add_session(session)
        LOGGER.info("Session added as number %s.", session_number)

        if self.login is None:
            app.print_warning("--login default will change to 'no'.")
            self.login = True
        if self.login:
            app.epp_handler.login(session)


@dataclass
class ServiceChangeSettings(ServiceRequest):
    """Change settings on the fly."""

    output_format: Optional[OutputFormat] = None
    autocomplete: Optional[bool] = None
    syntax_highlighting: Optional[bool] = None
    command_confirmation: Optional[bool] = None
    vi_mode: Optional[bool] = None
    online_validation: Optional[bool] = None
    print_raw_xml: Optional[PrintRawXml] = None

    def edit_settings(self, settings: Settings) -> None:
        """Edit the settings with gathered arguments."""
        settings.output_format = self.output_format if self.output_format is not None else settings.output_format
        settings.interactive.autocomplete = self.autocomplete if self.autocomplete is not None \
            else settings.interactive.autocomplete
        settings.interactive.syntax_highlighting = self.syntax_highlighting if self.syntax_highlighting is not None \
            else settings.interactive.syntax_highlighting
        settings.interactive.command_confirmation = self.command_confirmation if self.command_confirmation is not None \
            else settings.interactive.command_confirmation
        settings.interactive.vi_mode = self.vi_mode if self.vi_mode is not None \
            else settings.interactive.vi_mode
        settings.print_raw_xml = self.print_raw_xml if self.print_raw_xml is not None \
            else settings.print_raw_xml
        settings.interactive.online_validation = self.online_validation if self.online_validation is not None \
            else settings.interactive.online_validation

    def handle(self, app: "Application") -> None:
        """Handle the service request."""
        settings = app.settings
        self.edit_settings(settings)
        app.set_settings(settings)


@dataclass
class ServiceListSessions(ServiceRequest):
    """List configured sessions."""

    def handle(self, app: "Application") -> None:
        """Handle the service request."""
        sessions = [session.info_dict() for session in app.settings.sessions]
        app.output_handler.output_data(OutputFormatT(msg=None, code=None, data=sessions))
