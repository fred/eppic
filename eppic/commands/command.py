"""Command class responsible for recognising user input and arguments."""
import logging
from abc import ABC, abstractmethod
from dataclasses import fields, is_dataclass
from itertools import chain
from typing import TYPE_CHECKING, Any, Dict, Generic, Iterable, List, Optional, Type, TypeVar, cast

from eppic.models import ArgumentField, Request

from .argument_parser import ArgumentParser
from .argument_registry import ArgumentRegistry

if TYPE_CHECKING:
    from eppic.app import Application, CommandHook

LOGGER = logging.getLogger(__name__)

RequestT = TypeVar("RequestT", bound=Request)


class Command(ABC, Generic[RequestT]):
    """Command class."""

    def __init__(
        self,
        name: str,
        request_class: Type[RequestT],
        registry: ArgumentRegistry,
        *,
        pre_hooks: Optional[List["CommandHook"]] = None,
        post_hooks: Optional[List["CommandHook"]] = None,
        number_of_positional_args: int = 0,
        extra_arguments: Iterable[ArgumentField] = [],
    ) -> None:
        """Initialize the command from preprocessed parameters.

        Arguments:
            name: The name of the command.
            request_class: The class that is returned filled with arguments when the command is called.
            registry: An ArgumentRegistry instance.
            pre_hooks: A list of hooks to be run before a command.
            post_hooks: A list of hooks to be run after a command.
            number_of_positional_args: Says that `n` first arguments are allowed to be positional. Defaults to 0.
            extra_arguments: An iterable of extra arguments to be passed to the argument parser.
        """
        self.name = name
        self._request_class = request_class
        arg_fields = (ArgumentField.from_dataclass_field(field) for field in fields(request_class)
                      ) if is_dataclass(request_class) else ()
        self.arg_parser = ArgumentParser(chain(arg_fields, extra_arguments), registry, number_of_positional_args)
        self._pre_hooks = pre_hooks or []
        self._post_hooks = post_hooks or []

    def _prepare_request(self, arguments: Dict[str, Any]) -> RequestT:
        """Build the Request class and return it.

        Arguments:
            arguments: Already parsed arguments of the command.
        """
        return cast(RequestT, self._request_class(**arguments))

    def _run_pre_hooks(self, app: "Application", request: RequestT) -> None:
        """Run the pre-hooks."""
        for hook in self._pre_hooks:
            hook(app, request)

    def _run_post_hooks(self, app: "Application", request: RequestT) -> None:
        """Run the post-hooks."""
        for hook in self._post_hooks:
            hook(app, request)

    def run(self, app: "Application", args: Dict) -> None:
        """Run the command."""
        LOGGER.debug("Preparing request...")
        request = self._prepare_request(args)
        LOGGER.debug("Executing pre-hooks...")
        self._run_pre_hooks(app, request)
        LOGGER.debug("Executing request...")
        self._run(app, request)
        LOGGER.debug("Executing post-hooks...")
        self._run_post_hooks(app, request)

    @abstractmethod
    def _run(self, app: "Application", request: RequestT) -> None:
        """Run the command itself."""
