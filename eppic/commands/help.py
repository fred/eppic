"""Provide help for users."""
import logging
from pathlib import Path
from typing import TYPE_CHECKING, Dict, List, Tuple, cast

import yaml

from eppic.models import ArgumentLabelSuggestion, ParsedArgument, ParsedCommand

from .argument_parser import ArgumentParser
from .command_registry import CommandRegistry
from .service_command import ServiceCommand
from .service_requests import ServiceRequest

if TYPE_CHECKING:
    from eppic.app import Application

LOGGER = logging.getLogger(__name__)
HELP_PATH = Path(__file__).parent.parent / "help.yaml"


class HelpArgumentParser(ArgumentParser):
    """Parse arguments of the help command."""

    def __init__(self, registry: CommandRegistry) -> None:
        """Save arguments and initialize arguments list.

        Arguments:
            arguments: A dict of arguments with their types.
            registry: An CommandRegistry instance containing all the registered commands.

        """
        self._command_registry = registry

    def _get_command_name_and_args(self, arguments: List[ParsedArgument]) -> Tuple[str, List[ParsedArgument]]:
        """Get the name of the command user is looking for and its supplied arguments."""
        arguments = list(arguments)

        # if there are no arguments, default to an empty name -- the most general
        command_name = ""

        # if there are arguments, the command name will be the value of the first one.
        if len(arguments):
            first_argument = arguments.pop(0)
            command_name = first_argument.value or ""

        return command_name, arguments

    def parse_arguments(self, cmd: ParsedCommand) -> ParsedCommand:
        """Parse raw arguments from the command to the command arguments."""
        command_name, arguments = self._get_command_name_and_args(list(cmd.raw_args))

        # if `command_name` matches more commands, show all their helps.
        if command_name not in self._command_registry.registered_command_names:
            cmd.args = self._get_matching_commands_possible_arguments(command_name)
        else:
            # if it matches only one command in the database, show its help with argument help.
            cmd.args = self._get_single_command_possible_arguments(command_name, arguments)

        return cmd

    def _get_matching_commands_possible_arguments(self, command_name: str) -> Dict[str, List[ArgumentLabelSuggestion]]:
        ret = {}
        for command in self._command_registry.registered_command_names:
            if command_name in command:
                arguments = self._get_single_command_possible_arguments(command, [])
                ret[command] = arguments[command]

        if not ret.keys() and command_name != "":  # wrong command name.
            return self._get_matching_commands_possible_arguments("")

        return ret

    def _get_single_command_possible_arguments(
            self, command_name: str, command_arguments: List[ParsedArgument]
    ) -> Dict[str, List[ArgumentLabelSuggestion]]:
        if command_name == "help":
            return {command_name: [ArgumentLabelSuggestion(["command, arguments"], None, True)]}

        command = self._command_registry.get_command(command_name)
        arg_parser = command.arg_parser
        return {command_name: arg_parser.get_available_labels([])}

    def get_available_labels(self, parsed_arguments: List[ParsedArgument]) -> List[ArgumentLabelSuggestion]:
        """Get available argument labels, potentially with values, and whether they're required."""
        command_name, arguments = self._get_command_name_and_args(parsed_arguments)

        # if the user didn't specify a known command name, give him suggestions for all the names.
        if command_name not in self._command_registry.registered_command_names:
            return [ArgumentLabelSuggestion(["command"], self._command_registry.registered_command_names, True)]

        return self._command_registry.get_command(command_name).arg_parser.get_available_labels(arguments)


class ServiceHelp(ServiceRequest):
    """Print help."""

    help_path = HELP_PATH

    def __init__(self, **kwargs: List[ArgumentLabelSuggestion]) -> None:
        """Save commands."""
        self.commands = kwargs
        self._help = Help(self.help_path)

    def handle(self, app: "Application") -> None:
        """Print help."""
        for command, command_arguments in self.commands.items():
            print(self._help_command(command, command_arguments))

    def _help_command(self, command: str, arguments: List[ArgumentLabelSuggestion]) -> str:
        helps = [self._help.get_help(command)]

        for argument_labels, values, required in arguments:
            argument = ".".join(argument_labels)
            helps.append(self._help.get_help(command, argument, required=required))

            for value in values or []:
                helps.append(self._help.get_help(command, argument, value))

        return "\n".join(helps)


class HelpCommand(ServiceCommand[ServiceHelp]):
    """A representation of the help command."""

    def __init__(self, registry: CommandRegistry) -> None:
        """Init command."""
        self.name = "help"
        self._request_class = ServiceHelp
        self.arg_parser = HelpArgumentParser(registry)
        self._pre_hooks = []
        self._post_hooks = []


class Help:
    """Get and parse help from file."""

    indent_multiplier = 4
    max_level = 3

    def __init__(self, input_file: Path = HELP_PATH) -> None:
        """Load help."""
        self._help = {}

        input_file = input_file.expanduser()
        if not input_file.is_file():
            LOGGER.warning("Help file not found.")
            return

        with open(input_file) as f:
            self._help = yaml.safe_load(f)

    def get_help(self, *labels_tuple: str, required: bool = True) -> str:
        """Get formatted help."""
        labels = list(labels_tuple[:self.max_level])
        indent = len(labels) - 1
        if not labels:
            return ""

        name = labels[-1] if required else f"({labels[-1]})"
        try:
            command = self._help[labels.pop(0)]
            if not labels:
                return self._format(indent, name, command["description"])

            argument = command["arguments"][labels.pop(0)]
            if not labels:
                ret = argument if isinstance(argument, str) else cast(str, argument["description"])
                return self._format(indent, name, ret)

            value = argument["values"][labels.pop(0)]
            return self._format(indent, name, value)
        except KeyError:
            return self._format(indent, name, "")

    def _format(self, indent: int, name: str, help: str) -> str:
        """Format help."""
        return self.indent_multiplier * indent * ' ' + f"* {name}: {help}".strip()
