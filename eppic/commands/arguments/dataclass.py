"""Representation of a dataclass argument."""
from dataclasses import fields
from typing import Any, List, Optional, Sequence

from eppic.models import ArgumentField, ArgumentLabels, ArgumentLabelSuggestion, ParsedArgument

from .argument import Argument


class DataclassArgument(Argument):
    """Representation of a dataclass argument."""

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """Save the basic info about the argument using base class, also initializing subarguments."""
        super().__init__(*args, **kwargs)
        self._init_subarguments()

    def _init_subarguments(self) -> None:
        """Generate a list of subarguments.

        The subarguments have the correct type, they just think they're not in a sequence.

        For now, the list will have one without a number, and one with number one.
        """
        self._subarguments: List[Argument] = []
        for field in fields(self._arg_type):
            self._subarguments.append(self._factory(ArgumentField.from_dataclass_field(field)))

    def _find_subarg_match(self, label: ArgumentLabels) -> Optional[Argument]:
        """Find a matching subargument.

        If the subargument is not found, return None.

        Arguments:
            label: The label of the lowest-level subargument, split by levels.
        """
        label = label[1:]
        for subargument in self._subarguments:
            if subargument.recognise_label(label):
                return subargument
        return None

    def recognise_label(self, labels: ArgumentLabels) -> bool:
        """Find out whether the labels are yours or one of your subarguments'.

        Arguments:
            labels: The labels of the lowest-level subargument, split by levels.
                            (highest-argument level first)

        Returns:
            True if `labels` are my (subargument's) labels.
        """
        return len(labels) > 1 and labels[0] == self.label and self._find_subarg_match(labels) is not None

    def get_subarguments_args(self, args: Sequence[ParsedArgument]) -> List[ParsedArgument]:
        """Get the `ParsedArgument`s that belong to any of the `self._subarguments`.

        Arguments:
            args: A sequence of parsed arguments belonging to the current `Argument` instance.

        Returns:
            The sequence of parsed arguments, where each `ParsedArgument.labels` is missing the first label.
            This is done so that the labels get recognised by the subarguments.
        """
        return [ParsedArgument(labels[1:], value) for labels, value in args if labels]

    def get_available_labels(self, args: Sequence[ParsedArgument]) -> List[ArgumentLabelSuggestion]:
        """Generate the remaining labels accepted by the argument, based on the parsed arguments that do have labels.

        Arguments:
            args: Input arguments.
        """
        subarguments_args = self.get_subarguments_args(args)

        # if I'm optional and didn't receive any args, then all my label suggestions will be automatically optional.
        auto_optional = not self._required and len(subarguments_args) == 0

        ret = []
        for x in self._subarguments:
            for labels, values, required in x.get_available_labels(subarguments_args):
                ret.append(ArgumentLabelSuggestion(
                    self.get_subarguments_full_label(labels),
                    values,
                    False if auto_optional else required))

        return ret

    def parse_value(self, args: Sequence[ParsedArgument]) -> Optional[Any]:
        """Parse a value from parsed arguments.

        Arguments:
            args: A sequence of parsed arguments belonging to the current `Argument` instance.

        Note: `args` aren't checked for extra arguments not belonging in there!
        Note: If the dataclass can be empty, this method will return `None` rather than the empty dataclass.

        Returns:
            The parsed value of the argument (or `None`, if the argument was optional and didn't receive a value).
        """
        if not self._required and len(args) == 0:
            return None

        subarguments_args = self.get_subarguments_args(args)

        ret = {}
        for argument in self._subarguments:
            owned_subarguments = argument.get_recognised_parsed_arguments(subarguments_args)
            ret[argument.original_label] = argument.parse_value(owned_subarguments)

        return self._arg_type(**ret)
