"""An argument type to handle the EPPlib `Disclose` argument type."""
from itertools import chain
from typing import Any, List, Optional, Sequence

from epplib.models.common import Disclose, DiscloseField

from eppic.commands.arguments.argument import Argument
from eppic.exceptions import CommandParseError
from eppic.models import ArgumentField, ArgumentLabels, ArgumentLabelSuggestion, ParsedArgument


class DiscloseArgument(Argument):
    """Handle disclose argument."""

    def __init__(self, *args: Any, **kwargs: Any) -> None:  # noqa: D417
        """Save the basic info about the argument.

        Arguments:
            arg_name: The name of the argument.
            arg_type: The requested argument's value type.

        """
        super().__init__(*args, **kwargs)
        self._true = self._factory(ArgumentField(self.original_label, str, self._required))
        self._false = self._factory(ArgumentField("un" + self.original_label, str, self._required))

    def recognise_label(self, labels: ArgumentLabels) -> bool:
        """Find out whether the labels are yours or one of your subarguments'.

        Arguments:
            labels: The labels of the lowest-level subargument, split by levels.
                            (highest-argument level first)

        Returns:
            True if `labels` are my (subargument's) labels.

        """
        return self._false.recognise_label(labels) or self._true.recognise_label(labels)

    def get_available_labels(self, args: Sequence[ParsedArgument]) -> List[ArgumentLabelSuggestion]:
        """Generate the remaining labels accepted by the argument, based on the parsed arguments that do have labels.

        Arguments:
            args: Input arguments.
        """
        # if we already have a value, return.
        if any(self.recognise_parsed_argument(a) for a in args):
            return []

        # the `_false` and `_true` subargs aren't marked as required, so we pass `self._required` to their suggestions.
        subarguments_data = chain(self._false.get_available_labels(args),
                                  self._true.get_available_labels(args))
        return [ArgumentLabelSuggestion(label, vals, self._required) for label, vals, _ in subarguments_data]

    def parse_value(self, args: Sequence[ParsedArgument]) -> Optional[Any]:
        """Parse a value from parsed arguments.

        Arguments:
            args: A sequence of parsed arguments belonging to the current `Argument` instance.

        Note: `args` aren't checked for extra arguments not belonging in there!

        Returns:
            The parsed value of the argument (or `None`, if the argument was optional and didn't receive a value).
        """
        if self._false.get_recognised_parsed_arguments(args) and self._true.get_recognised_parsed_arguments(args):
            raise CommandParseError("Trying to use both disclose and undisclose. Choose one.")

        if self._false.get_recognised_parsed_arguments(args):
            values = self._false.parse_value(args)
            value_type = False
        else:
            values = self._true.parse_value(args)
            value_type = True

        if values is None:
            return None

        split_values = values.split(",") if values else []
        try:
            return Disclose(value_type, {DiscloseField(x) for x in split_values})
        except ValueError as e:
            raise CommandParseError(str(e)) from e
