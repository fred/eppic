"""Handle enums."""
from typing import Any, List, Sequence

from eppic.exceptions import CommandParseError
from eppic.models import ArgumentLabelSuggestion, ParsedArgument

from .argument import Argument


class EnumArgument(Argument):
    """Handle ENUM arguments."""

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """Init base class and get enum values."""
        super().__init__(*args, **kwargs)
        self._values = [val.name for val in self._arg_type]

    def _parse_value(self, arg_value: str) -> Any:
        """Parse the argument value itself.

        Arguments:
            arg_value: The raw value to be parsed.
        """
        if arg_value in self._values:
            return self._arg_type[arg_value]

        raise CommandParseError(
            f"Supplied value to argument {self.label} is not among accepted values: {', '.join(self._values)}.")

    def get_available_labels(self, args: Sequence[ParsedArgument]) -> List[ArgumentLabelSuggestion]:
        """Generate the remaining labels accepted by the argument, based on the parsed arguments that do have labels.

        Arguments:
            args: Input arguments.
        """
        if len(list(filter(self.recognise_parsed_argument, args))):
            return []

        return [ArgumentLabelSuggestion([self.label], self._values, self._required)]
