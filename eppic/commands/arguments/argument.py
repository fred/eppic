"""Generic Argument class."""
from typing import Any, Callable, List, Optional, Sequence

from eppic.exceptions import CommandParseError
from eppic.models import ArgumentField, ArgumentLabels, ArgumentLabelSuggestion, ParsedArgument


class Argument:
    """A generic representation of an argument.

    Works with all types, such that `the_type(x)`, where `x` is a string,
    returns the correct value.

    """

    def __init__(
            self,
            field: ArgumentField,
            factory: Callable[[ArgumentField], 'Argument']
    ):
        """Save the basic info about the argument.

        Arguments:
            field: Argument field.
            factory: The factory function creating potential subarguments.

        """
        self.original_label = field.name
        self.label = field.name.replace("_", "-")
        self._arg_type = field.type
        self._required = field.required
        self._factory = factory

    def get_subarguments_full_label(self, sub_label: ArgumentLabels) -> ArgumentLabels:
        """Return the sub_label with self._arg_label added to the end."""
        return [self.label] + sub_label

    def recognise_label(self, labels: ArgumentLabels) -> bool:
        """Find out whether the labels are yours or one of your subarguments'.

        Arguments:
            labels: The labels of the lowest-level subargument, split by levels.
                            (highest-argument level first)

        Returns:
            True if `labels` are my (subargument's) labels.

        """
        return len(labels) == 1 and labels[0] == self.label

    def recognise_parsed_argument(self, arg: ParsedArgument) -> bool:
        """Recognise a parsed argument."""
        return arg.labels is not None and self.recognise_label(arg.labels)

    def get_recognised_parsed_arguments(self, args: Sequence[ParsedArgument]) -> List[ParsedArgument]:
        """Filter only those parsed arguments that are recognised."""
        return [a for a in args if self.recognise_parsed_argument(a)]

    def get_available_labels(self, args: Sequence[ParsedArgument]) -> List[ArgumentLabelSuggestion]:
        """Generate the remaining labels accepted by the argument, based on the parsed arguments that do have labels.

        Arguments:
            args: Input arguments.
        """
        # if we already have a value, return.
        if any(self.recognise_parsed_argument(a) for a in args):
            return []

        return [ArgumentLabelSuggestion([self.label], None, self._required)]

    def _parse_value(self, arg_value: str) -> Any:
        """Parse the argument value itself.

        Arguments:
            arg_value: The raw value to be parsed.
        """
        try:
            return self._arg_type(arg_value)
        except ValueError as error:
            raise CommandParseError(f"Supplied value to argument {self.label} is of incorrect type.") from error

    def parse_value(self, args: Sequence[ParsedArgument]) -> Optional[Any]:
        """Parse a value from parsed arguments.

        Arguments:
            args: A sequence of parsed arguments belonging to the current `Argument` instance.

        Note: `args` aren't checked for extra arguments not belonging in there!

        Returns:
            The parsed value of the argument (or `None`, if the argument was optional and didn't receive a value).
        """
        if len(args) == 0 and self._required:
            raise CommandParseError(f"Required argument {self.label} didn't receive a value.")
        elif len(args) == 0:
            return None
        elif len(args) > 1:
            raise CommandParseError(f"Too many values for {self.label}.")

        assert args[0].value is not None  # noqa: S101 # `None` values were dealt with in `ArgumentParser`
        return self._parse_value(args[0].value)
