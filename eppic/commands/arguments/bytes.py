"""An argument for `bytes` type."""
from typing import Any

from .argument import Argument


class BytesArgument(Argument):
    """Get `bytes`."""

    def _parse_value(self, arg_value: str) -> Any:
        """Parse the argument value itself.

        Arguments:
            arg_value: The raw value to be parsed.
        """
        return bytes(arg_value, encoding="utf-8")
