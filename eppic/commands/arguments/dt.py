"""DateTime Argument Types."""
import datetime
from typing import Any

from eppic.exceptions import CommandParseError

from .argument import Argument


class DateTimeArgument(Argument):
    """Handle datetime creation."""

    def _parse_value(self, arg_value: str) -> Any:
        """Parse the argument value itself.

        Arguments:
            arg_value: The raw value to be parsed.
        """
        try:
            return datetime.datetime.fromisoformat(arg_value)
        except ValueError as error:
            raise CommandParseError(f"Supplied value to argument {self.label} is of incorrect format.") from error


class DateArgument(Argument):
    """Handle date creation."""

    def _parse_value(self, arg_value: str) -> Any:
        """Parse the argument value itself.

        Arguments:
            arg_value: The raw value to be parsed.
        """
        try:
            return datetime.date.fromisoformat(arg_value)
        except ValueError as error:
            raise CommandParseError(f"Supplied value to argument {self.label} is of incorrect format.") from error
