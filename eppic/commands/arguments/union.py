"""An argument type for handling Union arguments."""
from typing import Any, Dict, Iterable, List, Optional, Sequence, Type, get_args

from eppic.exceptions import CommandParseError
from eppic.models import ArgumentLabels, ArgumentLabelSuggestion, ParsedArgument

from .argument import Argument, ArgumentField


class UnionArgument(Argument):
    """Argument class handling Union arguments."""

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """Save the basic info about the argument using base class, also initializing subarguments."""
        super().__init__(*args, **kwargs)
        self._init_subarguments()

    def _init_subarguments(self) -> None:
        """Generate a list of subarguments."""
        self._subarguments: List[Argument] = []

        has_str = False
        for type in get_args(self._arg_type):
            if type is str:
                has_str = True
            else:
                self._add_subargument(type)

        if has_str:
            self._add_subargument(str)

    def _add_subargument(self, inner_type: Type) -> Argument:
        """Generate a subargument."""
        subargument = self._factory(ArgumentField(self.original_label, inner_type, self._required))
        self._subarguments.append(subargument)
        return subargument

    def recognise_label(self, labels: ArgumentLabels) -> bool:
        """Find out whether the labels are yours or one of your subarguments'.

        Arguments:
            labels: The labels of the lowest-level subargument, split by levels.
                            (highest-argument level first)

        Returns:
            True if `labels` are my (subargument's) labels.

        """
        return any(x.recognise_label(labels) for x in self._subarguments)

    def get_arguments_matching_labels(self, labels: List[List[str]]) -> Iterable[Argument]:
        """Get the arguments that match all the `labels`."""
        # get only the labels corresponding to this argument
        our_labels = [x for x in labels if self.recognise_label(x)]

        # get those subarguments that match all of our labels
        # such subarguments are candidates for returning a value
        return (a for a in self._subarguments if all(a.recognise_label(label) for label in our_labels))

    def get_available_labels(self, args: Sequence[ParsedArgument]) -> List[ArgumentLabelSuggestion]:
        """Generate the remaining labels accepted by the argument, based on the parsed arguments that do have labels.

        Arguments:
            args: Input arguments.
        """
        ret: Dict[str, ArgumentLabelSuggestion] = {}
        for arg in self.get_arguments_matching_labels([x.labels for x in args if x.labels]):
            # this loop makes sure that each argument name is there only once, even if suggested by multiple subargs.
            # it also correctly adds all the `value` suggestions for those duplicate args.
            for labels, vals, req in arg.get_available_labels(args):
                key = ".".join(labels)
                if key not in ret.keys():
                    ret[key] = ArgumentLabelSuggestion(labels, vals, req)
                else:
                    # handle possible `value` suggestions.
                    values = ret[key].values or []
                    values += vals or []
                    ret[key] = ArgumentLabelSuggestion(labels, values or None, req)

        return list(ret.values())

    def parse_value(self, args: Sequence[ParsedArgument]) -> Optional[Any]:
        """Parse a value from parsed arguments.

        Arguments:
            args: A sequence of parsed arguments belonging to the current `Argument` instance.

        Note: `args` aren't checked for extra arguments not belonging in there!

        Returns:
            The parsed value of the argument (or `None`, if the argument was optional and didn't receive a value).
        """
        # No args were supplied.
        if not args:
            if not self._required:
                return None
            raise CommandParseError(f"Required argument {self.label} didn't receive a value.")

        # Otherwise, pass the arguments to one of the sub-types.
        for argument in self._subarguments:
            # The argument has to recognise *all* the previously recognised parsed args.
            if argument.get_recognised_parsed_arguments(args) == args:
                try:
                    return argument.parse_value(args)
                except CommandParseError:
                    pass  # Try a different type

        # This happens when the value doesn't match any of the subtype.
        # It also happens for example with a union of two dataclasses, when
        # the user supplied some of the first one, and some of the second one.
        raise CommandParseError(
            f"Values supplied to {self.label} weren't consistent with any of its subtypes.")
