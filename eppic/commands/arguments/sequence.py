"""Argument handling sequence objects, such as lists or sets."""
from collections import defaultdict
from collections.abc import Sequence as ABCSequence
from typing import Any, Dict, Iterable, List, Optional, Sequence, get_args, get_origin

from eppic.models import ArgumentField, ArgumentLabels, ArgumentLabelSuggestion, ParsedArgument

from .argument import Argument


def _group_by_first_label(args: Iterable[ParsedArgument]) -> Dict[str, List[ParsedArgument]]:
    """Group labels into lists by the first label.

    Arguments on output keep their `labels` as they were.
    """
    groups = defaultdict(list)
    for arg in args:
        labels = arg.labels or []
        first_label = list(labels).pop(0)  # `list` creates a new instance, not affecting `arg.labels`.
        groups[first_label].append(arg)

    return groups


class SequenceArgument(Argument):
    """Argument class handling sequence objects, such as lists or sets."""

    SEQUENCE_CONVERSIONS = {
        Sequence: list,
        ABCSequence: list,
        List: list,
        list: list,
        set: set,
    }

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """Save the basic info about the argument using base class, also initializing subarguments."""
        super().__init__(*args, **kwargs)

        inner_type = get_args(self._arg_type)[0]
        self._subargument = self._factory(ArgumentField(self.original_label, inner_type, False))

    def recognise_label(self, labels: ArgumentLabels) -> bool:
        """Find out whether the labels are yours or one of your subarguments'.

        Arguments:
            labels: The labels of the lowest-level subargument, split by levels.
                            (highest-argument level first)

        Returns:
            True if `labels` are my (subargument's) labels.

        """
        return bool(labels) and labels[0].startswith(self.label)

    def get_canonical_labels(self, args: Sequence[ParsedArgument]) -> List[ParsedArgument]:
        """Get canonical labels of parsed arguments.

        Returns a list of parsed arguments, whose first label was changed to the `self.label` value.
        Drop all arguments with `label` either `None` or empty.
        """
        return [ParsedArgument([self.label] + labels[1:], value) for labels, value in args if labels]

    def get_available_labels(self, args: Sequence[ParsedArgument]) -> List[ArgumentLabelSuggestion]:
        """Generate the remaining labels accepted by the argument, based on the parsed arguments that do have labels.

        Arguments:
            args: Input arguments.
        """
        groupped_by_label = _group_by_first_label(filter(self.recognise_parsed_argument, args))
        ret = []

        for group_label, _ in groupped_by_label.items():
            args_canonical = self.get_canonical_labels(args)
            for label, vals, req in self._subargument.get_available_labels(args_canonical):
                ret.append(ArgumentLabelSuggestion([group_label] + label[1:], vals, self._required and req))

        # propose a new member of the sequence
        i = 1
        while f"{self.label}-{i}" in groupped_by_label.keys():
            i += 1
        new_label = f"{self.label}-{i}"
        for label, vals, req in self._subargument.get_available_labels([]):
            ret.append(ArgumentLabelSuggestion([new_label] + label[1:], vals, self._required and req))

        return ret

    def parse_value(self, args: Sequence[ParsedArgument]) -> Optional[Any]:
        """Parse a value from parsed arguments.

        Arguments:
            args: A sequence of parsed arguments belonging to the current `Argument` instance.

        Note: `args` aren't checked for extra arguments not belonging in there!

        Returns:
            The parsed value of the argument (or `None`, if the argument was optional and didn't receive a value).
        """
        groupped_by_first_label = _group_by_first_label(args)

        ret = []
        for _, group_args in groupped_by_first_label.items():
            # replace the custom first label (`arg_123`) by the one expected by the `_subargument` (`arg`)
            canonical_args = self.get_canonical_labels(group_args)
            parsed_value = self._subargument.parse_value(canonical_args)
            ret.append(parsed_value)

        # transform `ret` to be the correct sequence type.
        transform = self.SEQUENCE_CONVERSIONS[get_origin(self._arg_type)]  # type: ignore
        return transform(ret)
