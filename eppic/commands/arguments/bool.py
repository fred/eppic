"""Boolean argument."""
from typing import Any, List, Sequence

from eppic.exceptions import CommandParseError
from eppic.models import ArgumentLabelSuggestion, ParsedArgument

from .argument import Argument


class BoolArgument(Argument):
    """Bool Argument."""

    true = ["y", "yes", "t", "true"]
    false = ["n", "no", "f", "false"]

    def _parse_value(self, arg_value: str) -> Any:
        """Parse the argument value itself.

        Arguments:
            arg_value: The raw value to be parsed.
        """
        arg_value = arg_value.lower()

        if arg_value in self.true:
            return True
        elif arg_value in self.false:
            return False
        else:
            raise CommandParseError(f"Supplied value to argument {self.label} is of incorrect format.")

    def get_available_labels(self, args: Sequence[ParsedArgument]) -> List[ArgumentLabelSuggestion]:
        """Generate the remaining labels accepted by the argument, based on the parsed arguments that do have labels.

        Arguments:
            args: Input arguments.
        """
        if len(list(filter(self.recognise_parsed_argument, args))):
            return []

        return [ArgumentLabelSuggestion([self.label], ["true", "false"], self._required)]
