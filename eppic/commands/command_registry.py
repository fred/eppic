"""Get a command by name."""
from typing import Dict, List

from .command import Command, RequestT


class CommandRegistry:
    """Store commands information."""

    def __init__(self) -> None:
        """Initialize registry."""
        self._commands: Dict[str, Command] = {}

    def get_command(self, command_name: str) -> Command:
        """Get a command by name.

        Arguments:
            command_name: The name of the command.

        Returns: matched command.

        Raises: ValueError if the command is not found.
        """
        if command_name not in self.registered_command_names:
            raise ValueError("Command not found")

        return self._commands[command_name]

    def register_command(self, command: Command[RequestT]) -> None:
        """Register a new command."""
        if command.name in self._commands.keys():
            raise ValueError("Command already in registry.")

        self._commands[command.name] = command

    @property
    def registered_command_names(self) -> List[str]:
        """Get the list of all registered commands."""
        return list(self._commands.keys())
