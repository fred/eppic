"""A `Command` class for executing Epplib commands."""
import logging
from typing import TYPE_CHECKING, TypeVar

from .command import Command
from .service_requests import ServiceRequest

if TYPE_CHECKING:
    from eppic.app import Application

LOGGER = logging.getLogger(__name__)

ServiceRequestT = TypeVar("ServiceRequestT", bound=ServiceRequest)


class ServiceCommand(Command[ServiceRequestT]):
    """A command, which provides communication between the app and the user."""

    def _run(self, app: "Application", request: ServiceRequestT) -> None:
        """Run the command."""
        request.handle(app)
