"""A module for registering argument types."""
import datetime
import inspect
from dataclasses import is_dataclass
from enum import Enum
from typing import Any, Callable, Dict, List, Sequence, Tuple, Type, Union, get_origin

from epplib.models.common import Disclose

from .arguments import (
    Argument,
    BoolArgument,
    BytesArgument,
    DataclassArgument,
    DateArgument,
    DateTimeArgument,
    DiscloseArgument,
    EnumArgument,
    SequenceArgument,
    UnionArgument,
)


class ArgumentRegistry:
    """The class that stores the arguments."""

    def __init__(self) -> None:
        self._types: Dict[Type, Type[Argument]] = {}
        self._callables: List[Tuple[Callable, Type[Argument]]] = []

    def register_argument_by_callable(self, arg_type: Callable[[Any], bool], arg: Any) -> None:
        """Add `arg` to the registry, to be triggered by `arg_type`."""
        self._callables.append((arg_type, arg))

    def register_argument_by_type(self, arg_type: Type, arg: Any) -> None:  # https://github.com/python/mypy/issues/4717
        """Add `arg` to the registry, to be triggered by `arg_type`."""
        self._types[arg_type] = arg

    def get_argument(self, arg_type: Type) -> Type[Argument]:
        """Get the correct `Argument` subclass for the specified `arg_type`.

        `Types` have precedence over `Callable`s.
        """
        if arg_type in self._types.keys():
            return self._types[arg_type]

        for func, arg in self._callables:
            if func(arg_type):
                return arg

        raise ValueError(f"Argument not found for this type - {arg_type}")


def register_defaults(ar: ArgumentRegistry) -> None:
    """Register default argument types."""
    ar.register_argument_by_callable(
        lambda compared_type: inspect.isclass(compared_type) and issubclass(compared_type, Enum), EnumArgument)
    ar.register_argument_by_callable(is_dataclass, DataclassArgument)
    ar.register_argument_by_type(Sequence, SequenceArgument)
    ar.register_argument_by_type(datetime.date, DateArgument)
    ar.register_argument_by_type(datetime.datetime, DateTimeArgument)
    ar.register_argument_by_type(float, Argument)
    ar.register_argument_by_type(int, Argument)
    ar.register_argument_by_type(str, Argument)
    ar.register_argument_by_type(bool, BoolArgument)
    ar.register_argument_by_type(bytes, BytesArgument)
    ar.register_argument_by_type(Disclose, DiscloseArgument)
    ar.register_argument_by_callable(
        lambda compared: get_origin(compared) in SequenceArgument.SEQUENCE_CONVERSIONS.keys(), SequenceArgument)
    ar.register_argument_by_callable(
        lambda compared: get_origin(compared) is Union, UnionArgument)
