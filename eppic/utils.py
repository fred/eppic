"""Utility functions."""
from epplib.utils import safe_parse
from lxml.etree import tostring
from prompt_toolkit import print_formatted_text
from prompt_toolkit.formatted_text import PygmentsTokens
from pygments import lex
from pygments.lexers.html import XmlLexer

from .settings import PrintRawXml


def _print_fn(data: str, syntax_highlighting: bool) -> None:
    data = data.strip()
    if syntax_highlighting:
        tokens = list(lex(data, lexer=XmlLexer()))
        print_formatted_text(PygmentsTokens(tokens))
    else:
        print(data)


def print_xml(xml: bytes, syntax_highlighting: bool, mode: PrintRawXml) -> None:
    """Print the XML."""
    if mode == PrintRawXml.RAW:
        _print_fn(str(xml, encoding="utf-8"), syntax_highlighting)
    elif mode == PrintRawXml.FORMATTED:
        _print_fn(str(tostring(safe_parse(xml), encoding="utf-8", pretty_print=True), encoding="utf-8"),
                  syntax_highlighting)
