"""EPP Handler, carries the EPP session."""
import logging
from typing import Callable, Optional, cast

from epplib.client import Client
from epplib.commands import Login, Logout
from epplib.commands.base import Request as EppRequest, ResponseT
from epplib.exceptions import TransportError
from epplib.policy import Policy
from epplib.transport import SocketTransport

from .exceptions import EppicError, ServerError
from .settings import Session

LOGGER = logging.getLogger(__name__)


def _noop(xml: bytes) -> None:  # pragma: no cover
    """Noop print xml function."""


class EppHandler:
    """Wrapper around the epplib `Client`, handling session with the server.

    Note: The connection to the server is tied to the login session.
    """

    def __init__(self, session: Optional[Session], print_xml_fn: Callable[[bytes], None] = _noop) -> None:
        # Active client if any, or None
        self._client: Optional[Client] = None
        # Active session if any, or None
        self._session: Optional[Session] = None
        self._print_xml_fn = print_xml_fn

        if session is not None:
            self.login(session)

    @property
    def logged_in(self) -> bool:
        """Return `True` if there is currently an active session with the server."""
        return self._client is not None

    @property
    def session(self) -> Optional[Session]:
        """Return an active `Session` or None."""
        return self._session

    @session.setter
    def session(self, session: Session) -> None:
        """Set a session.

        Note: if there's an active session, it gets closed properly.
        """
        self.logout()
        self._session = session

    def logout(self, request: Optional[Logout] = None, * , cltrid: Optional[str] = None) -> None:
        """Log out of an active session and close the connection to the server.

        If not logged in, do nothing.
        If the logout or termination of the connection is unsuccessful in any way, give a warning.
        """
        if not self.logged_in:
            return

        LOGGER.info("Logging out and closing connection...")
        try:
            self._send_request(request or Logout(), cltrid=cltrid)
        except Exception:
            LOGGER.warning("Logout failed.")

        try:
            cast(Client, self._client).close()
        except Exception:
            LOGGER.warning("Connection failed on close.")

        self._client = None

    def login(self, session: Session) -> None:
        """Login to the provided session.

        Sends an EPP `Login` command.
        If there's already another active session, it gets closed properly.

        Raises an exception if the connection with the server fails or the login is unsuccessful.
        """
        self.logout()

        if session.hostname is None:
            raise ValueError("Hostname not specified.")
        if session.username is None:
            raise ValueError("Username not specified.")
        if session.password is None:
            raise ValueError("Password not specified.")

        LOGGER.info("Logging in...")
        transport = SocketTransport(session.hostname, session.port,
                                    cert_file=session.cert_file, key_file=session.key_file, verify=session.verify)
        client = Client(transport)
        try:
            self._session = session
            self._client = client
            client.connect()
            response = self.send_request(Login(cl_id=session.username, password=session.password,
                                               obj_uris=session.obj_uris),
                                         retries=0)
            if response.code >= 2000:
                if response.code in (2200, 2501):
                    raise ServerError("Invalid username or password.")
                else:
                    raise ServerError("Login was unsuccessful.")
        except Exception:
            self.logout()
            raise

    @property
    def policy(self) -> Optional[Policy]:
        """Get `Policy` of the client, or `None` if not logged in."""
        if not self.logged_in:
            return None
        else:
            return cast(Client, self._client).policy

    def send_request(
            self, request: EppRequest[ResponseT], cltrid: Optional[str] = None, retries: Optional[int] = None
    ) -> ResponseT:
        """Send an EPP request to the server, return the server response.

        If sending the request fails, connection to the server is reopened.
        If connecting fails more than `retries` times (`self.session.reconnects` if `None` given),
        the connection is properly terminated and an exception is raised.
        """
        if not self.logged_in:
            raise EppicError("Not logged in.")
        assert self.session is not None  # noqa: S101

        if retries is None:
            retries = self.session.reconnects
        for retry in range(retries + 1):
            try:
                return self._send_request(request, cltrid)
            except TransportError as e:
                self._print_xml_fn(cast(bytes, cast(Client, self._client).last_raw_command))
                if retry < retries:
                    LOGGER.warning("Communication with the server failed. Retrying...")
                    self.login(self.session)
                else:
                    LOGGER.exception("Communication with the server failed. Logging out and closing connection...")
                    self.logout()
                    raise ServerError("Communication with the server failed.") from e
        raise EppicError(f"Got a negative number of retries (session.reconnects): {retries}.")

    def _send_request(self, request: EppRequest[ResponseT], cltrid: Optional[str] = None) -> ResponseT:
        """Send an EPP request to the server, return the server response."""
        assert self._client is not None  # noqa: S101
        LOGGER.info("Send request: %s", request)
        response = self._client.send(request, tr_id=cltrid)
        self._print_xml_fn(cast(bytes, self._client.last_raw_command))
        self._print_xml_fn(cast(bytes, self._client.last_raw_response))

        return response
