"""Handle printing of output."""
import datetime
import json
import sys
from dataclasses import asdict, is_dataclass
from decimal import Decimal
from enum import Enum
from pprint import PrettyPrinter
from typing import Any, Dict, List, Optional, Sequence, TypedDict, cast

import yaml
from epplib.responses.base import MsgQ, Response, Result
from epplib.responses.info import InfoContactResultData
from epplib.responses.poll_messages import PollMessage, RawPollMessage
from lxml.etree import _Element, tostring
from prompt_toolkit import print_formatted_text
from prompt_toolkit.formatted_text import PygmentsTokens
from pygments import lex
from pygments.lexers import YamlLexer

if sys.version_info >= (3, 11):  # pragma: no cover
    from typing import NotRequired
else:  # pragma: no cover
    from typing_extensions import NotRequired

INDENT_MULTIPLIER = 4


class OutputFormat(TypedDict):
    """Format of the standard output (YAML, JSON)."""

    msg: Optional[str]
    code: Optional[int]
    data: Sequence[Dict]
    msg_q: NotRequired[Dict]
    errors: NotRequired[Dict]
    extensions: NotRequired[Sequence[Dict]]
    cl_tr_id: NotRequired[str]
    sv_tr_id: NotRequired[str]


class EppicSafeDumper(yaml.SafeDumper):
    """Custom YAML safe dumper with eppic specifics."""

    def represent_enum(self, member: Enum) -> Any:
        """Represent enum members as their values."""
        return self.represent_data(member.value)

    def represent_decimal(self, data: Decimal) -> Any:
        """Represent decimal as a string."""
        return self.represent_data(str(data))

    def represent_xml_element(self, data: _Element) -> Any:
        """Represent a XML element."""
        return self.represent_data(str(tostring(data), encoding="utf-8"))


EppicSafeDumper.add_representer(Decimal, EppicSafeDumper.represent_decimal)
EppicSafeDumper.add_multi_representer(_Element, EppicSafeDumper.represent_xml_element)
EppicSafeDumper.add_multi_representer(str, EppicSafeDumper.represent_str)
EppicSafeDumper.add_multi_representer(Enum, EppicSafeDumper.represent_enum)


class EppicTextSafeDumper(EppicSafeDumper):
    """Custom YAML safe dumper for the text output."""

    def represent_decimal(self, data: Decimal) -> Any:
        """Represent decimal as a string."""
        return self.represent_scalar('tag:yaml.org,2002:float', str(data))

    def represent_binary_data(self, data: bytes) -> Any:
        """Represent binary data as a string."""
        try:
            return self.represent_data(str(data, encoding="utf-8"))
        except UnicodeDecodeError:
            return self.represent_data(str(data))


EppicTextSafeDumper.add_representer(Decimal, EppicTextSafeDumper.represent_decimal)
EppicTextSafeDumper.add_representer(bytes, EppicTextSafeDumper.represent_binary_data)


class OutputHandler:
    """Prints output."""

    def __init__(self, syntax_highlighting: bool = True) -> None:
        """Initialize the `OutputHandler` instance.

        Arguments:
            syntax_highlighting: Whether or not to enable syntax highlighting.
        """
        self.pp = PrettyPrinter(indent=INDENT_MULTIPLIER)
        self._syntax_highlighting = syntax_highlighting

    def _serialize(self, data: Any) -> Any:
        """Serialize objects to simple types, which can be outputted."""
        if isinstance(data, InfoContactResultData) and data.disclose is not None:
            data_dict = asdict(data)
            data_dict["disclose"] = {"disclosed": data.disclose.disclosed_fields,
                                     "undisclosed": data.disclose.hidden_fields}
            return data_dict
        if is_dataclass(data):
            result = asdict(data)
            if (
                isinstance(data, MsgQ)
                and isinstance(data.msg, PollMessage)
                and not isinstance(data.msg, RawPollMessage)
            ):
                result["msg_type"] = str(data.msg.tag)
            return result
        return data

    def output_data(self, output_data: OutputFormat) -> None:
        """Format the serialized data."""
        # plain text output doesn't keep the output format
        data: Dict[str, Any] = cast(Dict[str, Any], output_data.copy())

        msg, code = data.pop("msg"), data.pop("code")
        if code is not None:
            print(f'{msg} ({code})\n')

        # do not keep the `data` list if it is empty
        if not data["data"]:
            data.pop("data")

        # if there's only single `data`, simplify the output
        if list(data) == ["data"] and len(data["data"]) == 1 and isinstance(data["data"][0], dict):
            data = data["data"][0]

        # convert the data dict to string using yaml
        data_str = yaml.dump(data, Dumper=EppicTextSafeDumper, allow_unicode=True) if data else ""

        if self._syntax_highlighting:
            tokens = list(lex(data_str, lexer=YamlLexer()))
            print_formatted_text(PygmentsTokens(tokens))
        else:
            print(data_str)

    def _get_errors(self, values: List[_Element], ext_values: List[Any]) -> Dict[str, Any]:
        """Get error output dict."""
        ret: Dict[str, Any] = {}
        if values:
            ret["values"] = values
        if ext_values:
            ret["ext_values"] = [self._serialize(x) for x in ext_values]
        return ret

    def output(self, res: Response) -> None:
        """Serialize and output the data."""
        if isinstance(res, Result):
            res_data = res.res_data if res.res_data is not None else []
            out: OutputFormat = {"msg": res.msg, "code": res.code, "data": [self._serialize(r) for r in res_data]}
            if res.msg_q is not None:
                out["msg_q"] = self._serialize(res.msg_q)
            if (errors := self._get_errors(list(res.values), list(res.ext_values))):
                out["errors"] = errors
            if (res.extensions):
                out["extensions"] = [self._serialize(ext) for ext in res.extensions]
            if res.cl_tr_id is not None:
                out["cl_tr_id"] = res.cl_tr_id
            out["sv_tr_id"] = res.sv_tr_id
        else:
            out = {"msg": None, "code": None, "data": [self._serialize(res)]}

        self.output_data(out)


class JsonOutputHandler(OutputHandler):
    """Outputs JSON. Multiple responses are split by newlines."""

    def output_data(self, output_data: OutputFormat) -> None:
        """Format the serialized data."""
        json.dump(output_data, sys.stdout, sort_keys=False, default=self._json_converter, ensure_ascii=False)
        print("")

    def _json_converter(self, obj: Any) -> Any:  # pragma: no branch
        """Convert complex types to JSON format."""
        if isinstance(obj, (datetime.datetime, datetime.date)):
            obj = obj.isoformat()
        if isinstance(obj, Decimal):
            obj = str(obj)
        if isinstance(obj, _Element):
            obj = tostring(obj)
        if isinstance(obj, set):
            obj = sorted(obj)  # sort the set values to make the output predictible
        if isinstance(obj, bytes):
            try:
                obj = str(obj, encoding="utf-8")  # encode the obj in utf-8
            except UnicodeDecodeError:
                obj = str(obj)

        # only happens when we encounter an unknown object.
        return obj  # pragma: no cover


class YamlOutputHandler(OutputHandler):
    """Always outputs valid YAML. Multiple responses are split into multiple documents.

    The whole `stdout` is a valid YAML multi-document.
    """

    def output_data(self, output_data: OutputFormat) -> None:
        """Format the serialized data."""
        yaml.dump(output_data, sys.stdout, sort_keys=False, explicit_start=True, explicit_end=True, allow_unicode=True,
                  Dumper=EppicSafeDumper)
