"""The main file, starting the whole program."""
import shlex
from logging import getLogger
from logging.config import dictConfig
from pathlib import Path
from typing import List, Optional

import click

from eppic.app import Application
from eppic.commands.service_requests import ServiceNewSession
from eppic.settings import Settings

LOGGER = getLogger(__name__)


@click.command()
@click.option('--input-file', help='Specify the file to get the input from. If not specified, stdin is used.')
@click.option('--config', type=click.Path(exists=True, file_okay=True, dir_okay=False),
              help='Settings file location.')
@click.option('--session', 'session_id',
              help='Specify session by alias. The default is the first in the list, if any sessions are defined.')
@click.option('--session-id', 'session_id_old',
              help='Deprecated alias for --session. Ignored if --session is used.')
@click.option('--username', help="Create a new session and set its `username`.")
@click.option('--password', help="Create a new session and set its `password`.")
@click.option('--hostname', help="Create a new session and set its `hostname`.")
@click.option('--port', type=int, help="Create a new session and set its `port`.")
@click.option('--cert-file', help="Create a new session and set its `cert_file`.")
@click.option('--key-file', help="Create a new session and set its `key_file`.")
@click.option('--obj-uris', type=str, help="Create a new session and set its `obj_uris`.", multiple=True)
@click.option('--verify', type=bool, help="Create a new session and set its `verify`.")
@click.option('--alias', help="Create a new session and set its `alias`. Defaults to username.")
@click.option('--reconnects', type=int, help="Create a new session and set its `reconnects`. Defaults to 1.")
@click.argument('command', nargs=-1, type=str)
def main(config: Optional[Path] = None, session_id: Optional[str] = None, session_id_old: Optional[str] = None,
         input_file: Optional[str] = None,
         username: Optional[str] = None, password: Optional[str] = None,
         hostname: Optional[str] = None, port: Optional[int] = None,
         cert_file: Optional[str] = None, key_file: Optional[str] = None,
         obj_uris: Optional[List[str]] = None, verify: Optional[bool] = None,
         alias: Optional[str] = None, reconnects: Optional[int] = None,
         command: Optional[List[str]] = None) -> None:
    """Run the EPP interactive client.

    If any of the options to create a new session are specified, the `--session-id` option is ignored and
    a new session is set up on start.
    """
    try:
        settings = Settings(input_file=input_file, settings_file=config)
        if settings.logging:
            dictConfig(settings.logging)
    except Exception as error:
        LOGGER.exception("There is an error in settings.")
        raise click.Abort from error

    command_str = " ".join(map(shlex.quote, command)) if command else None

    new_session_requested = any(x is not None for x in [        # empty obj_uris won't be none, rather an empty sequence
        username, password, hostname, port, cert_file, key_file, verify, alias, reconnects]) or obj_uris

    # if new session is requested, do not pass the session to the app to prevent double login.
    session = settings.get_session(session_id or session_id_old) if not new_session_requested else None

    app = Application(settings, command_str, session)
    if session_id_old is not None:
        app.print_warning("Argument --session-id is deprecated. Use --session instead.")

    if new_session_requested:
        ServiceNewSession(
            username=username, password=password, hostname=hostname, port=port,
            cert_file=cert_file, key_file=key_file, obj_uris=(list(obj_uris) if obj_uris is not None else None),
            verify=verify, alias=alias, reconnects=reconnects, login=True).handle(app)
    app.run()


if __name__ == "__main__":
    main()
