"""A module containing all the `eppic` exceptions."""
from click import ClickException


class EppicError(ClickException):
    """A general class for all `eppic` errors."""

    exit_code = 3


class CommandParseError(EppicError):
    """An error that occurs when parsing arguments."""

    exit_code = 4


class ServerError(EppicError):
    """An error that occurs when communication with the epp server fails."""

    exit_code = 5
