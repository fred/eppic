"""Basic structures."""
from dataclasses import MISSING, Field, dataclass
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    Generic,
    List,
    NamedTuple,
    Optional,
    Sequence,
    Type,
    TypeVar,
    Union,
    get_args,
    get_origin,
)

from epplib.commands.base import Request as EppRequest

if TYPE_CHECKING:
    from eppic.commands.service_requests import ServiceRequest

ArgumentLabels = List[str]
SessionId = Union[int, str]


class ParsedArgument(NamedTuple):
    """A pair of labels, value."""

    labels: Optional[ArgumentLabels]
    value: Optional[str]


RawCommandType = TypeVar('RawCommandType', str, Dict[str, Any])


@dataclass
class ParsedCommand(Generic[RawCommandType]):
    """Represents a CLI command.

    Attributes:
        raw: The raw command as provided by the user.
        name: The name of the command.
        raw_args: Raw arguments of the command.
        args: Actual arguments of the command.
        error: An error encountered on parsing if any.
    """

    raw: RawCommandType
    name: str
    raw_args: Sequence[ParsedArgument]
    args: Optional[Dict[str, Any]] = None
    error: Optional[str] = None

    def add_error(self, error: str) -> None:
        """Set an error if not already set."""
        if self.error is None:
            self.error = error


Request = Union[EppRequest, "ServiceRequest"]


class ArgumentField(NamedTuple):
    """Representation of argument data, similar to `dataclass.Field`.

    Attributes:
        name: Name of the argument.
        type: Type of the argument. May contain `Union`.
        required: Whether the argument is required.
    """

    name: str
    type: Type
    required: bool

    @classmethod
    def from_dataclass_field(cls, field: Field) -> 'ArgumentField':
        """Create instance from dataclass field."""
        required = (field.default is MISSING
                    and field.default_factory is MISSING
                    # Consider fields with `Optional` type and no default value as not required.
                    and (get_origin(field.type) is not Union or type(None) not in get_args(field.type)))

        return cls(field.name, field.type, required)


class ArgumentLabelSuggestion(NamedTuple):
    """A representation of an argument label suggestion.

    Attributes:
        labels: The labels of the argument.
        values: A list of values accepted by the arguments, or
            `None` if the argument doesn't accept values from a pre-defined list.
        required: Whether the argument is required for the completion of the command.
    """

    labels: ArgumentLabels
    values: Optional[List[str]]
    required: bool
