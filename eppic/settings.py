"""Handle Settings from several sources."""
import sys
from enum import Enum, unique
from functools import partial
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Type, cast

import yaml
from epplib.constants import NAMESPACE
from pydantic import BaseModel, Extra, Field, FilePath
from pydantic_settings import BaseSettings, InitSettingsSource, PydanticBaseSettingsSource, SettingsConfigDict

from .models import SessionId

SETTINGS_FILE_LOCATIONS = ["~/.eppic.conf", "/etc/eppic/eppic.conf"]


def _parse_settings_files(settings_file: Optional[Path]) -> Dict[str, Any]:
    """Parse a yaml config file. Either get it from the settings or choose it from `SETTINGS_FILE_LOCATIONS`.

    Arguments:
        settings_file: File path to the config file.

    Returns: A dictionary with the parsed values.
    """
    # if no settings file is specified, choose one of the default locations
    if settings_file is None:
        for file_location in SETTINGS_FILE_LOCATIONS:
            file_path = Path(file_location).expanduser()
            if file_path.is_file():
                settings_file = file_path
                break
    else:
        settings_file = Path(settings_file).expanduser()

    # no settings file exists
    if settings_file is None:
        return {}

    with open(settings_file) as f:
        return cast(Dict[str, Any], yaml.safe_load(f))


@unique
class PrintRawXml(str, Enum):
    """Define modes of printing raw XML."""

    DISABLED = "DISABLED"
    RAW = "RAW"
    FORMATTED = "FORMATTED"


@unique
class OutputFormat(str, Enum):
    """Define types of output."""

    DEFAULT = "DEFAULT"
    JSON = "JSON"
    YAML = "YAML"


@unique
class InputFormat(str, Enum):
    """Define types of input."""

    DEFAULT = "DEFAULT"
    JSON = "JSON"
    YAML = "YAML"


class Session(BaseModel):
    """Session configuration info."""

    hostname: Optional[str] = None
    port: int = 700
    cert_file: Optional[FilePath] = None
    key_file: Optional[FilePath] = None
    obj_uris: List[str] = [NAMESPACE.NIC_CONTACT, NAMESPACE.NIC_DOMAIN, NAMESPACE.NIC_NSSET, NAMESPACE.NIC_KEYSET]
    username: Optional[str] = None
    password: Optional[str] = None
    verify: bool = True
    alias: Optional[str] = None
    reconnects: int = 1

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """Init model and get alias."""
        super().__init__(*args, **kwargs)

        if self.alias is None:
            self.alias = self.username

    def info_dict(self) -> Dict[str, Any]:
        """Get a dict with information about the session."""
        return {
            "hostname": self.hostname,
            "port": self.port,
            "cert_file": str(self.cert_file) if self.cert_file else None,
            "key_file": str(self.key_file) if self.key_file else None,
            "obj_uris": self.obj_uris,
            "username": self.username,
            "password": "***" if self.password else None,
            "verify": self.verify,
            "alias": self.alias,
            "reconnects": self.reconnects,
        }


class InteractiveSettings(BaseModel):
    """Settings specific for interactive input/output."""

    autocomplete: bool = Field(default_factory=sys.stdin.isatty)
    command_confirmation: bool = False
    enable_prompt: bool = Field(default_factory=sys.stdin.isatty)
    history_file_path: Path = Path("~/.cache/eppic/autosuggest_history")
    syntax_highlighting: bool = Field(default_factory=sys.stdin.isatty)
    online_validation: bool = True
    vi_mode: bool = False


class Settings(BaseSettings):
    """Default Settings class."""

    _settings_file: Optional[Path] = None
    _session_id: Optional[SessionId] = None

    sessions: List[Session] = []
    input_file: Optional[FilePath] = None
    input_format: InputFormat = InputFormat.DEFAULT
    output_format: OutputFormat = OutputFormat.DEFAULT
    logging: Optional[Dict] = None
    interactive: InteractiveSettings = InteractiveSettings()
    print_raw_xml: PrintRawXml = PrintRawXml.DISABLED
    print_list_results_automatically: bool = True
    exit_on_error: bool = Field(default_factory=lambda: not sys.stdin.isatty())

    def __init__(
        self,
        *args: Any,
        **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        settings_file = kwargs.get("settings_file", None)
        self._settings_file = Path(settings_file) if settings_file is not None else None

    def get_session(self, session_id: Optional[SessionId]) -> Optional[Session]:
        """Choose the session object settings.

        Arguments:
            session_id: The ID of the requested session:
                str - the alias of the session,
                int - the index of the session, starting from zero,
                `None` - choose the first session.

        Returns:
            A session object, or `None` if no sessions exist, given that no id was set.

        Raises:
            TypeError if the `session_id` is of wrong type,
            ValueError if a `session_id` was specified, but no such session exists.
        """
        if len(self.sessions) == 0 and session_id is not None:  # trying to choose a session, but none are defined
            raise ValueError("No sessions specified.")

        elif len(self.sessions) == 0:
            return None

        if session_id is not None:
            # specified by index
            if isinstance(session_id, int):
                if session_id >= len(self.sessions) or 0 > session_id:
                    raise ValueError("Specified session does not exist!")
                return self.sessions[session_id]

            # specified by username
            elif isinstance(session_id, str):
                for session in self.sessions:
                    if session.alias == session_id:
                        return session
                raise ValueError("Specified session does not exist!")
            else:
                raise TypeError("Wrong type of session id")

        # not specified - choose first
        else:
            return self.sessions[0]

    model_config = SettingsConfigDict(
        env_prefix="EPPIC_",
        extra=Extra.ignore,
    )

    @classmethod
    def settings_customise_sources(
        cls, settings_cls: Type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> Tuple[PydanticBaseSettingsSource, ...]:
        """List of source functions."""
        # steal the `settings_file` path, if supplied
        kwargs = cast(InitSettingsSource, init_settings).init_kwargs
        specified_settings_path = kwargs.get("settings_file", None)
        return (
            init_settings,
            cast(PydanticBaseSettingsSource, partial(_parse_settings_files, specified_settings_path)),
            env_settings,
            dotenv_settings,
            file_secret_settings,
        )
