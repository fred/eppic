from unittest import TestCase
from unittest.mock import patch

from lxml.builder import ElementMaker
from lxml.etree import tostring
from testfixtures import OutputCapture

from eppic.settings import PrintRawXml
from eppic.utils import print_xml


class TestPrintXml(TestCase):

    EM = ElementMaker()
    sample_data = tostring(EM.create(EM.contact(id="asdf")))
    data_raw = "<create><contact id=\"asdf\"/></create>\n"
    data_formatted = """<create>
  <contact id=\"asdf\"/>
</create>
"""

    def get_outputs(self, *args, **kwargs):
        """Return a tuple: normal output and pretty output mock."""
        with OutputCapture() as out:
            with patch("eppic.utils.print_formatted_text") as formatted_text:
                print_xml(*args, **kwargs)
        return out.output, formatted_text

    def test_dont_print(self):
        for syntax in [True, False]:
            with self.subTest(syntax=syntax):
                out, formatted = self.get_outputs(self.sample_data, syntax, PrintRawXml.DISABLED)
                formatted.assert_not_called()
                self.assertEqual(out.getvalue(), "", msg=out.getvalue())

    def test_print_syntax_hi(self):
        for syntax in [PrintRawXml.FORMATTED, PrintRawXml.RAW]:
            with self.subTest(mode=syntax):
                out, formatted = self.get_outputs(self.sample_data, True, syntax)
                formatted.assert_called_once()
                self.assertEqual(out.getvalue(), "", msg=out.getvalue())

    def test_print_no_syntax(self):
        cases = [
            (PrintRawXml.FORMATTED, self.data_formatted),
            (PrintRawXml.RAW, self.data_raw),
        ]
        for mode, expected in cases:
            with self.subTest(mode=mode):
                out, formatted = self.get_outputs(self.sample_data, False, mode)
                formatted.assert_not_called()
                self.assertEqual(out.getvalue(), expected, msg=out.getvalue())
