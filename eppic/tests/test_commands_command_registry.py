from typing import cast
from unittest import TestCase

from eppic.commands.command import Command
from eppic.commands.command_registry import CommandRegistry


class DummyCommand:
    def __init__(self):
        self.name = "foobar"


class TestCommandRegistry(TestCase):

    def test_command_normal(self):
        cmd = cast(Command, DummyCommand())
        cr = CommandRegistry()
        cr.register_command(cmd)
        self.assertEqual(
            cr.get_command("foobar"),
            cmd
        )

    def test_register_existing(self):
        cmd = cast(Command, DummyCommand())
        cr = CommandRegistry()
        cr.register_command(cmd)
        self.assertRaises(
            ValueError,
            cr.register_command, cmd
        )

    def test_get_nonextisting(self):
        self.assertRaises(
            ValueError,
            CommandRegistry().get_command, "asdf"
        )
