from dataclasses import dataclass
from typing import Dict, Iterable, List, Optional, Tuple, Type, cast
from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel

from click.testing import CliRunner
from epplib.commands import CheckDomain
from epplib.commands.base import Command as EppRequest

from eppic.commands.epp_command import EppCommand
from eppic.commands.service_command import ServiceCommand
from eppic.models import ArgumentField

from .utils import DummyEppCommand, DummyServiceCommand, GetAppMixin, get_app


@dataclass
class DummyEppRequest(EppRequest):
    foo: str
    bar: int

    def _get_command_payload(self) -> None:  # pragma: no cover
        # Dummy implementation of abstract method.
        pass


@dataclass
class DummyEppRequestOptional(EppRequest):
    foo: str
    bar: Optional[int] = None
    baz: str = "asdf"

    def _get_command_payload(self) -> None:  # pragma: no cover
        # Dummy implementation of abstract method.
        pass


class NotDataclassEppRequest(EppRequest):
    foo: str = "asdf"

    def __init__(self) -> None:
        self.extensions = []

    def _get_command_payload(self) -> None:  # pragma: no cover
        # Dummy implementation of abstract method.
        pass

    def __eq__(self, o):
        return True


class TestEppCommand(GetAppMixin, TestCase):
    """Test the creation of the Command class."""

    argument_cases: List[Tuple[Type[EppRequest], List]] = [
        (DummyEppRequest, [ArgumentField("foo", str, True),
                           ArgumentField("bar", int, True)]),
        (DummyEppRequestOptional, [ArgumentField("foo", str, True),
                                   ArgumentField("bar", Optional[int], False),  # type: ignore
                                   ArgumentField("baz", str, False)]),
        (NotDataclassEppRequest, []),
    ]
    cltrid_arg = ArgumentField("cltrid", str, False)

    def command(self, extensions: Optional[Iterable[ArgumentField]] = None,
                command_class: Type[EppRequest] = DummyEppRequest) -> EppCommand:
        return EppCommand(
            "foobar",
            command_class,
            Mock(),
            extensions=extensions
        )

    def test_command_create_arg_parser(self) -> None:
        for cls, expected in self.argument_cases:
            with self.subTest(cls=cls):
                self.assertEqual(list(EppCommand("foobar", cls, Mock()).arg_parser._fields),
                                 expected + [self.cltrid_arg])

    def test_command_arg_parser_extensions(self):
        for cls, expected in self.argument_cases:
            with self.subTest(cls=cls):
                self.assertEqual(list(EppCommand("foobar", cls, Mock(),
                                 extensions=[ArgumentField("foobar", str, True)]).arg_parser._fields),
                                 expected + [ArgumentField("foobar", str, True), self.cltrid_arg])

    def test_pop_extensions(self):
        cmd = self.command([ArgumentField("asdf", int, False)])
        cases: List[Tuple[Dict, List, Dict]] = [
            ({"foo": "foo", "asdf": 69}, [69], {"foo": "foo"}),
            ({"foo": "foo", "asdf": None}, [], {"foo": "foo"}),
            ({"asdf": 69}, [69], {}),
            ({"asdf": None}, [], {}),
        ]
        for inp, extensions, remaining in cases:
            with self.subTest(inp=inp):
                self.assertEqual(cmd._pop_extensions(inp), extensions)
                self.assertEqual(inp, remaining)

    def test_generate(self):
        cmd = self.command()
        # command class, extensions, input args, result command, result extensions
        cases: List[Tuple[Type[EppRequest], List, Dict, EppRequest, List]] = [
            # no extensions
            (DummyEppRequest, [], {"foo": "foo", "bar": 42}, DummyEppRequest("foo", 42), []),
            (NotDataclassEppRequest, [], {}, NotDataclassEppRequest(), []),
            (DummyEppRequestOptional, [], {"foo": "foo", "bar": 42}, DummyEppRequestOptional("foo", 42, "asdf"), []),

            # extensions exist, but not used
            (DummyEppRequest, [ArgumentField("foobar", str, False)], {"foo": "foo", "bar": 42},
             DummyEppRequest("foo", 42), []),
            (NotDataclassEppRequest, [ArgumentField("foobar", str, False)], {}, NotDataclassEppRequest(), []),
            (DummyEppRequestOptional, [ArgumentField("foobar", str, False)], {"foo": "foo", "bar": 42},
             DummyEppRequestOptional("foo", 42, "asdf"), []),

            # extensions exist and used
            (DummyEppRequest, [ArgumentField("foobar", str, False)], {"foo": "foo", "bar": 42, "foobar": "asdf"},
             DummyEppRequest("foo", 42), ["asdf"]),
            (NotDataclassEppRequest, [ArgumentField("foobar", str, False)], {"foobar": "asdf"},
             NotDataclassEppRequest(), ["asdf"]),
            (DummyEppRequestOptional, [ArgumentField("foobar", str, False)], {"foo": "foo", "bar": 42,
                                                                              "foobar": "asdf"},
             DummyEppRequestOptional("foo", 42, "asdf"), ["asdf"]),

            # cltrid set
            (DummyEppRequest, [ArgumentField("foobar", str, False)], {"foo": "foo", "bar": 42, "foobar": "asdf",
                                                                      "cltrid": "foobarbaz"},
             DummyEppRequest("foo", 42), ["asdf"]),
            (DummyEppRequest, [ArgumentField("foobar", str, False)], {"foo": "foo", "bar": 42, "cltrid": "asdf"},
             DummyEppRequest("foo", 42), []),
            (DummyEppRequest, [], {"foo": "foo", "bar": 42, "cltrid": "asdf"},
             DummyEppRequest("foo", 42), []),
        ]

        for cls, extensions, args, expected, expected_ext in cases:
            with self.subTest(cls=cls, extensions=extensions, args=args):
                cmd = self.command(extensions=extensions, command_class=cls)
                res = cmd._prepare_request(args)
                self.assertEqual(res, expected)
                self.assertEqual(cast(EppRequest, res).extensions, expected_ext)

    def test_confirm(self):
        app = self.get_app(confirm=True, syntax_highlighting=False)
        cases = [
            ("n", app.epp_handler.send_request.assert_not_called),
            ("y", app.epp_handler.send_request.assert_called),
        ]
        for answer, test in cases:
            with self.subTest(answer=answer):
                with CliRunner().isolation(input=answer):
                    self.command(
                        command_class=DummyEppCommand
                    ).run(app, {"foobar": "foobar"})
                    test()


# XXX: get_app doesn't play well with GetAppMixin.
class EppCommandTest(TestCase):
    def test_cli(self):
        data = (
            # CLI command, epp call
            ('check-domain example.net', call(CheckDomain(names=['example.net']), tr_id=None)),
            ('check-domain example.net --cltrid future-echo',
             call(CheckDomain(names=['example.net']), tr_id='future-echo')),
        )
        for command, epp_call in data:
            with self.subTest(command=command):
                app = get_app()

                app.run_command(command)

                self.assertEqual(app.epp_mock_calls, [epp_call])


class TestServiceCommand(TestCase):

    def test_run(self):
        command = ServiceCommand("foobar", DummyServiceCommand, Mock())
        app = sentinel.app
        args = {"foobar": "foobar"}
        with patch("eppic.tests.test_commands_command.DummyServiceCommand.handle") as p:
            command.run(app, args)
        p.assert_called_once_with(sentinel.app)
