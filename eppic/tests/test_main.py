from contextlib import contextmanager
from tempfile import NamedTemporaryFile
from typing import Any, Iterator
from unittest import TestCase
from unittest.mock import ANY, call, patch, sentinel

import yaml
from click.testing import CliRunner, Result

from eppic.main import main
from eppic.settings import Session, Settings


@contextmanager
def override_settings(**overrides: Any) -> Iterator[Settings]:
    settings = Settings(**overrides)
    try:
        with patch('eppic.main.Settings', return_value=settings):
            yield settings
    finally:
        pass


class TestMain(TestCase):

    runner = CliRunner(mix_stderr=False)

    def setUp(self) -> None:
        patcher = patch("eppic.main.Application", autospec=True)
        self.addCleanup(patcher.stop)
        self.app_mock = patcher.start()
        self.app_mock.return_value.mock_add_spec(['epp_handler'])

    def assertResultSuccess(self, result: Result, *, stdout: str = ANY, stderr: str = '') -> None:
        """Check command result succeeded."""
        self.assertIsNone(result.exception)
        err_msg = "command failed with code {}, stderr: {!r}".format(result.exit_code, result.stderr)
        self.assertEqual(result.exit_code, 0, msg=err_msg)
        self.assertEqual(result.stdout, stdout)
        self.assertEqual(result.stderr, stderr)

    def assertResultFail(self, result: Result, *, stdout: str = '', stderr: str = '') -> None:
        """Check command result failed."""
        self.assertNotEqual(result.exit_code, 0)
        self.assertEqual(result.stdout, stdout)
        self.assertIn(stderr, result.stderr)

    def test_settings_error(self):
        with NamedTemporaryFile('w+') as tmp_file:
            settings = {'sessions': {'cert_file': '/does/not/exist'}}
            yaml.safe_dump(settings, tmp_file)

            result = self.runner.invoke(main, ["--config", tmp_file.name])

        self.assertResultFail(result)

        self.assertEqual(self.app_mock.mock_calls, [])

    def test(self):
        with override_settings(sessions=()) as settings:
            result = self.runner.invoke(main, [])

        self.assertResultSuccess(result)

        self.assertEqual(self.app_mock.mock_calls, [call(settings, None, None), call().run()])

    def test_command(self):
        with override_settings(sessions=()) as settings:
            result = self.runner.invoke(main, ["warm", "gazpacho", "soup"])

        self.assertResultSuccess(result)

        self.assertEqual(self.app_mock.mock_calls, [call(settings, "warm gazpacho soup", None), call().run()])

    @patch('eppic.settings.SETTINGS_FILE_LOCATIONS', new=())
    def test_input_file(self):
        with NamedTemporaryFile() as tmp_file:
            with patch('eppic.main.Settings', wraps=Settings) as mock_settings:
                result = self.runner.invoke(main, ['--input-file', tmp_file.name])

            self.assertResultSuccess(result)

            self.assertEqual(mock_settings.mock_calls, [call(input_file=tmp_file.name, settings_file=None)])
            self.assertEqual(self.app_mock.mock_calls,
                             [call(Settings(input_file=tmp_file.name, exit_on_error=True), None, None), call().run()])

    @patch('eppic.settings.SETTINGS_FILE_LOCATIONS', new=())
    def test_logging(self):
        with override_settings(logging={'key': sentinel.logging}) as settings:
            with patch('eppic.main.dictConfig') as dict_config_mock:
                result = self.runner.invoke(main, [])

        self.assertResultSuccess(result)

        self.assertEqual(dict_config_mock.mock_calls, [call({'key': sentinel.logging})])
        self.assertEqual(self.app_mock.mock_calls, [call(settings, None, None), call().run()])

    def test_session(self):
        session = Session(username="rimmer")
        with override_settings(sessions=[{'username': 'kryten'}, {'username': 'rimmer'}]) as settings:
            result = self.runner.invoke(main, ["--session", "rimmer"])

        self.assertResultSuccess(result)

        self.assertEqual(self.app_mock.mock_calls,
                         [call(settings, None, session), call().run()])

    def test_session_id(self):
        session = Session(username="rimmer")
        with override_settings(sessions=[{'username': 'kryten'}, {'username': 'rimmer'}]) as settings:
            result = self.runner.invoke(main, ["--session-id", "rimmer"])

        self.assertResultSuccess(result)

        msg = "Argument --session-id is deprecated. Use --session instead."
        self.assertEqual(self.app_mock.mock_calls,
                         [call(settings, None, session), call().print_warning(msg), call().run()])

    def test_new_session_hostname(self):
        session = Session(hostname="hello")
        with override_settings() as settings:
            result = self.runner.invoke(main, ["--hostname", "hello"])

        self.assertResultSuccess(result)

        calls = [call(settings, None, None), call().add_session(session), call().epp_handler.login(session),
                 call().run()]
        self.assertEqual(self.app_mock.mock_calls, calls)

    def test_new_session_obj_uris(self):
        session = Session(obj_uris=["foo"])
        with override_settings() as settings:
            result = self.runner.invoke(main, ["--obj-uris", "foo"])

        self.assertResultSuccess(result)

        calls = [call(settings, None, None), call().add_session(session), call().epp_handler.login(session),
                 call().run()]
        self.assertEqual(self.app_mock.mock_calls, calls)

    def test_new_session_all(self):
        session = Session(hostname="hello", port=123, username="foobar", password="passwd", obj_uris=["asdf", "ghjkl"],
                          alias="foobarbaz", reconnects=3)
        args = ["--hostname", "hello", "--port", "123", "--username", "foobar", "--password", "passwd",
                "--obj-uris", "asdf", "--obj-uris", "ghjkl", "--alias", "foobarbaz", "--reconnects", "3"]
        with override_settings() as settings:
            result = self.runner.invoke(main, args)

        self.assertResultSuccess(result)

        calls = [call(settings, None, None), call().add_session(session), call().epp_handler.login(session),
                 call().run()]
        self.assertEqual(self.app_mock.mock_calls, calls)

    def test_new_session_ignored(self):
        # Test --session is ignored when new session is to be created.
        session = Session(hostname="hello")
        with override_settings() as settings:
            result = self.runner.invoke(main, ["--session", "test", "--hostname", "hello"])

        self.assertResultSuccess(result)

        calls = [call(settings, None, None), call().add_session(session), call().epp_handler.login(session),
                 call().run()]
        self.assertEqual(self.app_mock.mock_calls, calls)
