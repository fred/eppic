"""Test argument registry."""
from enum import Enum
from typing import Optional, Set, Type, Union, cast
from unittest import TestCase

from eppic.commands.argument_registry import ArgumentRegistry, register_defaults
from eppic.commands.arguments import Argument, EnumArgument, SequenceArgument, UnionArgument


class TestArgumentRegistry(TestCase):
    """Test argument registry functions."""

    def setUp(self) -> None:
        """Prep argument_registry."""
        self.argument_registry = ArgumentRegistry()

    def test_reg_get_normal(self) -> None:
        """Test that when you register an argument by type, you can get it."""
        self.argument_registry.register_argument_by_type(str, Argument)
        self.assertEqual(
            self.argument_registry.get_argument(str),
            Argument
        )

    def test_reg_get_complicated(self) -> None:
        """Test that you can get even an argument with complicated type."""
        self.argument_registry.register_argument_by_type(cast(Type, Optional[str]), Argument)
        self.assertEqual(
            self.argument_registry.get_argument(cast(Type, Optional[str])),
            Argument
        )

    def test_not_getting_arguments(self) -> None:
        """Test getting an argument not registered."""
        self.argument_registry.register_argument_by_type(int, Argument)
        self.assertRaises(
            ValueError,
            self.argument_registry.get_argument, str,
        )

    def test_getting_an_argument_by_callable(self) -> None:
        """Test that you can get the argument even if callable."""
        self.argument_registry.register_argument_by_callable(lambda x: True, Argument)
        self.assertEqual(
            self.argument_registry.get_argument(cast(Type, Optional[str])),
            Argument
        )

    def test_not_getting_an_argument_by_callable(self) -> None:
        """Test that you can not get the argument even if callable."""
        self.argument_registry.register_argument_by_callable(lambda x: False, Argument)
        self.assertRaises(
            ValueError,
            self.argument_registry.get_argument, str,
        )

    def test_mix_callable_type(self):
        """Test whether it can handle both callables and types."""
        self.argument_registry.register_argument_by_callable(lambda x: x is str, None)
        self.argument_registry.register_argument_by_type(int, Argument)
        self.assertEqual(
            self.argument_registry.get_argument(int),
            Argument
        )
        self.assertEqual(
            self.argument_registry.get_argument(str),
            None
        )

    def test_reg_get_complicated_different_order(self) -> None:
        """Order should not matter with complicated arguments."""
        self.argument_registry.register_argument_by_type(cast(Type, Union[int, str]), Argument)
        self.assertEqual(
            self.argument_registry.get_argument(cast(Type, Union[str, int])),
            Argument
        )

    def test_different_unions(self):
        """Test adding a union and then wanitng a different one."""
        self.argument_registry.register_argument_by_type(cast(Type, Optional[int]), Argument)
        self.assertRaises(
            ValueError,
            self.argument_registry.get_argument, Optional[str],
        )


class DummyEnum(Enum):
    """Dummy enum."""

    foo = "bar"


class TestRegisterArguments(TestCase):
    def setUp(self) -> None:
        """Prep argument_registry."""
        self.argument_registry = ArgumentRegistry()
        register_defaults(self.argument_registry)

    def test_register_arguments(self):
        self.assertGreater(len(self.argument_registry._types), 0)
        self.assertGreater(len(self.argument_registry._callables), 0)

    def test_enum(self):
        self.assertEqual(self.argument_registry.get_argument(DummyEnum), EnumArgument)

    def test_sequence(self):
        self.assertEqual(self.argument_registry.get_argument(Set[str]), SequenceArgument)

    def test_sequence_empty(self):
        self.assertRaises(ValueError, self.argument_registry.get_argument, set)

    def test_union(self):
        self.assertEqual(self.argument_registry.get_argument(cast(Type, Union[str, int])), UnionArgument)

    def test_union_empty(self):
        self.assertRaises(ValueError, self.argument_registry.get_argument, Union)
