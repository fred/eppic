import json
from abc import ABC, abstractmethod
from copy import deepcopy
from dataclasses import dataclass
from datetime import date, datetime
from decimal import Decimal
from enum import Enum
from typing import Any, List, Mapping, Tuple, cast, final
from unittest import TestCase
from unittest.mock import patch

import yaml
from epplib.models.common import Disclose, DiscloseField, PostalInfo
from epplib.responses.base import ExtValue, MsgQ, Response, Result
from epplib.responses.info import InfoContactResultData
from epplib.responses.poll_messages import IdleContactDeletion, RawPollMessage
from lxml.builder import ElementMaker as EM
from testfixtures import OutputCapture

from eppic.output import JsonOutputHandler, OutputHandler, YamlOutputHandler


class TestEnum(int, Enum):
    TEST = 42


@dataclass
class EnumResData:
    member: TestEnum = TestEnum.TEST


@dataclass
class DummyResData:
    foo: str
    bar: str


@dataclass
class DummyResult:
    code: int
    msg: str
    res_data: List[DummyResData]


@dataclass
class DummyResponse(Response):
    random: int = 42

    @classmethod
    def _extract_payload(cls, element: Any) -> Mapping[str, Any]:  # pragma: no cover
        # Dummy impl.
        return {}


def _make_result(**kwargs: Any) -> Result:
    return Result(**kwargs, cl_tr_id='', sv_tr_id='')


class OutputTestMixin(ABC):
    handler: OutputHandler

    @abstractmethod
    def assertOutput(self, output: OutputCapture, data: dict, msg: bool = True, single_data: bool = True) -> None:
        """Check the output."""

    data: Tuple[Tuple[Result, dict, bool], ...] = (
        # result, data, has_single_response
        # Empty res_data
        (_make_result(code=1000, msg="Quagaars!", res_data=[]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [], "sv_tr_id": ""},
         False),

        # Single res_data
        (_make_result(code=1000, msg="Quagaars!", res_data=[DummyResData("asdf", "asdf")]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [{'foo': 'asdf', 'bar': 'asdf'}], "sv_tr_id": ""},
         False),

        # Multiple res_data
        (_make_result(code=1000, msg="Quagaars!", res_data=[DummyResData("asdf", "asdf"), DummyResData("foo", "bar")]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000,
          "data": [{'foo': 'asdf', 'bar': 'asdf'}, {'foo': 'foo', 'bar': 'bar'}], "sv_tr_id": ""},
         False),

        # msg_q
        (_make_result(code=1000, msg="Quagaars!", res_data=None,
                      msg_q=MsgQ(42, "ARNOLD", None, IdleContactDeletion("RIMMER"))),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [],
          "msg_q": {"msg_type": "{http://www.nic.cz/xml/epp/contact-1.6}idleDelData", "id": "ARNOLD", "count": 42,
                    "q_date": None, "msg": {"id": "RIMMER"}}, "sv_tr_id": ""},
         False),

        # msg_q raw message
        (_make_result(code=1000, msg="Quagaars!", res_data=None,
                      msg_q=RawPollMessage(data=EM().message(msg="Gazpacho!"))),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [], "msg_q": {"data": '<message msg="Gazpacho!"/>'},
          "sv_tr_id": ""},
         False),

        # msg_q not none, single res data
        (_make_result(code=1000, msg="Quagaars!", res_data=[{'foo': 'asdf', 'bar': 'asdf'}],
                      msg_q=MsgQ(42, "ARNOLD", None, IdleContactDeletion("RIMMER"))),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [{'foo': 'asdf', 'bar': 'asdf'}],
          "msg_q": {"msg_type": "{http://www.nic.cz/xml/epp/contact-1.6}idleDelData", "id": "ARNOLD", "count": 42,
                    "q_date": None, "msg": {"id": "RIMMER"}}, "sv_tr_id": ""}, False),

        # errors not none
        (_make_result(code=2000, msg="Quagaars!", res_data=None,
                      msg_q=None, values=[EM().contact(id="foobar")],
                      ext_values=[ExtValue(reason="foo", value=EM().domain(name="baz"))]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 2000, "data": [], "errors": {
             "values": ['<contact id="foobar"/>'], "ext_values": [{"reason": "foo", "value": '<domain name="baz"/>'}]
         }, "sv_tr_id": ""},
         False),

        # there are extensions
        (_make_result(code=2000, msg="Quagaars!", res_data=None,
                      msg_q=None, extensions=[DummyResData("foobar", "baz")]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 2000, "data": [], "extensions": [{"foo": "foobar", "bar": "baz"}],
          "sv_tr_id": ""},
         False),

        # String res_data
        (_make_result(code=1000, msg="Quagaars!", res_data=['Fried chicken']),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": ['Fried chicken'], "sv_tr_id": ""},
         False),  # non-dicts get parsed as lists

        # Multiple, non-dict res_data
        (_make_result(code=1000, msg="Quagaars!", res_data=['Fried chicken', 'Leipziger Allerlei']),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": ['Fried chicken', 'Leipziger Allerlei'],
          "sv_tr_id": ""},
         False),

        # Enum in res_data
        (_make_result(code=1000, msg="Quagaars!", res_data=[EnumResData()]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [{"member": 42}], "sv_tr_id": ""},
         False),

        # bytes in res_data
        (_make_result(code=1000, msg="Quagaars!", res_data=[b"hello world."]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": ["hello world."], "sv_tr_id": ""},
         False),

        (_make_result(code=1000, msg="Quagaars!", res_data=[b"\xffhello world."]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": ["b'\\xffhello world.'"], "sv_tr_id": ""},
         False),

        # Datetime res_data
        (_make_result(code=1000, msg="Quagaars!", res_data=[date(1988, 9, 6)]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [date(1988, 9, 6)], "sv_tr_id": ""},
         False),

        (_make_result(code=1000, msg="Quagaars!", res_data=[datetime(1988, 9, 6)]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [datetime(1988, 9, 6)], "sv_tr_id": ""},
         False),
    )

    dummy_response_out = {"msg": None, "code": None, "data": [{"random": 42}]}

    @final
    def test_outputs(self):
        for result, result_data, single_data in self.data:
            with cast(TestCase, self).subTest(data=result_data):
                with OutputCapture() as output:
                    self.handler.output(result)

                self.assertOutput(output, result_data, single_data=single_data)

    decimal_output_value: Any = '42.01234567890123456789'

    def test_output_response(self):
        with OutputCapture() as output:
            self.handler.output(DummyResponse())

        self.assertOutput(output, self.dummy_response_out, False)

    def test_decimal(self):
        result = _make_result(code=1000, msg="Quagaars!", res_data=[Decimal('42.01234567890123456789')])

        with OutputCapture() as output:
            self.handler.output(result)

        self.assertOutput(output, {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000,
                                   "data": [self.decimal_output_value], "sv_tr_id": ""},
                          single_data=False)
        # Check all decimal places are in the output.
        cast(TestCase, self).assertIn('42.01234567890123456789', output.captured)

    def test_unicode(self):
        result = _make_result(code=1000, msg="Quagaars!", res_data=['Pečené kuře'])

        with OutputCapture() as output:
            self.handler.output(result)

        result_data = {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": ['Pečené kuře'], "sv_tr_id": ""}
        self.assertOutput(output, result_data, single_data=False)
        # Explicit check of the unicode in the output.
        cast(TestCase, self).assertIn('Pečené kuře', output.captured)

    def test_no_cltrid(self):
        result: Result = Result(code=1000, msg="Quagaars!", res_data=[], cl_tr_id=None, sv_tr_id="")

        with OutputCapture() as output:
            self.handler.output(result)

        result_data = {"msg": "Quagaars!", "code": 1000, "data": [], "sv_tr_id": ""}
        self.assertOutput(output, result_data, single_data=False)


class OutputHandlerTest(OutputTestMixin, TestCase):
    # The precision of the decimal number the YAML implementation is able to load.
    # Only used in tests.
    decimal_output_value: Any = 42.012345678901234

    def setUp(self):
        self.handler = OutputHandler(False)

    def assertOutput(self, output: OutputCapture, data: dict, msg: bool = True, single_data: bool = True) -> None:
        if msg:
            self.assertRegex(output.captured, r'^{msg} \({code}\)\n\n'.format(**data))
            body = '\n'.join(output.captured.splitlines()[1:])
        else:
            self.assertNotRegex(output.captured, r'^{msg} \({code}\)\n\n'.format(**data))
            body = output.captured

        data = deepcopy(data)
        data.pop("msg")
        data.pop("code")
        if single_data:
            data = data['data'][0]
        elif data["data"] == []:
            data.pop("data")
        self.assertEqual(list(yaml.safe_load_all(body)), [data] if data else [], body)

    def test_output_syntax(self):
        result = _make_result(code=1000, msg="Quagaars!", res_data=[DummyResData("asdf", "asdf")])
        handler = OutputHandler(True)

        # XXX: OutputCapture doesn't capture the highlighted text.
        with OutputCapture() as output:
            with patch('eppic.output.print_formatted_text') as mock_print:
                handler.output(result)

        output.compare("Quagaars! (1000)\n\n")
        mock_print.assert_called_once()

    def test_output_disclose(self):
        result_data = InfoContactResultData(
            id="foo", postal_info=cast(PostalInfo, "bar"), disclose=Disclose(True, set(DiscloseField)),
            voice=None, fax=None, email=None, vat=None, ident=None, notify_email=None, roid="", statuses=[],
            cl_id="", cr_id=None, cr_date=None, up_id=None, up_date=None, tr_date=None, auth_info=None
        )
        handler = OutputHandler(True)
        self.assertEqual(handler._serialize(result_data)["disclose"],
                         {"disclosed": set(DiscloseField), "undisclosed": set()})


class JsonOutputHandlerTest(OutputTestMixin, TestCase):
    data = OutputTestMixin.data[:-2] + (
        # JSON can't deserialize datetimes
        (_make_result(code=1000, msg="Quagaars!", res_data=[date(1988, 9, 6)]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": ['1988-09-06'], "sv_tr_id": ""},
         False),
        (_make_result(code=1000, msg="Quagaars!", res_data=[datetime(1988, 9, 6)]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": ['1988-09-06T00:00:00'], "sv_tr_id": ""}, False),
        # Nor can it deserialize sets
        (_make_result(code=1000, msg="Quagaars!", res_data=[{"asdf", "asdfg"}]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [["asdf", "asdfg"]], "sv_tr_id": ""}, False),
    )

    def setUp(self):
        self.handler = JsonOutputHandler()

    def assertOutput(self, output: OutputCapture, data: dict, msg: bool = True, single_data: bool = True) -> None:
        self.assertEqual(json.loads(output.captured), data)


class YamlOutputHandlerTest(OutputTestMixin, TestCase):

    data = OutputTestMixin.data[:-4] + (
        (_make_result(code=1000, msg="Quagaars!", res_data=[b"hello world."]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [b"hello world."], "sv_tr_id": ""},
         False),
        # parse binary according to yaml spec.
        (_make_result(code=1000, msg="Quagaars!", res_data=[b"\xffhello world."]),
         {"cl_tr_id": "", "msg": "Quagaars!", "code": 1000, "data": [b'\xffhello world.'], "sv_tr_id": ""},
         False),
    ) + OutputTestMixin.data[-2:]

    def setUp(self):
        self.handler = YamlOutputHandler()

    def assertOutput(self, output: OutputCapture, data: dict, msg: bool = True, single_data: bool = True) -> None:
        self.assertEqual(yaml.safe_load(output.captured), data)
