from unittest import TestCase
from unittest.mock import Mock, patch, sentinel

from epplib.commands.list import GetResults, GetResultsResult
from epplib.commands.update import UpdateContact
from testfixtures import OutputCapture

from eppic.app import Application
from eppic.commands.hooks import apply_disclose_policy, get_results_hook

from .utils import DummyEppCommand, GetAppMixin


class TestGetResultsHook(GetAppMixin, TestCase):

    def _prep_get_results(self, enabled=True):
        settings = self.get_settings(confirm=True)
        settings.print_list_results_automatically = enabled
        app = Application(settings, "foobar asdf")
        self.epp_handler.send_request.return_value = GetResultsResult(code=1000, msg="All good", res_data=[],
                                                                      cl_tr_id='', sv_tr_id='')
        return app

    def test_automatically_print_results(self):
        app = self._prep_get_results(True)
        with OutputCapture() as output:
            with patch("eppic.output.print_formatted_text") as formatted:
                get_results_hook(app, Mock())

        formatted.assert_called()
        output.compare("All good (1000)\n")
        self.epp_handler.send_request.assert_called_once()
        # check that it was called with the `GetResults` command instance.
        arg = self.epp_handler.send_request.call_args[0][0]
        self.assertIsInstance(arg, GetResults)

    def test_automatically_print_results_multiple(self):
        app = self._prep_get_results(True)
        results = [["foo", "bar"], ["foobar"], [], ["baz"]]
        self.epp_handler.send_request = lambda x: GetResultsResult(code=1000, msg="All good",
                                                                   res_data=results.pop(0),  # type: ignore
                                                                   cl_tr_id='', sv_tr_id='')

        with OutputCapture() as output:
            with patch("eppic.output.print_formatted_text") as formatted:
                get_results_hook(app, Mock())

        formatted.assert_called()
        output.compare("All good (1000)\n")
        # check that `GetResults` was called until the empty list
        self.assertEqual(results, [["baz"]])

    def test_automatically_print_results_disabled(self):
        app = self._prep_get_results(False)
        get_results_hook(app, Mock())
        self.epp_handler.send_request.assert_not_called()


class TestDisclosePolicy(GetAppMixin, TestCase):

    def setUp(self):
        super().setUp()
        self.policy = Mock()
        self.epp_handler.policy = self.policy
        self.disclosed_request = UpdateContact

    def test_apply_disclose_wrong_command(self):
        self.assertRaises(AssertionError, apply_disclose_policy, self.get_app(), DummyEppCommand("foobar"))

    def test_apply_disclose_disclose_is_none(self):
        apply_disclose_policy(self.get_app(), self.disclosed_request("asdf"))
        self.policy.apply_disclose_policy.assert_not_called()

    def test_apply_disclose_correct(self):
        apply_disclose_policy(self.get_app(), self.disclosed_request("asdf", disclose=sentinel.disclose))
        self.policy.apply_disclose_policy.assert_called_once_with(sentinel.disclose)
