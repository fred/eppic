"""Test the Argument class."""
import datetime
from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum
from typing import Any, List, Optional, Sequence, Tuple, Type, Union, cast, get_args
from unittest import TestCase
from unittest.mock import Mock

from epplib.models.common import Disclose, DiscloseField

from eppic.commands.arguments import (
    Argument,
    BoolArgument,
    BytesArgument,
    DataclassArgument,
    DateArgument,
    DateTimeArgument,
    DiscloseArgument,
    EnumArgument,
    SequenceArgument,
    UnionArgument,
)
from eppic.exceptions import CommandParseError
from eppic.models import ArgumentField, ArgumentLabels, ParsedArgument


class GenerateAvailableLabelsTestMixin(ABC):

    # inputed, generated, argument required
    remaining_argument_test_examples: List[Tuple[List[List[str]], List[List[str]]]]

    @abstractmethod
    def get_argument(self, *, required: bool) -> Argument:
        """Get the argument, depending required."""

    def test_generate_remaining_arguments(self):
        for inp, expected in self.remaining_argument_test_examples:
            for arg_required in [True, False]:
                with cast(TestCase, self).subTest(inputed=inp, arg_required=arg_required):
                    arg = self.get_argument(required=True)
                    output = arg.get_available_labels([ParsedArgument(labels=x, value="asdf") for x in inp])
                    o_labels = [x.labels for x in output]
                    cast(TestCase, self).assertEqual(sorted(o_labels), sorted(expected))

    def test_generate_remaining_arguments_from_none(self):
        for x in [True, False]:
            with cast(TestCase, self).subTest(required=x):
                arg = self.get_argument(required=x)
                cast(TestCase, self).assertEqual(
                    arg.get_available_labels([ParsedArgument(labels=None, value="asdf")]),
                    arg.get_available_labels([]),
                )

    def test_generate_remaining_arguments_from_empty_label(self):
        for x in [True, False]:
            with cast(TestCase, self).subTest(required=x):
                arg = self.get_argument(required=x)
                cast(TestCase, self).assertEqual(
                    arg.get_available_labels([ParsedArgument(labels=[], value="asdf")]),
                    arg.get_available_labels([]),
                )


class ArgumentParsingTestMixin(ABC):

    # inputed args, expected_value, is required (None means it doesn't matter)
    correct_arguments: Sequence[Tuple[List[ParsedArgument], Any, Optional[bool]]]

    # inputed, error, error msg, argument required (None means it doesn't matter)
    incorrect_arguments: Sequence[Tuple[List[ParsedArgument], Type[CommandParseError], str, Optional[bool]]]

    @abstractmethod
    def get_argument(self, *, required: bool) -> Argument:
        """Get the argument, depending required."""

    def test_correct_argument_parsing(self):
        for inputed, expected, submitted_required in self.correct_arguments:
            required_list = [True, False] if submitted_required is None else [submitted_required]
            for required in required_list:
                with cast(TestCase, self).subTest(inputed=inputed, required=required):
                    value = self.get_argument(required=required).parse_value(inputed)
                    cast(TestCase, self).assertEqual(
                        value, expected)

    def test_incorrect_argument_parsing(self):
        for inputed, error, message, submitted_required in self.incorrect_arguments:
            required_list = [True, False] if submitted_required is None else [submitted_required]
            for required in required_list:
                with cast(TestCase, self).subTest(inputed=inputed, required=required):
                    cast(TestCase, self).assertRaisesRegex(
                        error, message, self.get_argument(required=required).parse_value, inputed)

    def test_required_nothing_in(self):
        cast(TestCase, self).assertRaises(CommandParseError, self.get_argument(required=True).parse_value, [])

    def test_not_required_nothing_in(self):
        cast(TestCase, self).assertIsNone(self.get_argument(required=False).parse_value([]))


class TestArgument(GenerateAvailableLabelsTestMixin, ArgumentParsingTestMixin, TestCase):
    """Test Argument."""

    remaining_argument_test_examples = [
        ([["foobar"]], []),
        ([["foobar"]], []),
        ([], [["foobar"]]),
        ([["foobar"]], []),
        ([], [["foobar"]]),
    ]

    correct_arguments = [
        ([ParsedArgument(["foobar"], "42")], 42, None),
    ]
    incorrect_arguments = [
        ([ParsedArgument(["foobar"], "asdf")], CommandParseError,
         "Supplied value to argument foobar is of incorrect type.", None),
        ([ParsedArgument(["foobar"], "41"), ParsedArgument(["foobar"], "43")], CommandParseError,
         "Too many values for foobar.", None),
    ]

    def get_argument(self, *, required: bool = True) -> Argument:
        """Get an argument."""
        return Argument(ArgumentField("foobar", int, required), Mock())

    @property
    def argument(self) -> Argument:
        """Generate a sample argument."""
        return self.get_argument()

    def test_name_dash(self):
        argument = Argument(ArgumentField("foo_bar", int, True), Mock())
        self.assertFalse(argument.recognise_label(["foo_bar"]))
        self.assertTrue(argument.recognise_label(["foo-bar"]))

    def test_get_subarguments_full_label(self) -> None:
        """Test get_subarguments_full_label."""
        self.assertEqual(
            self.argument.get_subarguments_full_label(["baz"]),
            ["foobar", "baz"]
        )

    def test_recognise_label_true(self) -> None:
        """Test correct labels."""
        self.assertTrue(self.argument.recognise_label(["foobar"]))

    def test_recognise_label_false(self) -> None:
        """Test incorrect labels."""
        examples: List[ArgumentLabels] = [
            ["foobaar"],
            ["foobar", "asdf"],
            ["1foobar"],
            ["foobar1"],
            ["foobarasdf", "baz"],
            ["asdf", "foobar"],
            [],
        ]

        for example in examples:
            with self.subTest(example=example):
                self.assertFalse(self.argument.recognise_label(example))


class SampleEnum(Enum):
    FOO = "foo"
    BAZ = "baz"


class TestEnumArgument(GenerateAvailableLabelsTestMixin, ArgumentParsingTestMixin, TestCase):
    """Test Argument."""

    remaining_argument_test_examples = [
        ([["foobar"]], []),
        ([], [["foobar"]]),
        ([["foobar"]], []),
        ([], [["foobar"]]),
    ]

    correct_arguments = [
        ([ParsedArgument(["foobar"], "FOO")], SampleEnum.FOO, None),
        ([ParsedArgument(["foobar"], "BAZ")], SampleEnum.BAZ, None),
    ]
    incorrect_arguments = [
        ([ParsedArgument(["foobar"], "BAR")], CommandParseError,
         "Supplied value to argument foobar is not among accepted values: FOO, BAZ.", None),
        ([ParsedArgument(["foobar"], "BAR"), ParsedArgument(["foobar"], "BAZ")], CommandParseError,
         "Too many values for foobar.", None),
    ]

    def get_argument(self, *, required: bool = True) -> Argument:
        """Get the argument."""
        return EnumArgument(ArgumentField("foobar", SampleEnum, required), Mock())


@dataclass
class SampleDataclass:
    """Sample testing dataclass."""

    foo: int
    bar: Optional[int] = None


@dataclass
class SecondSampleDataclass:
    """Sample testing dataclass."""

    foo: int
    foobar: int
    bar: Optional[int] = None


@dataclass
class AllOptional:
    """All optional dataclass."""

    foo: Optional[int] = None
    bar: Optional[int] = None


class TestDataclassArgument(ArgumentParsingTestMixin, GenerateAvailableLabelsTestMixin, TestCase):
    """Test DataclassArgument class."""

    remaining_argument_test_examples = [
        ([["foobarbaz"]], [["foobar", "foobar"], ["foobar", "foo"], ["foobar", "bar"]]),
        ([], [["foobar", "foo"], ["foobar", "foobar"], ["foobar", "bar"]]),
        ([["foobar", "foobar"]], [["foobar", "foo"], ["foobar", "bar"]]),
        ([["foobar", "foo"], ["foobar", "foobar"]], [["foobar", "bar"]]),
        ([["foobar", "foo"], ["foobar", "bar"]], [["foobar", "foobar"]]),
        ([["foobar", "foo"]], [["foobar", "bar"], ["foobar", "foobar"]]),
        ([], [["foobar", "foo"], ["foobar", "bar"], ["foobar", "foobar"]]),
        ([["foobar", "bar"]], [["foobar", "foo"], ["foobar", "foobar"]]),
    ]

    correct_arguments = [
        ([ParsedArgument(["foobar", "foo"], "42"), ParsedArgument(["foobar", "foobar"], "43")],
         SecondSampleDataclass(42, 43), None),
        ([ParsedArgument(["foobar", "foobar"], "43"), ParsedArgument(["foobar", "foo"], "42")],
         SecondSampleDataclass(42, 43), None),
        ([ParsedArgument(["foobar", "foo"], "42"), ParsedArgument(["foobar", "foobar"], "43"),
          ParsedArgument(["foobar", "bar"], "44")],
         SecondSampleDataclass(42, 43, 44), None),
    ]
    incorrect_arguments = [
        ([ParsedArgument(["foobar", "foo"], "42"), ParsedArgument(["foobar", "foobar"], "4a3")],
         CommandParseError, "Supplied value to argument foobar is of incorrect type.", None),
        ([ParsedArgument(["foobar"], "BAR")], CommandParseError,
         "Required argument foo didn't receive a value.", True),
        ([ParsedArgument(["foobar", "foo"], "42")], CommandParseError,
         "Required argument foobar didn't receive a value.", None),
        ([ParsedArgument(["foobar", "foobar"], "42")], CommandParseError,
         "Required argument foo didn't receive a value.", None),
        ([ParsedArgument(["foobar", "bar"], "42")], CommandParseError,
         "Required argument foo didn't receive a value.", None),
    ]

    def get_argument(self, *, dc_type: Type = SecondSampleDataclass, required: bool = True) -> Argument:
        """Get the argument."""
        return DataclassArgument(
            ArgumentField("foobar", dc_type, required),
            lambda x: Argument(ArgumentField(x.name, int, type(None) not in get_args(x.type)), Mock())
        )

    def test_all_empty_arguments(self):
        value = self.get_argument(dc_type=AllOptional, required=False).parse_value([])
        self.assertIsNone(value)

    @property
    def argument(self) -> Argument:
        """Generate a sample argument."""
        return self.get_argument(dc_type=SampleDataclass)

    def test_recognise_labels(self):
        tests = [
            ([""], False),
            (["foobar"], False),
            (["foobar-1"], False),
            (["foobar-213asdfasdf"], False),
            (["fobaar"], False),
            (["foobar-1", "adsf"], False),
            (["foobar--asdf"], False),
            (["foobar", "foo"], True),
            (["foobar", "bar"], True),
            (["foobar", "baz"], False),
        ]
        for val, exp in tests:
            with self.subTest(val=val, exp=exp):
                self.assertEqual(self.argument.recognise_label(val), exp)


class TestSequenceArgument(ArgumentParsingTestMixin, GenerateAvailableLabelsTestMixin, TestCase):
    """Test SequenceArgument class."""

    # inputed, generated, argument required, generate only required
    remaining_argument_test_examples = [
        ([["barbaz"]], [["foobar-1", "foo"], ["foobar-1", "bar"], ["foobar-1", "foobar"]]),
        ([["barbaz"]], [["foobar-1", "foo"], ["foobar-1", "bar"], ["foobar-1", "foobar"]]),
        ([], [["foobar-1", "foo"], ["foobar-1", "bar"], ["foobar-1", "foobar"]]),
        ([], [["foobar-1", "foo"], ["foobar-1", "bar"], ["foobar-1", "foobar"]]),
        ([["foobar", "foo"], ["foobar", "bar"]],
         [["foobar", "foobar"], ["foobar-1", "foo"], ["foobar-1", "bar"], ["foobar-1", "foobar"]]),
        ([["foobar-1", "foo"], ["foobar-1", "bar"], ["foobar-1", "foobar"]],
         [["foobar-2", "foo"], ["foobar-2", "bar"], ["foobar-2", "foobar"]]),
    ]

    correct_arguments = [
        ([ParsedArgument(["foobar", "foo"], "42"), ParsedArgument(["foobar", "foobar"], "43")],
         [SecondSampleDataclass(42, 43)], None),
        ([ParsedArgument(["foobar-1", "foo"], "42"), ParsedArgument(["foobar-1", "foobar"], "43"),
          ParsedArgument(["foobar-2", "foo"], "44"), ParsedArgument(["foobar-2", "foobar"], "45")],
         [SecondSampleDataclass(42, 43), SecondSampleDataclass(44, 45)], None),
    ]
    incorrect_arguments = [
        ([ParsedArgument(["foobar-1", "foo"], "42"), ParsedArgument(["foobar-1", "foobar"], "4a3")],
         CommandParseError, "Supplied value to argument foobar is of incorrect type.", None),
        ([ParsedArgument(["foobar-1", "foobar"], "43")], CommandParseError,
         "Required argument foo didn't receive a value.", None),
        ([ParsedArgument(["foobar", "foo"], "42")], CommandParseError,
         "Required argument foobar didn't receive a value.", True),
        ([ParsedArgument(["foobar-1", "foo"], "42"), ParsedArgument(["foobar-1", "foobar"], "43"),
          ParsedArgument(["foobar-2", "foo"], "44"), ParsedArgument(["foobar-2", "foobar"], "45"),
          ParsedArgument(["foobar-3", "foobar"], "42")], CommandParseError,
         "Required argument foo didn't receive a value.", True),
        ([ParsedArgument(["foobar", "baz"], "42")], CommandParseError,
         "Required argument foo didn't receive a value.", True),
    ]

    def test_required_nothing_in(self):
        cast(TestCase, self).assertEqual(self.get_argument(required=True).parse_value([]), [])

    def test_not_required_nothing_in(self):
        cast(TestCase, self).assertEqual(self.get_argument(required=False).parse_value([]), [])

    def get_argument(self, *, seq_type: Type = SecondSampleDataclass, required: bool = True) -> Argument:
        """Get the argument."""
        return SequenceArgument(
            ArgumentField("foobar", Sequence[seq_type], True),  # type: ignore[valid-type]
            lambda x: DataclassArgument(
                ArgumentField(x.name, seq_type, True),
                lambda x: Argument(ArgumentField(x.name, int, type(None) not in get_args(x.type)), Mock())
            ))

    @property
    def argument(self) -> Argument:
        """Generate a sample argument."""
        return SequenceArgument(ArgumentField("foobar", Sequence[int], True),
                                lambda x: Argument(ArgumentField(x.name, int, x.required), Mock()))

    def test_recognise_labels(self):
        tests = [
            ([""], False),
            (["foobar"], True),
            (["foobar-1"], True),
            (["foobar-213asdfasdf"], True),
            (["fobaar"], False),
            (["foobar-1", "adsf"], True),
            (["foobar--asdf"], True),
        ]
        for val, exp in tests:
            with self.subTest(val=val, exp=exp):
                self.assertEqual(self.argument.recognise_label(val), exp)


class TestDiscloseArgument(ArgumentParsingTestMixin, GenerateAvailableLabelsTestMixin, TestCase):
    """Test DiscloseArgument."""

    remaining_argument_test_examples = [
        ([["foobar"]], []),
        ([["unfoobar"]], []),
        ([["foobar"]], []),
        ([["unfoobar"]], []),
        ([], [["foobar"], ["unfoobar"]]),
        ([], [["foobar"], ["unfoobar"]]),
    ]

    correct_arguments = [
        ([ParsedArgument(["foobar"], "email")], Disclose(True, {DiscloseField.EMAIL}), None),
        ([ParsedArgument(["unfoobar"], "email")], Disclose(False, {DiscloseField.EMAIL}), None),
        ([ParsedArgument(["foobar"], "email,addr")], Disclose(True, {DiscloseField.EMAIL, DiscloseField.ADDR}), None),
        ([ParsedArgument(["unfoobar"], "email,addr")], Disclose(False, {DiscloseField.EMAIL, DiscloseField.ADDR}),
         None),
        ([ParsedArgument(["foobar"], "")], Disclose(True, set()), None),
        ([ParsedArgument(["unfoobar"], "")], Disclose(False, set()), None),
    ]
    incorrect_arguments = [
        ([ParsedArgument(["foobar"], "asdf")], CommandParseError, "'asdf' is not a valid DiscloseField",
         None),
        ([ParsedArgument(["foobar"], "BAR"), ParsedArgument(["foobar"], "BAZ")], CommandParseError,
         "Too many values for foobar.", None),
        ([ParsedArgument(["unfoobar"], "asdf")], CommandParseError, "'asdf' is not a valid DiscloseField",
         None),
        ([ParsedArgument(["unfoobar"], "BAR"), ParsedArgument(["unfoobar"], "BAZ")], CommandParseError,
         "Too many values for unfoobar.", None),
        ([ParsedArgument(["foobar"], "asdf"), ParsedArgument(["unfoobar"], "asdf")], CommandParseError,
         "Trying to use both disclose and undisclose. Choose one.", None),
    ]

    @property
    def argument(self) -> DiscloseArgument:
        """Generate a sample argument."""
        return self.get_argument(required=True)

    def get_argument(self, *, required: bool = True) -> DiscloseArgument:
        """Get disclose argument."""
        return DiscloseArgument(ArgumentField("foobar", Disclose, required),
                                lambda x: Argument(ArgumentField(x.name, str, x.required), Mock()))

    def test_recognition(self):
        argument = self.argument
        labels = ["foobar", "unfoobar"]
        for label in labels:
            with self.subTest(label=label):
                self.assertTrue(argument.recognise_label([label]))


class TestBytesArgument(ArgumentParsingTestMixin, TestCase):
    """Test Bytes argument class."""

    correct_arguments = [
        ([ParsedArgument(["foobar"], "asdf")], b"asdf", None),
        ([ParsedArgument(["foobar"], "asdf")], b"asdf", None),
        ([ParsedArgument(["foobar"], "pštros")], b"p\xc5\xa1tros", None),
    ]
    incorrect_arguments = []  # tested in `TestArgument`

    def get_argument(self, *, required: bool = True) -> BytesArgument:
        """Get bytes argument."""
        return BytesArgument(ArgumentField("foobar", bytes, required), Mock())


class TestDateTimeArgument(ArgumentParsingTestMixin, TestCase):
    """Test DateTime class."""

    correct_arguments = [
        ([ParsedArgument(["foobar"], "2022-09-09T07:58:20")],
         datetime.datetime(year=2022, month=9, day=9, hour=7, minute=58, second=20), None),
    ]
    incorrect_arguments = [
        ([ParsedArgument(["foobar"], "2022-09-09T07:58:asdf20")], CommandParseError,
         "Supplied value to argument foobar is of incorrect format.", None),
    ]

    def get_argument(self, *, required: bool = True) -> Argument:
        """Get an argument."""
        return DateTimeArgument(ArgumentField("foobar", datetime.datetime, required), Mock())


class TestDateArgument(ArgumentParsingTestMixin, TestCase):
    """Test DateTime class."""

    correct_arguments = [
        ([ParsedArgument(["foobar"], "2022-09-09")],
         datetime.date(year=2022, month=9, day=9), None),
    ]
    incorrect_arguments = [
        ([ParsedArgument(["foobar"], "2022-09-09T07:58:20")], CommandParseError,
         "Supplied value to argument foobar is of incorrect format.", None),
    ]

    def get_argument(self, *, required: bool = True) -> Argument:
        """Get an argument."""
        return DateArgument(ArgumentField("foobar", datetime.date, required), Mock())


class TestBoolArgument(ArgumentParsingTestMixin, GenerateAvailableLabelsTestMixin, TestCase):
    """Test Argument."""

    remaining_argument_test_examples = [
        ([["foobar"]], []),
        ([], [["foobar"]]),
        ([["foobar"]], []),
        ([], [["foobar"]]),
    ]

    correct_arguments = [
        ([ParsedArgument(["foobar"], v)], True, None)
        for v in BoolArgument.true] + [
        ([ParsedArgument(["foobar"], v)], False, None)
        for v in BoolArgument.false
    ]
    incorrect_arguments = [
        ([ParsedArgument(["foobar"], "2022-09-09T07:58:20")], CommandParseError,
         "Supplied value to argument foobar is of incorrect format.", None),
    ]

    def get_argument(self, *, required: bool = True) -> Argument:
        """Get an argument."""
        return BoolArgument(ArgumentField("foobar", bool, required), Mock())


class TestUnionArgument(ArgumentParsingTestMixin, GenerateAvailableLabelsTestMixin, TestCase):
    """Test Union argument."""

    remaining_argument_test_examples = [
        # nothing filled in yet, suggest all args
        ([], [["foobar"], ["foobar", "foo"], ["foobar", "bar"]]),
        # chosen arg w no more labels, return []
        ([["foobar"]], []),
        ([["foobar"]], []),
        # chosen arg w more labels, return them
        ([["foobar", "foo"]], [["foobar", "bar"]]),
        ([["foobar", "foo"]], [["foobar", "bar"]]),
        # chosen arg w more required labels, return them
        ([["foobar", "bar"]], [["foobar", "foo"]]),
        ([["foobar", "bar"]], [["foobar", "foo"]]),
    ]

    correct_arguments = [
        ([ParsedArgument(["foobar"], "42")], 42, None),
        ([ParsedArgument(["foobar"], "asdf")], "asdf", None),
        ([ParsedArgument(["foobar", "foo"], "42")],
         SampleDataclass(42), None),
        ([ParsedArgument(["foobar", "foo"], "42"),
          ParsedArgument(["foobar", "bar"], "44")],
         SampleDataclass(42, 44), None),
    ]
    incorrect_arguments = [
        ([ParsedArgument(["foobar"], "BAR"), ParsedArgument(["foobar"], "BAZ")], CommandParseError,
         "Values supplied to foobar weren't consistent with any of its subtypes.", None),
        ([ParsedArgument(["foobar", "foo"], "42"), ParsedArgument(["foobar", "bar"], "4a3")],
         CommandParseError, "Values supplied to foobar weren't consistent with any of its subtypes.", None),
        ([ParsedArgument(["foobar", "foobar"], "42")], CommandParseError,
         "Values supplied to foobar weren't consistent with any of its subtypes.", True),
        ([ParsedArgument(["foobar", "bar"], "42")], CommandParseError,
         "Values supplied to foobar weren't consistent with any of its subtypes.", True),
    ]

    def get_argument(self, *, type: object = Union[str, SampleDataclass, int], required: bool = True) -> Argument:
        """Get an argument."""
        return UnionArgument(ArgumentField("foobar", cast(Type, type), required), self._factory)

    def _factory(self, field: ArgumentField) -> Argument:
        if field.type == SampleDataclass:
            return DataclassArgument(field, self._factory)

        if field.type == Optional[int]:
            field = ArgumentField(name=field.name, required=False, type=int)

        return Argument(field, self._factory)

    def test_recognise_labels(self):
        tests = [
            ([""], False),
            (["foobar"], True),
            (["foobar_1"], False),
            (["foobar_213asdfasdf"], False),
            (["fobaar"], False),
            (["foobar", "foo"], True),
        ]
        argument = UnionArgument(ArgumentField("foobar", cast(Type, Union[int, SampleDataclass]), True), self._factory)
        for val, exp in tests:
            with self.subTest(val=val, exp=exp):
                self.assertEqual(argument.recognise_label(val), exp)
