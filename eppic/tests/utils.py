import tempfile
from dataclasses import dataclass
from pathlib import Path
from typing import Any, List, Mapping, Optional, Tuple, Type, cast
from unittest import TestCase
from unittest.mock import MagicMock, Mock, patch, sentinel

from epplib.client import Client
from epplib.commands.base import Command as EppRequest
from epplib.responses.base import Response
from lxml.etree import Element
from pydantic_settings import BaseSettings, PydanticBaseSettingsSource

from eppic.app import Application
from eppic.commands.command import Command
from eppic.commands.service_requests import ServiceRequest
from eppic.output import OutputHandler
from eppic.settings import InputFormat, OutputFormat, PrintRawXml, Session, Settings

DEFAULT_SESSIONS = [
    Session(hostname="asdf", port=123, username="foo",
            password="passwd", obj_uris=["asdf"], cert_file=__file__, key_file=__file__),
    Session(hostname="asdf", port=123, username="asdff",
            password="asdf", obj_uris=["asdf"], cert_file=__file__, key_file=__file__),
]


class DummyCommand(Command):

    def _run(self, app: "Application", executable_command: EppRequest) -> None:  # pragma: no cover
        pass


@dataclass
class DummyServiceCommand(ServiceRequest):

    foobar: str
    baz: Optional[int] = None

    handle = Mock()


@dataclass
class DummyEppCommand(EppRequest):

    foobar: str
    baz: Optional[int] = None

    def xml(self, *args, **kwargs):  # pragma: no cover
        """Return xml."""
        return Mock(spec=Element)

    def _get_command_payload(self) -> None:
        """Implement dummy method."""


@dataclass
class DummyResponse(Response):

    @classmethod
    def _extract_payload(cls, element: Any) -> Mapping[str, Any]:  # pragma: no cover
        """Implement dummy method."""
        return {}


class GetAppMixin:

    default_sessions = DEFAULT_SESSIONS

    def get_settings(self, *, confirm: bool = False, sessions: List[Session] = DEFAULT_SESSIONS,
                     syntax_highlighting: bool = True, autocomplete: bool = True,
                     input_format: InputFormat = InputFormat.DEFAULT, input_file: Optional[Path] = None,
                     output_format: OutputFormat = OutputFormat.DEFAULT, use_interactive_input: bool = True,
                     print_raw_xml: PrintRawXml = PrintRawXml.DISABLED, enable_prompt: bool = True,
                     exit_on_error: bool = False
                     ) -> Settings:
        """Get settings."""
        settings = Settings(sessions=sessions, output_format=output_format, input_format=input_format,
                            input_file=input_file, use_interactive_input=use_interactive_input,
                            print_raw_xml=print_raw_xml)
        settings.interactive.autocomplete = autocomplete
        settings.interactive.enable_prompt = enable_prompt
        settings.interactive.command_confirmation = confirm
        settings.interactive.syntax_highlighting = syntax_highlighting
        settings.exit_on_error = exit_on_error

        # setup temporary directory for history
        settings.interactive.history_file_path = self.get_temp_file("asdf")

        return settings

    def get_temp_file(self, content):
        tmp = tempfile.TemporaryDirectory()
        cast(TestCase, self).addCleanup(tmp.cleanup)
        file = Path(tmp.name) / "file"
        with open(file, "w") as w:
            w.write(content)
        return file

    def get_app(self, *, session=DEFAULT_SESSIONS[0], command=None, foobar_pre_hooks=None, foobar_post_hooks=None,
                foobar_is_service=False, **kwargs):
        app = Application(self.get_settings(**kwargs), command, session)
        foobar_spec = DummyEppCommand if not foobar_is_service else DummyServiceCommand
        app.command_registry.register_command(DummyCommand("foobar", cast(Type[DummyEppCommand], foobar_spec),
                                                           app.argument_registry,
                                                           pre_hooks=foobar_pre_hooks or [],
                                                           post_hooks=foobar_post_hooks or [],
                                                           number_of_positional_args=1))
        self.epp_handler.session = session
        self.epp_handler.logged_in = session is not None
        return app

    def setUp(self):
        # Patch `EppHandler` throughout the tests.
        patcher = patch("eppic.app.EppHandler", autospec=True)
        cast(TestCase, self).addCleanup(patcher.stop)
        self.epp_handler_cls = patcher.start()
        self.epp_handler = self.epp_handler_cls.return_value
        self.epp_handler.last_raw_command = b"<command></command>"
        self.epp_handler.last_raw_response = b"<response></response>"
        self.epp_handler.send_request.return_value = DummyResponse()

    def assertEppHandlerCreated(self):
        self.epp_handler_cls.assert_called()

    def assertEppHandlerExited(self) -> None:
        self.epp_handler.logout.assert_called()


class TestClient(Client):
    """EPP client for higher level tests."""

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.mock = Mock()

    def connect(self) -> None:  # pragma: no cover
        """Just record the call."""
        self.mock.connect()

    def close(self) -> None:  # pragma: no cover
        """Just record the call."""
        self.mock.close()

    def send(self, *args: Any, **kwargs: Any) -> Any:
        """Just record the call and return static response."""
        self.mock(*args, **kwargs)
        return sentinel.response


class TestApplication(Application):

    def get_output_handler(self, output_format: OutputFormat, syntax_highlighting: bool = True) -> OutputHandler:
        """Return dummy output handler."""
        return Mock(name='output handler')

    @property
    def epp_mock_calls(self):
        """Return mock calls on the EPP client."""
        return cast(TestClient, self.epp_handler._client).mock.mock_calls


class TestSettings(Settings):
    """Test settings which ignore any other setting sources."""

    @classmethod
    def settings_customise_sources(
        cls, settings_cls: Type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> Tuple[PydanticBaseSettingsSource, ...]:
        """List of source functions."""
        return (
            init_settings,
        )


def get_app(settings: Optional[Settings] = None) -> TestApplication:
    """Return test application, mainly for CLI command tests."""
    settings = settings or TestSettings()
    # No command, no session
    app = TestApplication(settings, None)
    # Simulate handler already logged in.
    app.epp_handler._client = TestClient(sentinel.transport)
    app.epp_handler._session = sentinel.session
    app.epp_handler._session.reconnects = MagicMock()  # type: ignore[union-attr]
    return app
