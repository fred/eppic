import tempfile
from pathlib import Path
from typing import Any, Dict, Optional, cast
from unittest import TestCase
from unittest.mock import patch

from epplib.constants import NAMESPACE
from pydantic import ValidationError

from eppic.settings import SETTINGS_FILE_LOCATIONS, Session, Settings, _parse_settings_files

SESSION = {
    "hostname": "asdf",
    "port": 1234,
    "cert_file": "cert",
    "key_file": "key",
    "username": "asdf",
    "password": "ghjkl"
}


class YamlParserTest(TestCase):

    def setUp(self) -> None:
        self.homeconfig = tempfile.NamedTemporaryFile().name
        self.etcconfig = tempfile.NamedTemporaryFile().name
        self.settings_file: Optional[Path] = Path(tempfile.NamedTemporaryFile().name)
        with open(self.homeconfig, "w") as f:
            f.write("HOME")
        with open(self.etcconfig, "w") as f:
            f.write("ETC")
        with open(self.settings_file, "w") as f:
            f.write("CUSTOM")
        self._settings_original = SETTINGS_FILE_LOCATIONS.copy()
        SETTINGS_FILE_LOCATIONS[0] = self.homeconfig
        SETTINGS_FILE_LOCATIONS[1] = self.etcconfig

    def tearDown(self):
        SETTINGS_FILE_LOCATIONS[0] = self._settings_original[0]
        SETTINGS_FILE_LOCATIONS[1] = self._settings_original[1]

    def test_w_custom_file(self) -> None:
        self.assertEqual(_parse_settings_files(self.settings_file), "CUSTOM")

    def test_w_both_settings_files(self) -> None:
        self.settings_file = None
        self.assertEqual(_parse_settings_files(self.settings_file), "HOME")

    def test_wo_home_settings_file(self) -> None:
        self.settings_file = None
        SETTINGS_FILE_LOCATIONS[0] += "/asdfasdf"
        self.assertEqual(_parse_settings_files(self.settings_file), "ETC")

    def test_wo_settings_files(self) -> None:
        self.settings_file = None
        SETTINGS_FILE_LOCATIONS[0] += "/asdfasdf"
        SETTINGS_FILE_LOCATIONS[1] += "/asdfasdf"
        self.assertEqual(_parse_settings_files(self.settings_file), {})


@patch("eppic.settings._parse_settings_files", new=lambda *args: {})
class SettingsTest(TestCase):

    def setUp(self) -> None:
        self.default_session = SESSION.copy()
        self.default_session["cert_file"] = self.create_key()
        self.default_session["key_file"] = self.create_key()
        self.default_session.pop("username")
        self.default_settings: Dict[str, Any] = {
            "sessions": [
                Session(**self.default_session, username="baz"),
                Session(**self.default_session, username="foo"),
                Session(**self.default_session, username="bar"),
            ]
        }

    def create_key(self):
        filename = tempfile.NamedTemporaryFile().name
        with open(filename, "w") as f:
            f.write("foobar")
        return filename

    def settings(self) -> Settings:
        return Settings(**self.default_settings)

    def test_no_sessions_at_all(self) -> None:
        self.default_settings["sessions"] = []
        settings = self.settings()
        self.assertIsNone(settings.get_session(None))

    def test_session_none(self) -> None:
        self.assertEqual(cast(Session, self.settings().get_session(None)).username, "baz")

    def test_session_but_no_specified(self):
        self.default_settings.pop("sessions")
        settings = self.settings()
        with self.assertRaisesRegex(ValueError, "No sessions specified\\."):
            settings.get_session("hello")

    def test_session_by_name(self) -> None:
        settings = self.settings()
        self.assertEqual(cast(Session, settings.get_session("foo")).username, "foo")

    def test_session_by_alias(self) -> None:
        self.default_settings["sessions"].append(Session(
            **self.default_session, username="notalias", alias="hello"
        ))
        self.assertEqual(self.settings().get_session("hello"), self.default_settings["sessions"][-1])
        self.assertRaises(ValueError, self.settings().get_session, "notalias")

    def test_session_wrong_type(self) -> None:
        self.assertRaises(TypeError, self.settings().get_session, 0.01)

    def test_session_by_name_wrong(self) -> None:
        settings = self.settings()
        self.assertRaises(ValueError, settings.get_session, "nonexistent")

    def test_session_by_number(self) -> None:
        settings = self.settings()
        self.assertEqual(cast(Session, settings.get_session(1)).username, "foo")

    def test_session_by_number_out_of_range(self) -> None:
        for i in [-1, 123456789, -123456789]:
            with self.subTest(i=i):
                settings = self.settings()
                self.assertRaises(ValueError, settings.get_session, i)

    def test_extra_settings(self) -> None:
        self.default_settings["foobar"] = "foobar"
        self.settings()

    def test_settings_file_set(self):
        settings = Settings(**self.default_settings, settings_file=Path("/"))
        self.assertEqual(settings._settings_file, Path("/"))

    def test_input_file_set(self):
        filename = tempfile.NamedTemporaryFile().name
        with open(filename, "w") as f:
            f.write("foobar")

        settings = Settings(**self.default_settings, input_file=filename)
        self.assertEqual(settings.input_file, Path(filename))


class SessionTest(TestCase):

    def setUp(self) -> None:
        self.default_session = SESSION.copy()
        self.default_session["cert_file"] = self.create_key()
        self.default_session["key_file"] = self.create_key()

    def create_key(self):
        filename = tempfile.NamedTemporaryFile().name
        with open(filename, "w") as f:
            f.write("foobar")
        return filename

    def test_session_file_validators_false(self) -> None:
        tmpdir = tempfile.TemporaryDirectory()
        self.default_session["cert_file"] = Path(tmpdir.name)
        self.addCleanup(tmpdir.cleanup)
        self.assertRaises(ValidationError, Session, **self.default_session)

        self.default_session["cert_file"] /= "asdf"  # type: ignore
        self.assertRaises(ValidationError, Session, **self.default_session)

    def test_session_no_client(self) -> None:
        self.default_session.pop("username")
        self.default_session.pop("password")
        session = Session(**self.default_session)
        self.assertIsNone(session.username)
        self.assertIsNone(session.password)

    def test_session_independent_obj_uris(self):
        session1 = Session(**self.default_session)
        session2 = Session(**self.default_session)
        session2.obj_uris.pop(0)
        self.assertNotEqual(len(session1.obj_uris), len(session2.obj_uris))

    def test_info_dict(self):
        session = Session(**self.default_session)

        info = {
            "hostname": "asdf",
            "port": 1234,
            "cert_file": self.default_session["cert_file"],
            "key_file": self.default_session["key_file"],
            "obj_uris": [NAMESPACE.NIC_CONTACT, NAMESPACE.NIC_DOMAIN, NAMESPACE.NIC_NSSET, NAMESPACE.NIC_KEYSET],
            "username": "asdf",
            "password": "***",
            "verify": True,
            "alias": "asdf",
            "reconnects": 1,
        }
        self.assertEqual(session.info_dict(), info)
