from typing import cast
from unittest import TestCase
from unittest.mock import Mock, patch

from testfixtures import LogCapture, OutputCapture

from eppic.exceptions import CommandParseError, EppicError
from eppic.input.input_handler import (
    CliArgsInputHandler,
    FileInputHandler,
    JsonInputHandler,
    PromptToolkitInputHandler,
    YamlInputHandler,
)
from eppic.input.input_processor import DictInputProcessor, StringInputProcessor
from eppic.output import JsonOutputHandler, OutputHandler, YamlOutputHandler
from eppic.settings import InputFormat, OutputFormat, PrintRawXml, Session

from .utils import DummyEppCommand, DummyResponse, GetAppMixin


class TestApplication(GetAppMixin, TestCase):

    def test_init(self):
        app = self.get_app()
        self.assertGreater(len(app.argument_registry._types), 0)
        self.assertGreater(len(app.command_registry._commands), 0)
        self.assertEppHandlerCreated()

    def test_print_warning(self):
        app = self.get_app()
        with OutputCapture(separate=True) as output:
            app.print_warning("Gazpacho!")

        output.compare(stderr="Warning: Gazpacho!\n")

    def test_prompt_filled(self):
        self.assertEqual(self.get_app()._prompt(), "foo@asdf > ")

    def test_prompt_empty(self):
        self.assertEqual(self.get_app(session=None)._prompt(), "> ")

    def test_run_command(self):
        with patch("eppic.tests.utils.DummyCommand._run") as p:
            app = self.get_app()
            app.run_command("foobar asdf")
        p.assert_called_once_with(app, DummyEppCommand("asdf"))

    def test_run(self):
        with patch("eppic.tests.utils.DummyCommand._run") as p:
            app = self.get_app(command="foobar asdf")
            app.run()
        p.assert_called_once_with(app, DummyEppCommand("asdf"))
        self.assertEppHandlerExited()

    def test_run_error_logging(self):
        with patch("eppic.tests.utils.DummyCommand._run") as p:
            file = self.get_temp_file("foobar asdf\nfoobar asdf")
            app = self.get_app(exit_on_error=False, input_file=file)
            p.side_effect = [EppicError("foobar error one"), EppicError("foobar error two")]
            with LogCapture() as log_capture:
                self.assertRaises(EppicError, app.run)
                self.assertEqual(log_capture.actual()[-1], ("eppic.app", "ERROR", "foobar error two"))
                self.assertEppHandlerExited()

    def test_run_error_exit_on_error(self):
        with patch("eppic.tests.utils.DummyCommand._run") as p:
            file = self.get_temp_file("foobar asdf\nfoobar asdf")
            app = self.get_app(exit_on_error=True, input_file=file)
            p.side_effect = [EppicError("foobar error one"), EppicError("foobar error two")]
            with LogCapture() as log_capture:
                self.assertRaises(EppicError, app.run)
                self.assertEqual(log_capture.actual()[-1], ("eppic.app", "ERROR", "foobar error one"))
                self.assertEppHandlerExited()

    def test_run_parsing_fail(self):
        with LogCapture() as log:
            self.assertRaises(CommandParseError, self.get_app(command="foobar asdf asdf").run)
        log.check_present(("eppic.app", "ERROR", "Too many positional arguments received."))
        self.epp_handler.send_request.assert_not_called()
        self.assertEppHandlerExited()

    def test_pre_hooks(self):
        self.epp_handler.send_request.return_value = DummyResponse()
        app = self.get_app(foobar_pre_hooks=[m := Mock()])
        with patch("eppic.tests.utils.DummyCommand._run") as p:
            app.run_command("foobar asdf")
        p.assert_called_once_with(app, DummyEppCommand("asdf"))
        m.assert_called_once()
        self.assertEqual(m.call_args.args[0], app)

    def test_post_hooks(self):
        self.epp_handler.send_request.return_value = DummyResponse()
        app = self.get_app(foobar_post_hooks=[m := Mock()])
        with patch("eppic.tests.utils.DummyCommand._run") as p:
            app.run_command("foobar asdf")
        p.assert_called_once_with(app, DummyEppCommand("asdf"))
        m.assert_called_once()
        self.assertEqual(m.call_args.args[0], app)

    def test_add_session(self):
        app = self.get_app()
        session = Session(hostname="hello", username="world")
        sess_id = app.add_session(session)  # session id is an index of the session in the array
        self.assertEqual(len(app.settings.sessions), sess_id + 1)
        self.assertEqual(session, app.settings.get_session(sess_id))

    def test_print_raw_xml_off(self):
        with patch("eppic.utils.print_formatted_text") as out:
            with patch("eppic.output.print_formatted_text"):  # we don't care about the command response
                self.get_app(print_raw_xml=PrintRawXml.DISABLED, syntax_highlighting=True).print_xml(b"foobar")
                out.assert_not_called()

    def test_print_raw_xml_on(self):
        cases = [
            (PrintRawXml.RAW, "<command></command>"),
            (PrintRawXml.FORMATTED, "<command/>"),
        ]
        for format, command in cases:
            with self.subTest(format=format):
                with self.subTest(syntax_highlighting=True):
                    with patch("eppic.utils.print_formatted_text") as formatted_output:
                        with patch("eppic.output.print_formatted_text"):  # we don't care about the command response
                            self.get_app(print_raw_xml=format, syntax_highlighting=True
                                         ).print_xml(b"<command></command>")
                    formatted_output.assert_called()

                with self.subTest(syntax_highlighting=False):
                    with patch("eppic.utils.print_formatted_text") as formatted_output:
                        with OutputCapture() as output:
                            self.get_app(print_raw_xml=format, syntax_highlighting=False
                                         ).print_xml(b"<command></command>")
                    formatted_output.assert_not_called()
                    output.compare(command)


class TestGetOutputHandler(GetAppMixin, TestCase):

    def get_output_handler(self, *args, **kwargs):
        return self.get_app().get_output_handler(*args, **kwargs)

    def test_choose_type(self):
        cases = [
            (OutputFormat.JSON, JsonOutputHandler),
            (OutputFormat.YAML, YamlOutputHandler),
            (OutputFormat.DEFAULT, OutputHandler),
        ]
        for format, expected in cases:
            with self.subTest(format=format):
                self.assertIsInstance(self.get_output_handler(format), expected)


class TestGetInputHandler(GetAppMixin, TestCase):

    def get_input_handler(self, command=None, command_registry=None, **kwargs):
        settings = self.get_settings(**kwargs)
        command_registry = command_registry if command_registry is not None else Mock()
        return self.get_app().get_input_handler(settings, command, command_registry)

    def test_command(self):
        self.assertIsInstance(
            self.get_input_handler(command="asdf"),
            CliArgsInputHandler
        )

    def test_prompt_toolkit(self):
        ih = self.get_input_handler()
        self.assertIsInstance(ih, PromptToolkitInputHandler)
        self.assertIsNotNone(cast(PromptToolkitInputHandler, ih).prompt.completer)
        self.assertIsNotNone(cast(PromptToolkitInputHandler, ih).prompt.message)

    def test_prompt_toolkit_no_completion(self):
        ih = self.get_input_handler(autocomplete=False)
        self.assertIsInstance(ih, PromptToolkitInputHandler)
        self.assertIsNone(cast(PromptToolkitInputHandler, ih).prompt.completer)

    def test_prompt_toolkit_no_prompt(self):
        with patch("sys.stdin"):
            ih = self.get_app().get_input_handler(self.get_settings(enable_prompt=False), None, Mock())
        self.assertIsInstance(ih, PromptToolkitInputHandler)
        self.assertIsNone(cast(PromptToolkitInputHandler, ih).prompt.message)

    def test_default(self):
        file = self.get_temp_file("asdf")
        self.assertIsInstance(
            self.get_input_handler(input_file=file),
            FileInputHandler
        )

    def test_json(self):
        file = self.get_temp_file("{'asdf': 'asdf'}")
        self.assertIsInstance(
            self.get_input_handler(input_format=InputFormat.JSON, input_file=file),
            JsonInputHandler
        )

    def test_yaml(self):
        file = self.get_temp_file("asdf: asdf")
        self.assertIsInstance(
            self.get_input_handler(input_format=InputFormat.YAML, input_file=file),
            YamlInputHandler
        )


class TestGetInputProcessor(GetAppMixin, TestCase):

    def get_input_processor(self, *args):
        return self.get_app().get_input_processor(*args)

    def test_input_processors(self):
        for dict_type in [InputFormat.JSON, InputFormat.YAML]:
            with self.subTest(type=dict_type):
                self.assertIsInstance(self.get_input_processor(dict_type), DictInputProcessor)

    def test_string(self):
        for dict_type in [InputFormat.DEFAULT]:
            with self.subTest(type=dict_type):
                self.assertIsInstance(self.get_input_processor(dict_type), StringInputProcessor)
