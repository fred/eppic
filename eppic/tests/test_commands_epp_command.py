from typing import cast
from unittest import TestCase
from unittest.mock import call, sentinel

from click.testing import CliRunner
from epplib.commands import Logout

from .utils import TestClient, TestSettings, get_app


class LogoutCommandTest(TestCase):
    def test_cli(self):
        app = get_app()
        mock_client = cast(TestClient, app.epp_handler._client).mock

        app.run_command('logout')

        self.assertEqual(mock_client.mock_calls, [call(Logout(), tr_id=None), call.close()])
        self.assertEqual(app.epp_handler.session, sentinel.session)
        self.assertFalse(app.epp_handler.logged_in)

    def test_cli_cltrid(self):
        app = get_app()
        mock_client = cast(TestClient, app.epp_handler._client).mock

        app.run_command('logout --cltrid future-echo')

        self.assertEqual(mock_client.mock_calls, [call(Logout(), tr_id='future-echo'), call.close()])
        self.assertEqual(app.epp_handler.session, sentinel.session)
        self.assertFalse(app.epp_handler.logged_in)

    def test_confirm(self):
        data = [
            ("n", []),
            ("y", [call(Logout(), tr_id=None), call.close()]),
        ]
        for answer, calls in data:
            with self.subTest(answer=answer):
                app = get_app(TestSettings(interactive={'command_confirmation': True}))
                mock_client = cast(TestClient, app.epp_handler._client).mock
                with CliRunner().isolation(input=answer):
                    app.run_command('logout')

                    self.assertEqual(mock_client.mock_calls, calls)
