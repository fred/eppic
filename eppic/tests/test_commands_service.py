"""Tests for Service Commands."""
from typing import Any, Dict, cast
from unittest import TestCase
from unittest.mock import Mock, call, patch

from testfixtures import OutputCapture

from eppic.commands.service_requests import (
    ServiceChangeSettings,
    ServiceExit,
    ServiceListSessions,
    ServiceLogin,
)
from eppic.epp import EppHandler
from eppic.output import JsonOutputHandler
from eppic.settings import OutputFormat, Session

from .utils import DEFAULT_SESSIONS, GetAppMixin, get_app


class TestServiceExit(GetAppMixin, TestCase):

    @patch("eppic.commands.service_requests.sys")
    def test_exit(self, sys):
        command = ServiceExit()
        command.handle(self.get_app())
        sys.exit.assert_called_once_with(0)


class TestServiceLogin(GetAppMixin, TestCase):

    def test_service_login_already_logged(self):
        command = ServiceLogin("asdff")
        command.handle(self.get_app())

        self.assertEqual(self.epp_handler.mock_calls, [call.login(DEFAULT_SESSIONS[1])])

    def test_service_login_not_logged(self):
        command = ServiceLogin("asdff")
        command.handle(self.get_app(session=None))

        self.assertEqual(self.epp_handler.mock_calls, [call.login(DEFAULT_SESSIONS[1])])

    def test_service_login_incorrect_session(self):
        self.assertRaises(ValueError, ServiceLogin("hello").handle, self.get_app())

    def test_service_login_no_sessions_exist(self):
        self.assertRaises(ValueError, ServiceLogin().handle, self.get_app(sessions=[], session=None))

    def test_service_login_numeric(self):
        # Test login with a numeric session argument is deprecated
        command = ServiceLogin(session=0)
        with OutputCapture() as output:
            command.handle(self.get_app(session=None))

        self.assertEqual(self.epp_handler.mock_calls, [call.login(DEFAULT_SESSIONS[0])])
        output.compare("Warning: Numeric values for --session argument are deprecated. Use aliases instead.")

    def test_service_login_session_id(self):
        # Test login with a deprecated session_id argument
        command = ServiceLogin(session_id="asdff")
        with OutputCapture() as output:
            command.handle(self.get_app(session=None))

        self.assertEqual(self.epp_handler.mock_calls, [call.login(DEFAULT_SESSIONS[1])])
        output.compare("Warning: Argument --session-id is deprecated. Use --session instead.")


class TestServiceNewSession(TestCase):
    def setUp(self):
        self.epp_mock = Mock(EppHandler, autospec=True)

    def test_service_modify_some(self):
        app = get_app()
        app.epp_handler = self.epp_mock
        app.add_session(DEFAULT_SESSIONS[1])

        app.run_command('new-session --base-session=asdff --username asdfgh --login=no')

        new_session = Session(**dict(DEFAULT_SESSIONS[1], username="asdfgh", alias="asdfgh"))
        self.assertEqual(app.settings.sessions, [DEFAULT_SESSIONS[1], new_session])
        self.assertEqual(self.epp_mock.mock_calls, [])

    def test_service_modify_none(self):
        app = get_app()
        app.epp_handler = self.epp_mock
        app.add_session(DEFAULT_SESSIONS[1])

        app.run_command('new-session --base-session=asdff --login=no')

        self.assertEqual(app.settings.sessions, [DEFAULT_SESSIONS[1], DEFAULT_SESSIONS[1]])
        self.assertEqual(self.epp_mock.mock_calls, [])

    def test_service_modify_base_session_numeric(self):
        app = get_app()
        app.epp_handler = self.epp_mock
        app.add_session(DEFAULT_SESSIONS[0])

        with OutputCapture() as output:
            app.run_command('new-session --base-session=0 --login=no')

        output.compare("Warning: Numeric values for --base-session argument are deprecated. Use aliases instead.")
        self.assertEqual(app.settings.sessions, [DEFAULT_SESSIONS[0], DEFAULT_SESSIONS[0]])
        self.assertEqual(self.epp_mock.mock_calls, [])

    def test_service_modify_base_session_id(self):
        app = get_app()
        app.epp_handler = self.epp_mock
        app.add_session(DEFAULT_SESSIONS[1])

        with OutputCapture() as output:
            app.run_command('new-session --base-session-id=asdff --login=no')

        output.compare("Warning: Argument --base-session-id is deprecated. Use --base-session instead.")
        self.assertEqual(app.settings.sessions, [DEFAULT_SESSIONS[1], DEFAULT_SESSIONS[1]])
        self.assertEqual(self.epp_mock.mock_calls, [])

    def test_service_all_new(self):
        app = get_app()
        app.epp_handler = self.epp_mock

        app.run_command('new-session --hostname=asdfgh --port=123 --login=no')

        new_session = Session(hostname="asdfgh", port=123)
        self.assertEqual(app.settings.sessions, [new_session])
        self.assertEqual(self.epp_mock.mock_calls, [])

    def test_service_all_new_all_params(self):
        app = get_app()
        app.epp_handler = self.epp_mock
        file = __file__

        app.run_command('new-session --username=kryten --password=Gazpacho! --hostname=asdfgh --port=123 '
                        f'--cert-file={file} --key-file={file} --obj-uris-1=a --obj-uris-2=b --verify=yes '
                        '--alias=kryten --reconnects=3 --login=no')

        new_session = Session(username="kryten", password="Gazpacho!", hostname="asdfgh", port="123",
                              cert_file=file, key_file=file, obj_uris=["a", "b"], verify=True,
                              alias="kryten", reconnects=3, login=False)
        self.assertEqual(app.settings.sessions, [new_session])
        self.assertEqual(self.epp_mock.mock_calls, [])

    def test_service_new_session_invalid_id(self):
        app = get_app()

        with self.assertRaises(ValueError):
            app.run_command('new-session --base-session=hello --hostname=asdfgh --port=123')

    def test_login_no(self):
        app = get_app()
        app.epp_handler = self.epp_mock

        app.run_command('new-session --username=kryten --login=no')

        self.assertEqual(self.epp_mock.mock_calls, [])

    def test_login_yes(self):
        app = get_app()
        app.epp_handler = self.epp_mock

        app.run_command('new-session --username=kryten --login=yes')

        self.assertEqual(self.epp_mock.mock_calls, [call.login(Session(username='kryten', alias='kryten'))])

    def test_login_default(self):
        # Test new-session performs login by default and prints warning.
        app = get_app()
        app.epp_handler = self.epp_mock

        with OutputCapture() as output:
            app.run_command('new-session --username=kryten')

        output.compare("Warning: --login default will change to 'no'.")
        self.assertEqual(self.epp_mock.mock_calls, [call.login(Session(username='kryten', alias='kryten'))])


class TestServiceChangeSettings(GetAppMixin, TestCase):

    def test_service_change_settings(self):
        tests = [  # command input, test
            ({"output_format": OutputFormat.JSON}, lambda a: isinstance(a.output_handler, JsonOutputHandler)),
            ({"autocomplete": False}, lambda a: a.input_handler.prompt.completer is None),
            ({"syntax_highlighting": False}, lambda a: a.input_handler.prompt.lexer is None),
            ({"command_confirmation": True}, lambda a: a.settings.interactive.command_confirmation is True),
            ({"print_raw_xml": True}, lambda a: a.settings.print_raw_xml is True),
        ]
        for kwargs, test in tests:
            kwargs = cast(Dict[str, Any], kwargs)
            with self.subTest(**kwargs):
                app = self.get_app(autocomplete=True)
                self.assertFalse(test(app))
                ServiceChangeSettings(**kwargs).handle(app)
                self.assertTrue(test(app))


class TestServiceListSessions(GetAppMixin, TestCase):

    def test_service_change_settings(self):
        the_files = DEFAULT_SESSIONS[0].key_file
        app = self.get_app(autocomplete=True, syntax_highlighting=False)
        with OutputCapture() as output:
            ServiceListSessions().handle(app)

        self.assertEqual(output.captured.strip(), f"""data:
- alias: foo
  cert_file: {the_files}
  hostname: asdf
  key_file: {the_files}
  obj_uris:
  - asdf
  password: '***'
  port: 123
  reconnects: 1
  username: foo
  verify: true
- alias: asdff
  cert_file: {the_files}
  hostname: asdf
  key_file: {the_files}
  obj_uris:
  - asdf
  password: '***'
  port: 123
  reconnects: 1
  username: asdff
  verify: true""".strip())
