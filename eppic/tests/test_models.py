from dataclasses import Field, field
from typing import Any, Optional, Type, cast
from unittest import TestCase
from unittest.mock import sentinel

from eppic.models import ArgumentField, ParsedCommand


class ArgumentFieldTest(TestCase):
    def test_from_dataclass_field(self):
        def make_field(name: str, type: Type, **kwargs: Any) -> Field:
            f = field(**kwargs)
            f.name = name
            f.type = type
            return cast(Field, f)

        data = (
            # field, result
            (make_field('answer', int), ArgumentField('answer', int, True)),
            (make_field('answer', Optional[int]), ArgumentField('answer', Optional[int], False)),  # type: ignore
            (make_field('answer', int, default=42), ArgumentField('answer', int, False)),
            (make_field('answer', list, default_factory=list), ArgumentField('answer', list, False)),
        )
        for f, result in data:
            with self.subTest(field=f):
                self.assertEqual(ArgumentField.from_dataclass_field(f), result)


class ParsedCommandTest(TestCase):
    def test_add_error(self):
        cmd = ParsedCommand(sentinel.raw, sentinel.name, sentinel.raw_args)
        cmd.add_error(sentinel.error)
        self.assertEqual(cmd.error, sentinel.error)

    def test_add_error_existing(self):
        cmd = ParsedCommand(sentinel.raw, sentinel.name, sentinel.raw_args, error=sentinel.old_error)
        cmd.add_error(sentinel.error)
        self.assertEqual(cmd.error, sentinel.old_error)
