"""Test InputProcessor class."""
from typing import Any, Dict, Iterable, Iterator, Tuple
from unittest import TestCase
from unittest.mock import sentinel

from eppic.input.input_processor import DictInputProcessor, InputProcessor, StringInputProcessor
from eppic.models import ParsedArgument, ParsedCommand


class TestInputProcessor(InputProcessor):
    """Simple input processor for tests."""

    def _get_command_name_and_raw_args(self, raw_in: Any) -> Tuple[str, Any]:
        return "foobar", sentinel.args

    def _get_args_generator(self, args: Any) -> Iterator[ParsedArgument]:
        yield ParsedArgument(None, "args")
        yield ParsedArgument(["args"], "parsed")


class InputProcessorTest(TestCase):
    def setUp(self) -> None:
        self.processor = TestInputProcessor()

    def test_call(self):
        result = self.processor(sentinel.cmd)

        self.assertEqual(result, ParsedCommand(sentinel.cmd, name="foobar",
                         raw_args=(ParsedArgument(None, "args"), ParsedArgument(["args"], "parsed"))))


class StringInputProcessorTest(TestCase):
    def test_processor(self):
        processor = StringInputProcessor()
        data = (
            ("hello", ParsedCommand("hello", name="hello", raw_args=())),
            ("hello ", ParsedCommand("hello ", name="hello", raw_args=())),
            ("hello foo", ParsedCommand("hello foo", name="hello", raw_args=(ParsedArgument(None, "foo"), ))),
            ("hello foo bar", ParsedCommand("hello foo bar", name="hello",
                                            raw_args=(ParsedArgument(None, "foo"), ParsedArgument(None, "bar")))),
            ('hello "foo bar"', ParsedCommand('hello "foo bar"', name="hello",
                                              raw_args=(ParsedArgument(None, "foo bar"), ))),
            ("hello --foo=42", ParsedCommand("hello --foo=42", name="hello",
                                             raw_args=(ParsedArgument(["foo"], "42"), ))),
            ("hello --foo=42,43", ParsedCommand("hello --foo=42,43", name="hello",
                                                raw_args=(ParsedArgument(["foo"], "42,43"), ))),
            ('hello --foo="42"', ParsedCommand('hello --foo="42"', name="hello",
                                               raw_args=(ParsedArgument(["foo"], "42"), ))),
            ("hello '--foo=4 2'", ParsedCommand("hello '--foo=4 2'", name="hello",
                                                raw_args=(ParsedArgument(["foo"], "4 2"), ))),
            ("hello --foo='4 2'", ParsedCommand("hello --foo='4 2'", name="hello",
                                                raw_args=(ParsedArgument(["foo"], "4 2"), ))),
            ("hello --foo.bar=42", ParsedCommand("hello --foo.bar=42", name="hello",
                                                 raw_args=(ParsedArgument(["foo", "bar"], "42"), ))),
            ("hello --foo.bar 42", ParsedCommand("hello --foo.bar 42", name="hello",
                                                 raw_args=(ParsedArgument(["foo", "bar"], "42"), ))),
            ("hello '--foo.bar=42'", ParsedCommand("hello '--foo.bar=42'", name="hello",
                                                   raw_args=(ParsedArgument(["foo", "bar"], "42"), ))),
            ("hello '--foo.bar' '42'", ParsedCommand("hello '--foo.bar' '42'", name="hello",
                                                     raw_args=(ParsedArgument(["foo", "bar"], "42"), ))),
            ("hello '--foo.bar=--baz=asdf'", ParsedCommand("hello '--foo.bar=--baz=asdf'", name="hello",
                                                           raw_args=(ParsedArgument(["foo", "bar"], "--baz=asdf"), ))),
            ("hello '--foo.bar' '--baz=asdf'", ParsedCommand("hello '--foo.bar' '--baz=asdf'", name="hello",
                                                             raw_args=(ParsedArgument(["foo", "bar"], "--baz=asdf"),))),
            ("hello --foo.bar=--baz=asdf", ParsedCommand("hello --foo.bar=--baz=asdf", name="hello",
                                                         raw_args=(ParsedArgument(["foo", "bar"], "--baz=asdf"), ))),
            ("hello --foo.bar --baz=asdf", ParsedCommand("hello --foo.bar --baz=asdf", name="hello",
                                                         raw_args=(ParsedArgument(["foo", "bar"], "--baz=asdf"), ))),
            ("hello --foo.bar --baz=a:%sdf", ParsedCommand("hello --foo.bar --baz=a:%sdf", name="hello",
                                                           raw_args=(ParsedArgument(["foo", "bar"], "--baz=a:%sdf"),))),
            ("hello --foo=42 --bar=196",
             ParsedCommand("hello --foo=42 --bar=196", name="hello",
                           raw_args=(ParsedArgument(["foo"], "42"), ParsedArgument(["bar"], "196")))),
            ("hello bar --foo=42",
             ParsedCommand("hello bar --foo=42", name="hello",
                           raw_args=(ParsedArgument(None, "bar"), ParsedArgument(["foo"], "42")))),
            ("hello --foo=42 bar",
             ParsedCommand("hello --foo=42 bar", name="hello",
                           raw_args=(ParsedArgument(["foo"], "42"), ParsedArgument(None, "bar")))),
            # Unfinished argument
            ("hello --foo", ParsedCommand("hello --foo", name="hello", raw_args=(ParsedArgument(["foo"], None), ))),
            # Empty value
            ("hello --foo=", ParsedCommand("hello --foo=", name="hello", raw_args=(ParsedArgument(["foo"], ""), ))),
            ("hello --foo.bar a:%!sdf", ParsedCommand(
                "hello --foo.bar a:%!sdf", name="hello",
                raw_args=(ParsedArgument(["foo", "bar"], "a:%!sdf"),))),
            ("hello --foo.bar []{}()!@#$%^&*_+=-/.,;:~`", ParsedCommand(
                "hello --foo.bar []{}()!@#$%^&*_+=-/.,;:~`", name="hello",
                raw_args=(ParsedArgument(["foo", "bar"], "[]{}()!@#$%^&*_+=-/.,;:~`"),))),
            ("hello --foo.bar aáéíóůúÁÉÍÓŮÚ", ParsedCommand(
                "hello --foo.bar aáéíóůúÁÉÍÓŮÚ", name="hello",
                raw_args=(ParsedArgument(["foo", "bar"], "aáéíóůúÁÉÍÓŮÚ"),))),
        )
        for cmd, result in data:
            with self.subTest(cmd=cmd):
                self.assertEqual(processor(cmd), result)


class DictInputProcessorTest(TestCase):
    def test_processor(self):
        processor = DictInputProcessor()
        data: Iterable[Tuple[Dict[str, Any], ParsedCommand[Dict[str, Any]]]] = (
            ({"hello": {}}, ParsedCommand({"hello": {}}, name="hello", raw_args=())),
            ({"hello": {"foo": "42"}}, ParsedCommand({"hello": {"foo": "42"}}, name="hello",
                                                     raw_args=(ParsedArgument(["foo"], "42"), ))),
            ({"hello": {"foo": {"bar": "42"}}}, ParsedCommand({"hello": {"foo": {"bar": "42"}}}, name="hello",
                                                              raw_args=(ParsedArgument(["foo", "bar"], "42"), ))),
            ({"hello": {"foo": "42", "bar": "196"}},
             ParsedCommand({"hello": {"foo": "42", "bar": "196"}}, name="hello",
                           raw_args=(ParsedArgument(["foo"], "42"), ParsedArgument(["bar"], "196")))),
            ({"hello": {"foo": ["42", "196"]}},
             ParsedCommand({"hello": {"foo": ["42", "196"]}}, name="hello",
                           raw_args=(ParsedArgument(["foo_0"], "42"), ParsedArgument(["foo_1"], "196")))),
            ({"hello": {"foo": [{"bar": "42"}, {"baz": "196"}]}},
             ParsedCommand({"hello": {"foo": [{"bar": "42"}, {"baz": "196"}]}}, name="hello",
                           raw_args=(ParsedArgument(["foo_0", "bar"], "42"), ParsedArgument(["foo_1", "baz"], "196")))),
        )
        for cmd, result in data:
            with self.subTest(cmd=cmd):
                self.assertEqual(processor(cmd), result)

    def test_multiple_commands(self):
        processor = DictInputProcessor()
        with self.assertRaises(ValueError):
            processor({"hello": {"": "there"}, "hey": {"the": "re"}})
