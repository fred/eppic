import tempfile
from pathlib import Path
from unittest import TestCase
from unittest.mock import DEFAULT, Mock, call, patch, sentinel

from epplib.commands.base import Login, Logout
from epplib.exceptions import TransportError
from testfixtures import LogCapture

from eppic.epp import EppHandler, _noop
from eppic.exceptions import EppicError, ServerError
from eppic.settings import Session

from .utils import DummyEppCommand as DummyRequest


@patch("eppic.epp.SocketTransport")
class EppHandlerTest(TestCase):
    def setUp(self) -> None:
        self.temp_dir = tempfile.TemporaryDirectory()
        keys = Path(self.temp_dir.name)
        with open(keys / "key", "w") as f:
            f.write("foobar")
        with open(keys / "cert", "w") as f:
            f.write("foobar")

        self.session = Session(
            hostname="hello",
            port=123,
            cert_file=keys / "cert",
            key_file=keys / "key",
            username="kryten",
            password="Gazpacho!",
            obj_uris=["foo", "bar"],
        )

        patcher = patch("eppic.epp.Client")
        self.addCleanup(patcher.stop)
        self.client_mock = patcher.start()

    def tearDown(self) -> None:
        self.temp_dir.cleanup()

    def test_session_none(self, socket_transport):
        handler = EppHandler(None)
        self.assertIsNone(handler.session)

    def test_session(self, socket_transport):
        handler = EppHandler(None)
        handler._session = sentinel.session
        self.assertEqual(handler.session, sentinel.session)

    def test_set_session_logged_out(self, socket_transport):
        epp = EppHandler(None)

        epp.session = self.session

        self.assertEqual(epp._session, self.session)
        self.assertFalse(epp.logged_in)

    def test_set_session_logged_in(self, socket_transport):
        epp = EppHandler(None)
        epp._session = sentinel.session
        epp._client = self.client_mock.return_value

        epp.session = self.session

        self.assertEqual(epp._session, self.session)
        self.assertFalse(epp.logged_in)

    def test_login(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 1000
        epp = EppHandler(None, _noop)

        epp.login(self.session)

        self.assertEqual(epp.session, self.session)
        self.assertTrue(epp.logged_in)
        client_calls = [
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None)]
        self.assertEqual(self.client_mock.mock_calls, client_calls)

    def test_login_invalid_connection(self, socket_transport):
        epp = EppHandler(None, _noop)
        self.client_mock.return_value.send.side_effect = Exception('Gazpacho!')

        with LogCapture() as log:
            with self.assertRaisesRegex(Exception, 'Gazpacho!'):
                epp.login(self.session)

        log.check_present(("eppic.epp", "WARNING", "Logout failed."))
        self.assertEqual(epp.session, self.session)
        self.assertFalse(epp.logged_in)
        client_calls = [
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None),
            call().send(Logout(), tr_id=None), call().close()]
        self.assertEqual(self.client_mock.mock_calls, client_calls)

    def test_login_invalid_unknown_err(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 2345
        epp = EppHandler(None, _noop)

        with self.assertRaisesRegex(ServerError, "Login was unsuccessful."):
            epp.login(self.session)

        self.assertEqual(epp.session, self.session)
        self.assertFalse(epp.logged_in)
        client_calls = [
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None),
            call().send(Logout(), tr_id=None), call().close()]
        self.assertEqual(self.client_mock.mock_calls, client_calls)

    def test_login_invalid_auth(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 2501
        epp = EppHandler(None, _noop)

        with self.assertRaisesRegex(ServerError, "Invalid username or password."):
            epp.login(self.session)

        self.assertEqual(epp.session, self.session)
        self.assertFalse(epp.logged_in)
        client_calls = [
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None),
            call().send(Logout(), tr_id=None), call().close()]
        self.assertEqual(self.client_mock.mock_calls, client_calls)

    def test_login_no_hostname(self, socket_transport):
        epp = EppHandler(None)

        with self.assertRaisesRegex(ValueError, "Hostname not specified."):
            epp.login(Session())

        self.assertIsNone(epp.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [])

    def test_login_no_username(self, socket_transport):
        epp = EppHandler(None)

        with self.assertRaisesRegex(ValueError, "Username not specified."):
            epp.login(Session(hostname='example.org'))

        self.assertIsNone(epp.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [])

    def test_login_no_password(self, socket_transport):
        epp = EppHandler(None)

        with self.assertRaisesRegex(ValueError, "Password not specified."):
            epp.login(Session(hostname='example.org', username='kryten'))

        self.assertIsNone(epp.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [])

    def test_logout(self, socket_transport):
        epp = EppHandler(None)
        epp._session = sentinel.session
        epp._client = self.client_mock.return_value

        epp.logout()

        self.assertEqual(epp.session, sentinel.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [call().send(Logout(), tr_id=None), call().close()])

    def test_logout_obj(self, socket_transport):
        # Test logout with a command object.
        epp = EppHandler(None)
        epp._session = sentinel.session
        epp._client = self.client_mock.return_value

        epp.logout(sentinel.logout)

        self.assertEqual(epp.session, sentinel.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [call().send(sentinel.logout, tr_id=None), call().close()])

    def test_logout_cltrid(self, socket_transport):
        epp = EppHandler(None)
        epp._session = sentinel.session
        epp._client = self.client_mock.return_value

        epp.logout(cltrid=sentinel.cltrid)

        self.assertEqual(epp.session, sentinel.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [call().send(Logout(), tr_id=sentinel.cltrid), call().close()])

    def test_logout_not_logged(self, socket_transport):
        epp = EppHandler(None)

        epp.logout()

        self.assertIsNone(epp.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [])

    def test_logout_error(self, socket_transport):
        self.client_mock.return_value.send.side_effect = Exception('Gazpacho!')
        epp = EppHandler(None)
        epp._session = sentinel.session
        epp._client = self.client_mock.return_value

        with LogCapture() as log:
            epp.logout()  # exception is not thrown.

        log.check_present(("eppic.epp", "WARNING", "Logout failed."))
        self.assertEqual(epp.session, sentinel.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [call().send(Logout(), tr_id=None), call().close()])

    def test_logout_close_error(self, socket_transport):
        self.client_mock.return_value.close.side_effect = Exception('Gazpacho!')
        epp = EppHandler(None)
        epp._session = sentinel.session
        epp._client = self.client_mock.return_value

        with LogCapture() as log:
            epp.logout()  # exception is not thrown.

        log.check_present(("eppic.epp", "WARNING", "Connection failed on close."))
        self.assertEqual(epp.session, sentinel.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [call().send(Logout(), tr_id=None), call().close()])

    def test_send_request_set_cltrid(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 1000
        epp = EppHandler(self.session, _noop)

        epp.send_request(DummyRequest("foobar"), "baz")

        client_calls = [
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None),
            call().send(DummyRequest("foobar"), tr_id="baz")]
        self.assertEqual(self.client_mock.mock_calls, client_calls)

    def test_send_request_negative_retries(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 1000
        epp = EppHandler(self.session)

        self.assertRaises(EppicError, epp.send_request, sentinel.request, retries=-1)

    def test_send_request_error(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 1000
        print_xml_mock = Mock()
        epp = EppHandler(self.session, print_xml_mock)
        # reset after the login

        self.client_mock.return_value.send.side_effect = TransportError("foo")
        with LogCapture() as log:
            self.assertRaises(ServerError, epp.send_request, sentinel.request, retries=0)

        self.assertEqual(epp.session, self.session)
        self.assertFalse(epp.logged_in)
        client_calls = [
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None),
            call().send(sentinel.request, tr_id=None), call().send(Logout(), tr_id=None), call().close()]
        self.assertEqual(self.client_mock.mock_calls, client_calls)
        log.check_present(("eppic.epp", "WARNING", "Logout failed."))
        print_xml_mock.assert_called_with(self.client_mock.return_value.last_raw_command)

    def test_send_request_permanent_error_retry(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 1000
        print_xml_mock = Mock()
        epp = EppHandler(self.session, print_xml_mock)

        self.client_mock.return_value.send.side_effect = TransportError("foo")
        with LogCapture() as log:
            self.assertRaises(ServerError, epp.send_request, sentinel.request)

        self.assertEqual(epp.session, self.session)
        self.assertFalse(epp.logged_in)
        client_calls = [
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None),
            call().send(sentinel.request, tr_id=None), call().send(Logout(), tr_id=None), call().close(),
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None),
            call().send(Logout(), tr_id=None), call().close()]
        self.assertEqual(self.client_mock.mock_calls, client_calls)
        log.check_present(("eppic.epp", "WARNING", "Logout failed."))
        print_xml_mock.assert_called_with(self.client_mock.return_value.last_raw_command)

    def test_send_request_onetime_error_retry(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 1000
        print_xml_mock = Mock()
        epp = EppHandler(self.session, print_xml_mock)

        self.client_mock.return_value.send.side_effect = (
            # sentinel.request, logout, login, sentinel.request
            TransportError("foo"), TransportError("foo"), DEFAULT, DEFAULT
        )
        with LogCapture() as log:
            epp.send_request(sentinel.request)

        self.assertEqual(epp.session, self.session)
        assert epp._client is not None
        client_calls = [
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None),
            call().send(sentinel.request, tr_id=None), call().send(Logout(), tr_id=None), call().close(),
            call(socket_transport.return_value), call().connect(),
            call().send(Login("kryten", "Gazpacho!", self.session.obj_uris), tr_id=None),
            call().send(sentinel.request, tr_id=None)]
        self.assertEqual(self.client_mock.mock_calls, client_calls)
        log.check_present(("eppic.epp", "WARNING", "Logout failed."))
        self.assertListEqual(print_xml_mock.mock_calls, [
            call(epp._client.last_raw_command),  # login
            call(epp._client.last_raw_response),
            call(epp._client.last_raw_command),  # failed sentinel.request
            call(epp._client.last_raw_command),  # login
            call(epp._client.last_raw_response),
            call(epp._client.last_raw_command),  # sentinel.request
            call(epp._client.last_raw_response),
        ])

    def test_send_request_no_session(self, socket_transport):
        epp = EppHandler(None, _noop)

        with self.assertRaisesRegex(EppicError, "Not logged in."):
            epp.send_request(sentinel.request)

        self.assertIsNone(epp.session)
        self.assertFalse(epp.logged_in)
        self.assertEqual(self.client_mock.mock_calls, [])

    def test_policy(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 1000
        epp = EppHandler(self.session)
        self.assertIsNotNone(epp.policy)

    def test_policy_logged_out(self, socket_transport):
        epp = EppHandler(None)
        self.assertIsNone(epp.policy)

    def test_print_raw_xml(self, socket_transport):
        self.client_mock.return_value.send.return_value.code = 1000
        epp = EppHandler(self.session, epp_handler_print_xml := Mock())
        epp.send_request(DummyRequest("foobar"), "baz")
        self.assertListEqual(epp_handler_print_xml.mock_calls, [
            call(epp._client.last_raw_command),  # type: ignore
            call(epp._client.last_raw_response),  # type: ignore
            call(epp._client.last_raw_command),  # type: ignore
            call(epp._client.last_raw_response),  # type: ignore
        ])
