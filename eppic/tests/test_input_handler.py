"""Test InputHandler classes."""
import tempfile
from dataclasses import dataclass
from json import JSONDecodeError
from pathlib import Path
from typing import ClassVar, Generic, List, Tuple, TypeVar, cast
from unittest import TestCase
from unittest.mock import Mock, patch

from prompt_toolkit import PromptSession
from prompt_toolkit.application import create_app_session
from prompt_toolkit.enums import EditingMode
from prompt_toolkit.input import create_pipe_input
from prompt_toolkit.output import DummyOutput
from yaml import YAMLError

from eppic.commands.argument_registry import ArgumentRegistry
from eppic.commands.arguments.argument import Argument
from eppic.commands.command_registry import CommandRegistry
from eppic.input.input_handler import (
    CliArgsInputHandler,
    FileInputHandler,
    JsonInputHandler,
    PromptToolkitInputHandler,
    YamlInputHandler,
)
from eppic.input.input_processor import StringInputProcessor
from eppic.input.prompt_toolkit_handlers import Completer, Validator
from eppic.settings import InteractiveSettings

from .utils import DummyCommand

T = TypeVar("T")


class InputHandlerTesterMixin(Generic[T]):

    input_handler: ClassVar
    error_class: ClassVar

    normal_commands: List[Tuple[str, List[T]]] = []
    error_commands: List[str] = []

    def input_handler_parse_input(self, contents: str) -> List[T]:
        tmp = tempfile.TemporaryDirectory()
        cast(TestCase, self).addCleanup(tmp.cleanup)
        file_path = Path(tmp.name) / "foobar"
        with open(file_path, "w") as f:
            f.write(contents)

        with open(file_path) as f:
            return list(self.input_handler(f))

    def test_normal_commands(self):
        for command, expected in self.normal_commands:
            with cast(TestCase, self).subTest(command=command):
                cast(TestCase, self).assertEqual(
                    self.input_handler_parse_input(command),
                    expected)

    def test_error_commands(self):
        for command in self.error_commands:
            with cast(TestCase, self).subTest(command=command):
                cast(TestCase, self).assertRaises(self.error_class, self.input_handler_parse_input, command)


class CliArgsInputHandlerTest(TestCase):
    """Test InputHandler from args."""

    def setUp(self) -> None:
        self.ig = CliArgsInputHandler("foo bar")

    def test_iterable(self) -> None:
        iter(self.ig)

    def test_one_output(self):
        self.assertEqual(list(self.ig), ["foo bar"])

    def test_none_output(self):
        self.ig = CliArgsInputHandler(None)
        self.assertEqual(list(self.ig), [])


class FileInputHandlerTest(InputHandlerTesterMixin[str], TestCase):
    """Test InputHandler from file."""

    input_handler = FileInputHandler
    normal_commands = [
        ("", []),
        ("asdf asdf\nfoo bar baz\nhello", ["asdf asdf", "foo bar baz", "hello"]),
        ("asdf asdf\n\nfoo bar baz\nhello", ["asdf asdf", "foo bar baz", "hello"]),
        ("asdf asdf\n\nfoo bar baz\nhello\n", ["asdf asdf", "foo bar baz", "hello"]),
        ("asdf asdf\n\nfoo bar baz\nhello\n\n\n\n\n", ["asdf asdf", "foo bar baz", "hello"]),
    ]
    error_commands = []  # there is no way to make this invalid


class JsonInputHandlerTest(InputHandlerTesterMixin[dict], TestCase):
    """Test InputHandler from json file."""

    input_handler = JsonInputHandler
    normal_commands = [
        ('{"asdf": "asdf"}', [{"asdf": "asdf"}]),
        ('{"asdf": "asdf"}\n{"foo":"bar"}', [{"asdf": "asdf"}, {"foo": "bar"}]),
        ('{"asdf": "asdf"}\n\n{"foo":"bar"}\n\n', [{"asdf": "asdf"}, {"foo": "bar"}]),
        ('', []),
    ]
    error_class = JSONDecodeError
    error_commands = [
        '{\n"asdf": "asdf"}\n{"foo":"bar"}',
    ]


class YamlInputHandlerTest(InputHandlerTesterMixin[dict], TestCase):
    """Test InputHandler from yaml file."""

    input_handler = YamlInputHandler
    normal_commands = [
        ('''---
asdf: asdf
...
---
foo: bar
...''', [{"asdf": "asdf"}, {"foo": "bar"}]),
        ('''---
asdf: asdf
...
''', [{"asdf": "asdf"}]),
        ('''
asdf: asdf
''', [{"asdf": "asdf"}]),
        ('', []),
    ]

    error_class = YAMLError
    error_commands = ['''---
asdf: asdf
foo: bar
.-ůsd''']


class SampleEmpty:
    """Sample empty command."""


@dataclass
class TestCommand:
    """Sample command with fields."""

    foo: str
    bar: int = 1


class PromptToolkitTest(TestCase):
    """Test InputHandler for prompt_toolkit."""

    commands = [
        ("foobar", TestCommand),
        ("exit", SampleEmpty),
    ]

    def setUp(self) -> None:
        self.input_handler = PromptToolkitInputHandler(Mock(), Mock())

    def prompt_toolkit(self) -> PromptToolkitInputHandler:
        cr = CommandRegistry()
        ar = ArgumentRegistry()
        ar.register_argument_by_type(int, Argument)
        ar.register_argument_by_type(str, Argument)

        for command, cls in self.commands:
            cr.register_command(DummyCommand(command, cls, ar))

        ip = StringInputProcessor()
        completer = Completer(command_registry=cr, input_processor=ip)
        validator = Validator(command_registry=cr, input_processor=ip)
        return PromptToolkitInputHandler(completer, validator)

    def test_init(self):
        handler = self.prompt_toolkit()

        self.assertIsInstance(handler.prompt, PromptSession)
        self.assertEqual(handler.prompt.message, '> ')
        self.assertIsNone(handler.prompt.validator)
        self.assertIsNone(handler.prompt.lexer)
        self.assertIsNone(handler.prompt.completer)
        self.assertEqual(handler.prompt.app.editing_mode, EditingMode.EMACS)

    def test_set_settings(self):
        handler = self.prompt_toolkit()
        settings = InteractiveSettings(autocomplete=True, enable_prompt=True, syntax_highlighting=True,
                                       online_validation=True, vi_mode=True)

        handler.set_settings(settings)

        self.assertIsInstance(handler.prompt, PromptSession)
        self.assertEqual(handler.prompt.message, '> ')
        self.assertIsNotNone(handler.prompt.validator)
        self.assertIsNotNone(handler.prompt.lexer)
        self.assertIsNotNone(handler.prompt.completer)
        self.assertEqual(handler.prompt.app.editing_mode, EditingMode.VI)

    def test_get_commands(self) -> None:
        with create_pipe_input() as inp:
            with create_app_session(input=inp, output=DummyOutput()):
                ih = iter(self.prompt_toolkit())
                inp.send_text("foobar --foo=12\n")
                inp.send_text("\n")
                inp.send_text("exit\n")
                inp.close()

                self.assertEqual(next(ih), "foobar --foo=12")
                self.assertEqual(next(ih), "exit")

    def test_close_eof(self) -> None:
        with create_pipe_input() as inp:
            with create_app_session(input=inp, output=DummyOutput()):
                ih = iter(self.prompt_toolkit())
                inp.send_text("foobar --foo=12\n")
                inp.close()

                self.assertEqual(next(ih), "foobar --foo=12")
                self.assertRaises(StopIteration, next, ih)

    @patch("eppic.input.input_handler.PromptSession")
    def test_ctrl_c(self, prompt_session: Mock) -> None:
        prompt = prompt_session.return_value.prompt
        prompt.side_effect = [KeyboardInterrupt(), EOFError()]
        ih = iter(self.prompt_toolkit())
        self.assertRaises(StopIteration, next, ih)
        self.assertEqual(len(prompt.call_args), 2)

    def test_keybind_help(self):  # as far as I know, prompt toolkit does not support better tests.
        pt = self.prompt_toolkit()
        m = Mock()
        pt.help_keybind(m)
        buf = m.current_buffer
        buf.append_to_history.assert_called_once_with()
        buf.validate_and_handle.assert_called_once_with()

        buf.transform_current_line.assert_called_once()
        self.assertEqual(buf.transform_current_line.call_args[0][0]("foobar"), "help foobar")
