import tempfile
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import Dict, Iterable, List, Optional, Tuple, Type, cast
from unittest import TestCase
from unittest.mock import Mock, PropertyMock, patch, sentinel

import yaml
from testfixtures import LogCapture, OutputCapture

from eppic.app import Application
from eppic.commands.argument_registry import ArgumentRegistry
from eppic.commands.arguments import Argument, EnumArgument
from eppic.commands.command_registry import CommandRegistry
from eppic.commands.help import Help, HelpArgumentParser, HelpCommand, ServiceHelp
from eppic.commands.service_requests import ServiceRequest
from eppic.models import ArgumentLabels, ArgumentLabelSuggestion, ParsedArgument, ParsedCommand, Request
from eppic.settings import Settings

from .utils import DummyCommand


class DummyEnum(Enum):
    foo = "foo"
    bar = "bar"


@dataclass
class TestRequest:
    """Sample request with fields."""

    foo: str
    bar: DummyEnum


class HelpTestMixin:

    def setUp(self) -> None:
        ar = ArgumentRegistry()
        ar.register_argument_by_type(str, Argument)
        ar.register_argument_by_type(int, Argument)
        ar.register_argument_by_type(DummyEnum, EnumArgument)
        cmd = DummyCommand("foobar", cast(Type[Request], TestRequest), ar)
        help_cmd = DummyCommand("help", ServiceHelp, ar)
        self.cr = CommandRegistry()
        self.cr.register_command(cmd)
        self.cr.register_command(help_cmd)
        self.parser = HelpArgumentParser(self.cr)
        self.tmp = tempfile.TemporaryDirectory()
        cast(TestCase, self).addCleanup(self.tmp.cleanup)


class TestHelpArgumentParser(HelpTestMixin, TestCase):

    def getParsed(self, input: Iterable[Tuple[Optional[ArgumentLabels], Optional[str]]]) -> Dict:
        cmd = ParsedCommand(sentinel.raw, sentinel.name, [ParsedArgument(x, y) for x, y in input])
        cmd = self.parser.parse_arguments(cmd)
        self.assertIsNone(cmd.error)
        assert cmd.args is not None, "cmd.args is not None on processed command."
        return cmd.args

    def getParsedRaw(self, input: Iterable[Tuple[Optional[ArgumentLabels], Optional[str]]]) -> List[ParsedArgument]:
        cmd = ParsedCommand(sentinel.raw, sentinel.name, [ParsedArgument(x, y) for x, y in input])
        cmd = self.parser.parse_arguments(cmd)
        self.assertIsNone(cmd.error)
        assert cmd.args is not None, "cmd.args is not None on processed command."
        return list(cmd.raw_args)

    def test_nothing(self):
        self.assertEqual(self.getParsed([]), {"foobar": [(["foo"], None, True), (["bar"], ["foo", "bar"], True)],
                                              "help": [(["command, arguments"], None, True)]})

    def test_parse_just_command(self):
        self.assertEqual(self.getParsed([
            (None, "foobar")
        ]), {"foobar": [(["foo"], None, True), (["bar"], ["foo", "bar"], True)]})

    def test_parse_just_unknown_command(self):
        self.assertEqual(self.getParsed([
            (None, "foobaar")
        ]), {"foobar": [(["foo"], None, True), (["bar"], ["foo", "bar"], True)],
             "help": [(["command, arguments"], None, True)]})

    def test_parse_command_legal_args(self):
        self.assertEqual(self.getParsed([
            (None, "foobar"), (["foo"], "asdf")
        ]), {"foobar": [(["foo"], None, True), (["bar"], ["foo", "bar"], True)]})

    def test_parse_unknown_command_and_args(self):
        self.assertEqual(self.getParsed([
            (None, "foobaar"), (["foo"], "asdf")
        ]), {"foobar": [(["foo"], None, True), (["bar"], ["foo", "bar"], True)],
             "help": [(["command, arguments"], None, True)]})

    def test_parse_command_args_wrong_values(self):
        self.assertEqual(self.getParsed([
            (None, "foobar"), (["foo"], "asdf"), (["bar"], "asdf")
        ]), {"foobar": [(["foo"], None, True), (["bar"], ["foo", "bar"], True)]})

    def test_parse_command_args_wrong_arg(self):
        self.assertEqual(self.getParsed([
            (None, "foobar"), (["foo"], "asdf"), (["baar"], "asdf")
        ]), {"foobar": [(["foo"], None, True), (["bar"], ["foo", "bar"], True)]})

    def test_parse_command_args_hanging_last(self):
        self.assertEqual(self.getParsed([
            (None, "foobar"), (["foo"], "asdf"), (["bar"], None)
        ]), {"foobar": [(["foo"], None, True), (["bar"], ["foo", "bar"], True)]})

    def test_labels_none(self):
        args = self.getParsedRaw([])
        self.assertEqual(
            self.parser.get_available_labels(args), [(["command"], ["foobar", "help"], True)]
        )

    def test_labels_command(self):
        args = self.getParsedRaw([
            (None, "foobar")
        ])
        self.assertEqual(
            self.parser.get_available_labels(args), [(["foo"], None, True), (["bar"], ["foo", "bar"], True)]
        )

    def test_labels_command_wrong(self):
        args = self.getParsedRaw([
            (None, "foobaar")
        ])
        self.assertEqual(
            self.parser.get_available_labels(args), [(["command"], ["foobar", "help"], True)]
        )

    def test_parse_help(self):
        self.assertEqual(self.getParsed([
            (None, "help")
        ]), {"help": [(["command, arguments"], None, True)]})


class TestHelpCommand(HelpTestMixin, TestCase):

    def test_prepare_run(self):
        with OutputCapture() as out:
            cast(ServiceRequest, HelpCommand(self.cr)._prepare_request({"foobar": []})).handle(Mock())
            out.compare("* foobar:")

    @patch("eppic.app.EppHandler")
    def test_run_help_from_app(self, epp_handler):
        app = Application(Settings(), "")
        with OutputCapture() as out:
            app.run_command("help")
        self.assertIn("* help:", out.output.getvalue())


class TestServiceHelp(HelpTestMixin, TestCase):

    def make_tmp_file(self, name: str, content: str) -> Path:
        with open(Path(self.tmp.name) / name, "w") as f:
            f.write(content)

        return Path(self.tmp.name) / name

    def test_help_existing(self):
        file = self.make_tmp_file("asdf", """foobar:
  description: hello
  arguments:
    foo:
      description: asdf
    bar:
      description: ghjk
      values:
        foo: asdf
        bar: asdf""")
        with patch("eppic.commands.help.ServiceHelp.help_path", new_callable=PropertyMock) as m:
            m.return_value = file
            help = ServiceHelp()

            args = [ArgumentLabelSuggestion(["foo"], None, True),
                    ArgumentLabelSuggestion(["bar"], ["foo", "bar"], False)]
            self.assertEqual(help._help_command("foobar", args),
                             """* foobar: hello
    * foo: asdf
    * (bar): ghjk
        * foo: asdf
        * bar: asdf""")

    def test_help_not_existing(self):
        with patch("eppic.commands.help.ServiceHelp.help_path", new_callable=PropertyMock) as m:
            with LogCapture() as log:
                m.return_value = Path("/")
                help = ServiceHelp()
                args = [ArgumentLabelSuggestion(["foo"], None, True),
                        ArgumentLabelSuggestion(["bar"], ["foo", "bar"], False)]
                self.assertEqual(help._help_command("foobar", args),
                                 """* foobar:
    * foo:
    * (bar):
        * foo:
        * bar:""")
        log.check_present(("eppic.commands.help", "WARNING", "Help file not found."))


class TestHelp(TestCase):

    help = {
        "command": {
            "description": "cool command",
            "arguments": {
                "foo": {
                    "description": "foo arg",
                    "values": {
                        "hello": "world"
                    }
                },
                "bar": "shorthand",
            }
        },
        "another_command": {"arguments": {"foo": "foo"}},
        "strange_command": {"description": {"hello": "world"}}
    }

    def setUp(self) -> None:
        self.tmp_dir = tempfile.TemporaryDirectory()
        self.tmp = Path(self.tmp_dir.name) / "help.yaml"
        with self.tmp.open("w") as f:
            yaml.safe_dump(self.help, f)
        self.help_object = Help(self.tmp)

    def tearDown(self) -> None:
        self.tmp_dir.cleanup()

    def assertHelp(self, help_request: List[str], expected: str) -> None:
        generated_help = self.help_object.get_help(*help_request)
        self.assertEqual(generated_help, expected)

    def test_get_one_level(self):
        self.assertHelp(["command"], "* command: cool command")

    def test_get_two_levels(self):
        self.assertHelp(["command", "foo"], "    * foo: foo arg")

    def test_get_three_levels(self):
        self.assertHelp(["command", "foo", "hello"], "        * hello: world")

    def test_get_more_levels(self):
        self.assertHelp(["command", "foo", "hello", "wtf"], "        * hello: world")

    def test_arguments_shorthand(self):
        self.assertHelp(["command", "bar"], "    * bar: shorthand")

    def test_zero_levels(self):
        self.assertHelp([], "")

    def test_not_string_to_format(self):
        self.assertHelp(["strange_command"], "* strange_command: {'hello': 'world'}")
