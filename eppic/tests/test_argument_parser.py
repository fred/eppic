from typing import Any, List, Optional, Tuple, Type, Union, cast
from unittest import TestCase
from unittest.mock import sentinel

from eppic.commands.argument_parser import ArgumentParser, _unpack_required_type
from eppic.commands.argument_registry import ArgumentRegistry
from eppic.commands.arguments.argument import Argument
from eppic.models import ArgumentField, ArgumentLabelSuggestion, ParsedArgument, ParsedCommand


def _make_command(*_args: ParsedArgument, **kwargs: Any) -> ParsedCommand:
    return ParsedCommand(sentinel.raw, sentinel.name, _args, **kwargs)


def _make_commands(*_args: ParsedArgument, **kwargs: Any) -> Tuple[ParsedCommand, ParsedCommand]:
    return (_make_command(*_args), _make_command(*_args, **kwargs))


class TestArgumentParser(TestCase):
    """Test ArgumentParser class."""

    def setUp(self):
        self.registry = ArgumentRegistry()
        self.registry.register_argument_by_type(int, Argument)
        self.fields = (ArgumentField("foo", int, True), ArgumentField("bar", int, True),
                       ArgumentField("baz", cast(Type, Optional[int]), False))

    def test_unpacking_union(self):
        self.assertEqual(_unpack_required_type(cast(Type, Union[None, str, int])), Union[str, int])

    def test_parse_arguments(self):
        data = (
            # n_pos, cmd, result
            (0, _make_commands(ParsedArgument(["foo"], "42"), ParsedArgument(["bar"], "196"),
                               args={"foo": 42, "bar": 196, "baz": None})),
            (0, _make_commands(ParsedArgument(["foo"], "42"), ParsedArgument(["bar"], "196"),
                               ParsedArgument(["baz"], "42"), args={"foo": 42, "bar": 196, "baz": 42})),
            # Missing args
            (0, _make_commands(args={"baz": None}, error="Required argument foo didn't receive a value.")),
            (0, _make_commands(ParsedArgument(["foo"], "42"), args={"foo": 42, "baz": None},
                               error="Required argument bar didn't receive a value.")),
            # Extra args
            (0, _make_commands(ParsedArgument(["asdf"], "3"), args={"baz": None},
                               error="Argument ['asdf'] not found.")),
            (0, _make_commands(ParsedArgument(["foo"], "42"), ParsedArgument(["bar"], "196"),
                               ParsedArgument(["asdf"], "3"),
                               args={"foo": 42, "bar": 196, "baz": None}, error="Argument ['asdf'] not found.")),
            (3, _make_commands(ParsedArgument(["foo"], "42"), ParsedArgument(["bar"], "0"),
                               ParsedArgument(["baz"], "0"),
                               ParsedArgument(None, "3"), args=None,
                               error="Too many positional arguments received.")),

            # Restriction on positional
            (1, _make_commands(ParsedArgument(["foo"], "42"), ParsedArgument(["bar"], "196"),
                               args={"foo": 42, "bar": 196, "baz": None})),
            (1, _make_commands(ParsedArgument(None, "42"), ParsedArgument(["bar"], "196"),
                               args={"foo": 42, "bar": 196, "baz": None})),
            (60, _make_commands(ParsedArgument(None, "42"), ParsedArgument(["bar"], "196"),
                                args={"foo": 42, "bar": 196, "baz": None})),
            (1, _make_commands(ParsedArgument(["bar"], "196"), ParsedArgument(None, "42"),
                               args={"foo": 42, "bar": 196, "baz": None})),
            (0, _make_commands(ParsedArgument(None, "42"), ParsedArgument(["bar"], "196"),
                               error="Too many positional arguments received.")),
            (60, _make_commands(ParsedArgument(None, "42"), ParsedArgument(None, "196"),
                                args={"foo": 42, "bar": 196, "baz": None})),
        )
        for n_pos, (cmd, result) in data:
            with self.subTest(n_pos=n_pos, cmd=cmd, res=result):
                parser = ArgumentParser(self.fields, self.registry, n_pos)
                self.assertEqual(parser.parse_arguments(cmd), result)

    def test_get_available_labels(self):
        tests: List[Tuple[List[Union[List[str], None]], List[ArgumentLabelSuggestion]]] = [
            # in, out
            ([["foo"]], [ArgumentLabelSuggestion(["bar"], None, True), ArgumentLabelSuggestion(["baz"], None, False)]),
            ([["bar"]], [ArgumentLabelSuggestion(["foo"], None, True), ArgumentLabelSuggestion(["baz"], None, False)]),
            ([["bar"], ["baz"]], [ArgumentLabelSuggestion(["foo"], None, True)]),
            ([None, ["baz"]], [ArgumentLabelSuggestion(["bar"], None, True)]),
            ([["bar"], None], [ArgumentLabelSuggestion(["baz"], None, False)]),
            ([None], [ArgumentLabelSuggestion(["bar"], None, True),
                      ArgumentLabelSuggestion(["baz"], None, False)]),
        ]
        for in_labels, out in tests:
            with self.subTest(in_labels=in_labels):
                in_objs = [ParsedArgument(x, "42") for x in in_labels]
                parser = ArgumentParser(self.fields, self.registry, 1)
                self.assertEqual(parser.get_available_labels(in_objs), out)
