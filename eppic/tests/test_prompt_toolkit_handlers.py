from dataclasses import dataclass
from typing import List, Optional, Tuple, Type, cast
from unittest import TestCase
from unittest.mock import Mock

from prompt_toolkit.document import Document
from prompt_toolkit.validation import ValidationError

from eppic.commands.argument_registry import ArgumentRegistry
from eppic.commands.arguments.argument import Argument
from eppic.commands.command_registry import CommandRegistry
from eppic.input.input_processor import StringInputProcessor
from eppic.input.prompt_toolkit_handlers import Completer, Validator
from eppic.models import Request

from .utils import DummyCommand


class SampleEmpty:
    """Sample empty command."""


@dataclass
class TestCommand:
    """Sample command with fields."""

    foo: str
    bar: int = 1


class PromptToolkitCommandsMixin:
    """Setup stuff for prompt toolkit tests."""

    commands = [
        ("foobar", TestCommand),
        ("foobrbaz", SampleEmpty)
    ]

    def setUp(self) -> None:
        cr = CommandRegistry()
        ar = ArgumentRegistry()
        ar.register_argument_by_type(int, Argument)
        ar.register_argument_by_type(str, Argument)

        for command, command_class in self.commands:
            cr.register_command(DummyCommand(command, cast(Type[Request], command_class), ar))

        ip = StringInputProcessor()
        self.completer = Completer(command_registry=cr, input_processor=ip)
        self.validator = Validator(command_registry=cr, input_processor=ip)


class CompleterTest(PromptToolkitCommandsMixin, TestCase):

    def assertCompleter(
        self,
        input: str,
        expected_completions: Optional[List[Tuple[str, str]]] = None,
    ) -> None:
        document = Document(input)
        completions = self.completer.get_completions(document, None)
        self.assertEqual(
            [(compl.text, compl.display_text) for compl in completions],
            expected_completions or [], self.completer.last_word(document)
        )

    def test_command_partial(self):
        self.assertCompleter(
            "foob", [("foobar", "foobar"), ("foobrbaz", "foobrbaz")]
        )

    def test_command_known(self):
        self.assertCompleter(
            "foobar", [("foobar", "foobar")]
        )

    def test_command_known_and_space(self):
        self.assertCompleter(
            "foobar ", [("--foo=", "--foo="), ("--bar=", "(--bar=)")]
        )

    def test_command_known_and_more(self):
        self.assertCompleter(
            "foobar --", [("--foo=", "--foo="), ("--bar=", "(--bar=)")],
        )

    def test_arg_first_and_space(self):
        self.assertCompleter(
            "foobar --foo=asdf ", [("--bar=", "(--bar=)")]
        )

    def test_arg_name_in_progress(self):
        self.assertCompleter(
            "foobar --fo", [("--foo=", "--foo=")],
        )

    def test_value_in_progress(self):
        self.assertCompleter(
            "foobar --foo=asdf", []
        )

    def test_arg_unfinished(self):
        self.assertCompleter(
            "foobar --foo ", [],
        )

    def test_finished_command(self):
        self.assertCompleter(
            "foobar --foo foo --bar=1 ", []
        )

    def test_suggesting_values(self):
        compl = Completer(command_registry=Mock(), input_processor=Mock())
        self.assertEqual(
            [("--foo=", "--foo="), ("--foo=bar", "--foo=bar"), ("--foo=baz", "--foo=baz")],
            list(compl._single_argument_completions(["foo"], ["bar", "baz"], True))
        )

    def test_errors_on_suggestions(self):
        compl = Completer(command_registry=Mock(), input_processor=Mock())
        document = Document("unknowncommand --unknownargument=foobar")
        self.assertEqual(list(compl.get_completions(document, None)), [])  # exception not thrown


class ValidatorTest(PromptToolkitCommandsMixin, TestCase):

    def test_last_position(self):
        data = (
            # line, error
            ('', None),
            ('foo', None),  # Command ignored until finished.
            ('huhu', None),  # Command ignored until finished.
            ('foobar', None),  # Actual command
            ('foo ', 'Command not found'),
            ('huhu ', 'Command not found'),
            ('foobar --', "Required argument foo didn't receive a value."),
            ('foobar --foo=42 --any', None),  # Ignore last word until finished
            ('foobar --foo=42 --any=', None),  # Ignore last word until finished
            ('foobar --foo=42 --any="asdf', None),  # Ignore last word until finished
            ('foobar --any=42 ', r"Argument \['any'\] not found"),
            ('foobar --foo=42 --any=42 ', r"Argument \['any'\] not found"),
            ('foobar --any="42 ', 'No closing quotation'),
            ('foobar --foo ', r"Argument \['foo'\] didn't receive a value"),
            ('foobar --foo=42 ', None),
            ('foobar --foo 42 ', None),
            ('foobar --foo="42" ', None),
            ('foobar --foo "42" ', None),
            ('foobar --bar=42 ', "Required argument foo didn't receive a value."),
            ('foobar --bar=invalid ', "Required argument foo didn't receive a value."),
            ('foobar --foo=42 --bar=invalid ', "Supplied value to argument bar is of incorrect type"),
            ('foobar --foo=42 --bar=42', None),
            ('foobar --foo=42 --bar=42 ', None),
        )
        for line, error in data:
            document = Document(line)
            with self.subTest(document=document):
                if error is not None:
                    with self.assertRaisesRegex(ValidationError, error):
                        self.validator.validate(document)
                else:
                    # No error expected.
                    self.validator.validate(document)

    def test_middle_position(self):
        data = (
            # before, after, error
            ('foobar ', '--bar=42', r"Required argument foo didn't receive a value."),
            ('foobar --foo=42 ', '--any=42', r"Argument \['any'\] not found"),
            ('foobar --foo="as ', 'df"', None),
        )
        for before, after, error in data:
            document = Document(before + after, cursor_position=len(before))
            with self.subTest(document=document):

                if error is not None:
                    with self.assertRaisesRegex(ValidationError, error):
                        self.validator.validate(document)
                else:
                    # No error expected.
                    self.validator.validate(document)
