"""The module used to run the application itself, connecting the different modules together."""
import logging
import sys
from typing import Any, Callable, Optional, TextIO, Union

from epplib.commands.base import Hello, Logout, PollAcknowledgement, PollRequest, RawRequest
from epplib.commands.check import CheckContact, CheckDomain, CheckKeyset, CheckNsset
from epplib.commands.command_extensions import (
    CreateContactMailingAddressExtension,
    CreateDomainEnumExtension,
    RenewDomainEnumExtension,
    UpdateContactMailingAddressExtension,
    UpdateDomainEnumExtension,
)
from epplib.commands.create import CreateContact, CreateDomain, CreateKeyset, CreateNsset
from epplib.commands.delete import DeleteContact, DeleteDomain, DeleteKeyset, DeleteNsset
from epplib.commands.extensions import (
    CreditInfoRequest,
    SendAuthInfoContact,
    SendAuthInfoDomain,
    SendAuthInfoKeyset,
    SendAuthInfoNsset,
    TestNsset,
)
from epplib.commands.info import InfoContact, InfoDomain, InfoKeyset, InfoNsset
from epplib.commands.list import (
    GetResults,
    ListContacts,
    ListDomains,
    ListDomainsByContact,
    ListDomainsByKeyset,
    ListDomainsByNsset,
    ListKeysets,
    ListKeysetsByContact,
    ListNssets,
    ListNssetsByContact,
    ListNssetsByNs,
)
from epplib.commands.renew import RenewDomain
from epplib.commands.transfer import TransferContact, TransferDomain, TransferKeyset, TransferNsset
from epplib.commands.update import UpdateContact, UpdateDomain, UpdateKeyset, UpdateNsset

from eppic.input import InputHandler, InputProcessor
from eppic.input.input_handler import (
    CliArgsInputHandler,
    FileInputHandler,
    JsonInputHandler,
    PromptToolkitInputHandler,
    YamlInputHandler,
)
from eppic.input.input_processor import DictInputProcessor, StringInputProcessor
from eppic.input.prompt_toolkit_handlers import Completer, Validator
from eppic.output import JsonOutputHandler, OutputHandler, YamlOutputHandler
from eppic.settings import InputFormat, OutputFormat
from eppic.utils import print_xml

from .commands.argument_registry import ArgumentRegistry, register_defaults
from .commands.command_registry import CommandRegistry
from .commands.epp_command import EppCommand, LogoutCommand
from .commands.help import HelpCommand
from .commands.hooks import apply_disclose_policy, get_results_hook
from .commands.service_command import ServiceCommand
from .commands.service_requests import (
    ServiceChangeSettings,
    ServiceExit,
    ServiceListSessions,
    ServiceLogin,
    ServiceNewSession,
)
from .epp import EppHandler
from .exceptions import CommandParseError
from .models import ArgumentField, Request
from .settings import Session, Settings

try:
    # New in epplib 3.1.0
    from epplib.commands.command_extensions import CheckDomainAuctionExtension
except ImportError:
    CheckDomainAuctionExtension = None  # type: ignore[misc, assignment, unused-ignore]

LOGGER = logging.getLogger(__name__)

CommandHook = Callable[['Application', Request], None]


class Application:
    """Main application information.

    Attributes:
        argument_registry: An object holding available `Argument` classes.
            Decides the most suitable `Argument` class for a given argument, based on it's type.
        command_registry: Holds all the available commands.
        input_handler: Fetches raw input from the user.
        input_processor: An `InputProcessor` object, which parses the raw input into a `ParsedCommand`.
        output_handler: Formats and prints the output information.
        epp_handler: Handles the connection to the server.
    """

    def __init__(self, settings: Settings, command: Optional[str],
                 session: Optional[Session] = None) -> None:
        """Initialize the application.

        Arguments:
            settings: Configuration information.
            command: An optional initial command.
                    If set, this one command is run and then the application exits.
                    If `None`, an `InputHandler` gathers input from `stdin`.
            session: An optional initial `Session` information. If set, it's passed to the `EppHandler`,
                    which initializes the connection to the server.
        """
        self.argument_registry = ArgumentRegistry()
        register_defaults(self.argument_registry)
        self.command_registry = CommandRegistry()
        self.register_commands()

        self.input_handler = self.get_input_handler(settings, command, self.command_registry, self._prompt)
        self.input_processor = self.get_input_processor(settings.input_format)
        self.set_settings(settings)

        self.epp_handler = EppHandler(session, self.print_xml)

    def print_warning(self, msg: str) -> None:
        """Print warning message for user."""
        sys.stderr.write("Warning: " + msg + "\n")

    def set_settings(self, settings: Settings) -> None:
        """Set `settings` and update related changable attributes.

        Note: Drops the old `OutputHandler`, generates a new one.
        Note: Calls the `InputHandler.set_input_preferences` method on the current input_handler object.
        """
        self.settings = settings
        self.input_handler.set_settings(settings.interactive)
        self.output_handler = self.get_output_handler(settings.output_format, settings.interactive.syntax_highlighting)

    def add_session(self, session: Session) -> int:
        """Add a session to the app settings.

        Note: This doesn't change the session set in the `epp_handler`, only the `settings.sessions`.
        Note: The new session is forgotten on restart.
        """
        self.settings.sessions.append(session)
        return len(self.settings.sessions) - 1

    def _prompt(self) -> str:
        """Get the current prompt based on the information from the `EppHandler`."""
        if not self.epp_handler.logged_in:
            return "> "
        assert self.epp_handler.session is not None  # noqa: S101
        return f"{self.epp_handler.session.username}@{self.epp_handler.session.hostname} > "

    def print_xml(self, xml: bytes) -> None:
        """Pretty print xml."""
        print_xml(xml, syntax_highlighting=self.settings.interactive.syntax_highlighting,
                  mode=self.settings.print_raw_xml)

    def run_command(self, inp: Any) -> None:
        """Run one command from raw input."""
        LOGGER.debug("Parsing raw input...")
        cmd = self.input_processor(inp)
        LOGGER.debug("Parsing command...")
        command = self.command_registry.get_command(cmd.name)
        LOGGER.debug("Parsing arguments...")
        cmd = command.arg_parser.parse_arguments(cmd)
        if cmd.error is not None:
            raise CommandParseError(cmd.error)
        assert cmd.args is not None, "cmd.args is not None on processed command."  # noqa: S101
        LOGGER.debug("Running %s...", cmd.name)
        command.run(self, cmd.args)
        LOGGER.debug("Command completed.")

    def run(self) -> None:
        """Fetch commands from the input handler and run them."""
        last_result = None
        for command in self.input_handler:
            try:
                self.run_command(command)
                last_result = None
            except Exception as e:
                LOGGER.error(str(e))
                last_result = e
                if self.settings.exit_on_error:
                    break

        self.epp_handler.logout()
        if last_result is not None:
            raise last_result

    def get_output_handler(self, output_format: OutputFormat, syntax_highlighting: bool = True) -> OutputHandler:
        """Return an output handler."""
        if output_format == OutputFormat.JSON:
            return JsonOutputHandler()
        elif output_format == OutputFormat.YAML:
            return YamlOutputHandler()
        else:
            return OutputHandler(syntax_highlighting=syntax_highlighting)

    def get_input_handler(
            self, settings: Settings, command: Optional[str],
            command_registry: CommandRegistry, prompt: Union[str, Callable[[], str]] = "> ") -> InputHandler:
        """Generate the proper Input Handler."""
        if command is not None:
            return CliArgsInputHandler(command)
        elif settings.input_file is None:
            input_processor = self.get_input_processor(settings.input_format)
            completer = Completer(command_registry=command_registry, input_processor=input_processor)
            validator = Validator(command_registry=command_registry, input_processor=input_processor)
            handler = PromptToolkitInputHandler(completer, validator,
                                                history_path=settings.interactive.history_file_path, prompt=prompt)
            handler.set_settings(settings.interactive)
            return handler

        with settings.input_file.open("r") as f:
            return self.get_file_input_handler(settings.input_format, f)

    def get_file_input_handler(self, input_format: InputFormat, file: TextIO) -> InputHandler:
        """Get one of the input handlers for files, based on its format."""
        if input_format == InputFormat.JSON:
            return JsonInputHandler(file)
        if input_format == InputFormat.YAML:
            return YamlInputHandler(file)
        else:
            return FileInputHandler(file)

    def get_input_processor(self, input_format: InputFormat) -> InputProcessor:
        """Return an Input Processor."""
        if input_format in [InputFormat.JSON, InputFormat.YAML]:
            return DictInputProcessor()

        return StringInputProcessor()

    def register_commands(self) -> None:
        """Register all current commands to the registry."""
        self.command_registry.register_command(EppCommand("hello", Hello, self.argument_registry))
        self.command_registry.register_command(ServiceCommand("exit", ServiceExit, self.argument_registry))
        self.command_registry.register_command(EppCommand("raw-request", RawRequest, self.argument_registry))

        # session
        self.command_registry.register_command(ServiceCommand("login", ServiceLogin, self.argument_registry,
                                                              number_of_positional_args=2))
        self.command_registry.register_command(LogoutCommand("logout", Logout, self.argument_registry))
        self.command_registry.register_command(ServiceCommand("new-session", ServiceNewSession, self.argument_registry,
                                                              number_of_positional_args=10))
        self.command_registry.register_command(ServiceCommand("change-settings", ServiceChangeSettings,
                                                              self.argument_registry))
        self.command_registry.register_command(ServiceCommand("list-sessions", ServiceListSessions,
                                                              self.argument_registry))

        self.command_registry.register_command(EppCommand("poll-req", PollRequest, self.argument_registry))
        self.command_registry.register_command(EppCommand("poll-ack", PollAcknowledgement, self.argument_registry,
                                                          number_of_positional_args=1))

        # info
        self.command_registry.register_command(EppCommand("info-contact", InfoContact, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("info-domain", InfoDomain, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("info-keyset", InfoKeyset, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("info-nsset", InfoNsset, self.argument_registry,
                                                          number_of_positional_args=1))

        # transfer
        self.command_registry.register_command(EppCommand("transfer-contact", TransferContact,
                                                          self.argument_registry, number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("transfer-domain", TransferDomain, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("transfer-keyset", TransferKeyset, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("transfer-nsset", TransferNsset, self.argument_registry,
                                                          number_of_positional_args=1))

        # update
        self.command_registry.register_command(
            EppCommand("update-contact", UpdateContact, self.argument_registry,
                       extensions=[ArgumentField("mailing", UpdateContactMailingAddressExtension, False)],
                       pre_hooks=[apply_disclose_policy],
                       number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("update-domain", UpdateDomain, self.argument_registry,
                                                          extensions=[ArgumentField(
                                                              "enum", UpdateDomainEnumExtension, False)],
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("update-keyset", UpdateKeyset, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("update-nsset", UpdateNsset, self.argument_registry,
                                                          number_of_positional_args=1))

        # create
        self.command_registry.register_command(
            EppCommand("create-contact", CreateContact, self.argument_registry,
                       extensions=[ArgumentField("mailing", CreateContactMailingAddressExtension, False)],
                       pre_hooks=[apply_disclose_policy],
                       number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("create-domain", CreateDomain, self.argument_registry,
                                                          extensions=[ArgumentField(
                                                              "enum", CreateDomainEnumExtension, False)],
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("create-keyset", CreateKeyset, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("create-nsset", CreateNsset, self.argument_registry,
                                                          number_of_positional_args=1))

        # renew
        self.command_registry.register_command(EppCommand("renew-domain", RenewDomain, self.argument_registry,
                                                          extensions=[ArgumentField("enum", RenewDomainEnumExtension,
                                                                                    False)],
                                                          number_of_positional_args=1))

        # extensions
        self.command_registry.register_command(EppCommand("credit-info-request", CreditInfoRequest,
                                                          self.argument_registry))
        self.command_registry.register_command(EppCommand("send-auth-info-contact", SendAuthInfoContact,
                                                          self.argument_registry, number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("send-auth-info-domain", SendAuthInfoDomain,
                                                          self.argument_registry, number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("send-auth-info-keyset", SendAuthInfoKeyset,
                                                          self.argument_registry, number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("send-auth-info-nsset", SendAuthInfoNsset,
                                                          self.argument_registry, number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("test-nsset", TestNsset, self.argument_registry,
                                                          number_of_positional_args=3))

        # delete
        self.command_registry.register_command(EppCommand("delete-contact", DeleteContact, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("delete-domain", DeleteDomain, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("delete-keyset", DeleteKeyset, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("delete-nsset", DeleteNsset, self.argument_registry,
                                                          number_of_positional_args=1))

        # check
        self.command_registry.register_command(EppCommand("check-contact", CheckContact, self.argument_registry,
                                                          number_of_positional_args=1))
        check_domain_extensions = []
        if CheckDomainAuctionExtension is not None:
            check_domain_extensions.append(ArgumentField("auction", CheckDomainAuctionExtension, False))
        self.command_registry.register_command(EppCommand("check-domain", CheckDomain, self.argument_registry,
                                                          extensions=check_domain_extensions,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("check-keyset", CheckKeyset, self.argument_registry,
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("check-nsset", CheckNsset, self.argument_registry,
                                                          number_of_positional_args=1))

        # list
        self.command_registry.register_command(EppCommand("get-results", GetResults, self.argument_registry))
        self.command_registry.register_command(EppCommand("list-contacts", ListContacts, self.argument_registry,
                                                          post_hooks=[get_results_hook]))
        self.command_registry.register_command(EppCommand("list-domains", ListDomains, self.argument_registry,
                                                          post_hooks=[get_results_hook]))
        self.command_registry.register_command(EppCommand("list-domains-by-contact", ListDomainsByContact,
                                                          self.argument_registry, post_hooks=[get_results_hook],
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("list-domains-by-keyset", ListDomainsByKeyset,
                                                          self.argument_registry, post_hooks=[get_results_hook],
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("list-domains-by-nsset", ListDomainsByNsset,
                                                          self.argument_registry, post_hooks=[get_results_hook],
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("list-keysets", ListKeysets, self.argument_registry,
                                                          post_hooks=[get_results_hook]))
        self.command_registry.register_command(EppCommand("list-keysets-by-contact", ListKeysetsByContact,
                                                          self.argument_registry, post_hooks=[get_results_hook],
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("list-nssets", ListNssets, self.argument_registry,
                                                          post_hooks=[get_results_hook]))
        self.command_registry.register_command(EppCommand("list-nssets-by-contact", ListNssetsByContact,
                                                          self.argument_registry, post_hooks=[get_results_hook],
                                                          number_of_positional_args=1))
        self.command_registry.register_command(EppCommand("list-nssets-by-ns", ListNssetsByNs, self.argument_registry,
                                                          post_hooks=[get_results_hook],
                                                          number_of_positional_args=1))

        # help command
        help_command = HelpCommand(self.command_registry)
        self.command_registry.register_command(help_command)
